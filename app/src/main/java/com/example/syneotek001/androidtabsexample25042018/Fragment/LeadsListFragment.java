package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.LeadsItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.dialog.UpdateLeadLabelDialog;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LeadItemModel;

import java.util.ArrayList;
import java.util.Calendar;

public class LeadsListFragment extends Fragment implements OnLeadItemViewClickListener {
    public static final String BUNDLE_UPDATE_LABEL_POSITION = "position";
    public static final String BUNDLE_UPDATE_LABEL_VALUE = "labelValue";
    public static final int LEAD_LIST_UPDATE_LABEL = 1;
    ArrayList<LeadItemModel> leadItemArray = new ArrayList<>();
    LeadsItemAdapter mAdapter;
    View view;
    public RecyclerView recyclerView;
    public TextView tvNoDataFound;
    boolean is_from_notification = false;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_lead_list, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);

        if (getArguments()!=null)
            is_from_notification = getArguments().getBoolean(Utils.EXTRA_IS_FROM_NOTIFICATION);

        setUpRecyclerView();

        return view;
    }

    private void setUpRecyclerView() {

        leadItemArray.clear();
        leadItemArray.add(new LeadItemModel("1", "152244893677", "1", "Abc Patel", "abc@gmail.com", "9876655443", "www.google.com", "1", 1486961877, 1503061200, 0, 0));
        leadItemArray.add(new LeadItemModel("2", "152244893477", "2", "Xyz test", "xyz@gmail.com", "9878965443", "www.gmail.com", "2", 1486234877, 1508751200, 0, 0));
        leadItemArray.add(new LeadItemModel("3", "152244893489", "3", "Pqr Shah", "pqr@gmail.com", "9879665987", "www.youtube.com", "3", 1486904400, 1490101200, 0, 0));
        leadItemArray.add(new LeadItemModel("4", "152244891563", "4", "qwerty Panchal", "qwerty@gmail.com", "9978965989", "www.drive.com", "4", 1516299840, 1524754800, 0, 0));
        leadItemArray.add(new LeadItemModel("5", "152244891123", "5", "Paresh Prajapati", "paresh@gmail.com", "9978966598", "www.bitbucket.com", "5", 1512918000, 1518894000, 0, 0));
        leadItemArray.add(new LeadItemModel("6", "152244891123", "1", "Raj Sadhu", "raj@gmail.com", "9978912345", "www.test.com", "6", 1461492000, 1464955200, 0, 0));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new LeadsItemAdapter(getActivity(), leadItemArray, this,is_from_notification);
        recyclerView.setAdapter(mAdapter);
        if (is_from_notification)
        recyclerView.getLayoutManager().scrollToPosition(3);
    }

    public void onItemClicked(int position) {
    }

    public void onLeadLabelClicked(int position) {
        UpdateLeadLabelDialog dialog = new UpdateLeadLabelDialog();
        Bundle args = new Bundle();
        args.putString(BUNDLE_UPDATE_LABEL_VALUE, ((LeadItemModel) leadItemArray.get(position)).getLeadLabel());
        args.putInt("position", position);
        dialog.setArguments(args);
        dialog.setTargetFragment(this, 1);
        dialog.show(getFragmentManager().beginTransaction(), UpdateLeadLabelDialog.class.getSimpleName());
    }

    public void onLeadDetailIconClicked(int position) {

        ((HomeActivity) getActivity()).replaceFragment(new LeadsDetailFragment());

    }

    public void onLeadEmailIconClicked(int position) {
        ((HomeActivity) getActivity()).replaceFragment(new LeadAssignFragment());
    }

    public void onLeadScheduleIconClicked(int position) {
//        ((HomeActivity) getActivity()).replaceFragment(new LeadAssignFragment());

    }

    public void onLeadInactiveIconClicked(int position) {
    }

    public void onLeadDealRequestClicked(int position) {
    }

    public void onLeadBusinessRequestClicked(int position) {
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        String newLabel = bundle.getString(BUNDLE_UPDATE_LABEL_VALUE);
                        int position = bundle.getInt("position");
                        LeadItemModel objModel = (LeadItemModel) leadItemArray.get(position);
                        objModel.setLeadLabel(newLabel);
                        mAdapter.notifyItemChanged(position, objModel);
                        return;
                    }
                    return;
                } else if (resultCode == 0) {
                    Logger.m6e("PICKER", "Result Cancel");
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpRecyclerView();
    }
}
