package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.CreateDealFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealLinkFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealListInfoDetailsFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealsDetailsFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.DealsModel;

import java.util.ArrayList;
import java.util.List;

public class DealsItemAdapter extends RecyclerView.Adapter<DealsItemAdapter.MyViewHolder> {
    private Context mContext;
    private List<DealsModel> mData = new ArrayList();

    class MyViewHolder extends ViewHolder {
        public TextView tvAssign,tvCustomerId,tvDealId,tvEdit,tvInfo,tvName;

        MyViewHolder(View view) {
            super(view);
            tvAssign=view.findViewById(R.id.tvAssign);
            tvCustomerId=view.findViewById(R.id.tvCustomerId);
            tvDealId=view.findViewById(R.id.tvDealId);
            tvEdit=view.findViewById(R.id.tvEdit);
            tvInfo=view.findViewById(R.id.tvInfo);
            tvName=view.findViewById(R.id.tvName);
            tvInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    ((HomeActivity)mContext).replaceFragment(new DealListInfoDetailsFragment());
                    ((HomeActivity)mContext).replaceFragment(new DealsDetailsFragment());
                }
            });
            tvAssign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity)mContext).replaceFragment(new DealLinkFragment());

                }
            });


        }
    }

    public DealsItemAdapter(Context context, ArrayList<DealsModel> list) {
        this.mContext = context;
        this.mData = list;
    }

    public int getItemViewType(int position) {
        return 1;
    }
    @NonNull
    public DealsItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_deal_list, parent, false);

        return new DealsItemAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DealsModel mDealsModel = (DealsModel) mData.get(position);
        holder.tvDealId.setText(mDealsModel.getDealId());
        holder.tvCustomerId.setText(mDealsModel.getCustomerId());
        holder.tvName.setText(mDealsModel.getSalesManName());
        //holder.tvInfo.setText(mDealsModel.get());
    }


    public int getItemCount() {
        return mData.size();
    }
}
