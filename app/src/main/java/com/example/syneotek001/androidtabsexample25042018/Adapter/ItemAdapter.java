/*
 * Copyright 2014 Magnus Woxblom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;

public class ItemAdapter extends DragItemAdapter<Pair<Long, String>, ItemAdapter.ViewHolder> {

    private int mLayoutId;
    private int mGrabHandleId;
    private boolean mDragOnLongPress;

    public ItemAdapter(ArrayList<Pair<Long, String>> list, int layoutId, int grabHandleId, boolean dragOnLongPress) {
        mLayoutId = layoutId;
        mGrabHandleId = grabHandleId;
        mDragOnLongPress = dragOnLongPress;
        setItemList(list);
    }

    ItemInterface itemInterface;

    public interface ItemInterface {
        void onClickItem( int pos);
    }

    public void setItemInterface(ItemInterface itemInterface) {

        this.itemInterface = itemInterface;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        String text = mItemList.get(position).second;
        holder.mText.setText(text);
        holder.itemView.setTag(mItemList.get(position));
        holder.web_view.setBackgroundColor(Color.TRANSPARENT);
        holder.web_view.getSettings().setJavaScriptEnabled(true);
        holder.web_view.getSettings().setLoadsImagesAutomatically(true);
        holder.web_view.getSettings().setDomStorageEnabled(true);
        holder.web_view.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        holder.web_view.getSettings().setLoadWithOverviewMode(true);
        // holder.web_view.getSettings().setUseWideViewPort(true);

        holder.web_view.loadDataWithBaseURL(null,mItemList.get(position).second, "text/html", "UTF-8", null);

        // LogUtils.i(position+ " mpos "+mItemList.get(position).first);

        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mpos = mItemList.get(position).first.intValue();
                //  LogUtils.i( " mpos "+mpos);
                itemInterface.onClickItem(mpos);
            }
        });




      /*  if(mItemList.get(position).first == 0)
        {   holder.lyt_1.setVisibility(View.VISIBLE);holder.lyt_2.setVisibility(View.GONE);holder.lyt_3.setVisibility(View.GONE);holder.lyt_4.setVisibility(View.GONE);}
        else if(mItemList.get(position).first == 1)
        {   holder.lyt_2.setVisibility(View.VISIBLE);holder.lyt_1.setVisibility(View.GONE);holder.lyt_3.setVisibility(View.GONE);holder.lyt_4.setVisibility(View.GONE);}
        else if(mItemList.get(position).first == 2)
        {   holder.lyt_3.setVisibility(View.VISIBLE);holder.lyt_2.setVisibility(View.GONE);holder.lyt_1.setVisibility(View.GONE);holder.lyt_4.setVisibility(View.GONE);}
        else if(mItemList.get(position).first == 3)
        {   holder.lyt_3.setVisibility(View.GONE);holder.lyt_2.setVisibility(View.GONE);holder.lyt_1.setVisibility(View.GONE);holder.lyt_4.setVisibility(View.VISIBLE);}
*/

    }

    @Override
    public long getUniqueItemId(int position) {
        return mItemList.get(position).first;
    }

    class ViewHolder extends DragItemAdapter.ViewHolder {
        TextView mText;LinearLayout lyt_1,lyt_2,lyt_3,lyt_4;
        WebView web_view; CardView card; ImageView img_edit;

        ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            mText = (TextView) itemView.findViewById(R.id.text);
            lyt_1 =(LinearLayout)itemView.findViewById(R.id.lyt_1);
            lyt_2 =(LinearLayout)itemView.findViewById(R.id.lyt_2);
            lyt_3 =(LinearLayout)itemView.findViewById(R.id.lyt_3);
            lyt_4 =(LinearLayout)itemView.findViewById(R.id.lyt_4);
            web_view = (WebView)itemView.findViewById(R.id.web_view);
            card = (CardView)itemView.findViewById(R.id.card);
            img_edit = (ImageView)itemView.findViewById(R.id.img_edit);
        }

        @Override
        public void onItemClicked(View view) {
//            Toast.makeText(view.getContext(), "Item clicked", Toast.LENGTH_SHORT).show();
        }

        @Override
        public boolean onItemLongClicked(View view) {
//            Toast.makeText(view.getContext(), "Item long clicked", Toast.LENGTH_SHORT).show();
            return true;
        }
    }
}
