package com.example.syneotek001.androidtabsexample25042018.Adapter.dialog;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.SoldInactiveLeadListFragment;
import com.example.syneotek001.androidtabsexample25042018.R;

public class UpdateLeadStatusDialog extends AppCompatDialogFragment implements OnClickListener {
    private String currentStatus = "2";
//    DialogUpdateLeadStatusBinding mBinding;
    private int position;

    public ImageView ivClose,ivStatusClosing,ivStatusContact,ivStatusUnderContract;

    public LinearLayout llClosingLeads,llContactLeads,llUnderContractLeads;
    public TextView tvStatusClosing,tvStatusContact,tvStatusUnderContract;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.dialog_update_lead_status, container, false);
        ivClose=view.findViewById(R.id.ivClose);
        ivStatusClosing=view.findViewById(R.id.ivStatusClosing);
        ivStatusContact=view.findViewById(R.id.ivStatusContact);
        ivStatusUnderContract=view.findViewById(R.id.ivStatusUnderContract);
        llClosingLeads=view.findViewById(R.id.llClosingLeads);
        llContactLeads=view.findViewById(R.id.llContactLeads);
        llUnderContractLeads=view.findViewById(R.id.llUnderContractLeads);
        tvStatusClosing=view.findViewById(R.id.tvStatusClosing);
        tvStatusContact=view.findViewById(R.id.tvStatusContact);
        tvStatusUnderContract=view.findViewById(R.id.tvStatusUnderContract);
//            this.mBinding = (DialogUpdateLeadStatusBinding) DataBindingUtil.inflate(inflater, R.layout.dialog_update_lead_status, container, false);
//            this.view = this.mBinding.getRoot();
        prepareLayout(getArguments());
        setClickEvent();
        return view;
    }

    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
    }

    private void prepareLayout(Bundle bundle) {
        if (bundle != null) {
            this.currentStatus = bundle.getString(SoldInactiveLeadListFragment.BUNDLE_UPDATE_STATUS_VALUE);
            this.position = bundle.getInt("position");
        }
    }

    private void setClickEvent() {
       llContactLeads.setOnClickListener(this);
      llUnderContractLeads.setOnClickListener(this);
        llClosingLeads.setOnClickListener(this);
       ivClose.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                dismiss();
                break;
            case R.id.llClosingLeads:
                this.currentStatus = "4";
                break;
            case R.id.llContactLeads:
                this.currentStatus = "2";
                break;
            case R.id.llUnderContractLeads:
                this.currentStatus = "3";
                break;
        }
        getTargetFragment().onActivityResult(getTargetRequestCode(), -1, new Intent().putExtra(SoldInactiveLeadListFragment.BUNDLE_UPDATE_STATUS_VALUE, this.currentStatus).putExtra("position", this.position));
        dismiss();
    }
}
