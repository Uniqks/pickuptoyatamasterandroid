package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.ItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.EmailTemplateSelect;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Temp_1_Activity;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.woxthebox.draglistview.BoardView;
import com.woxthebox.draglistview.DragItem;

import java.util.ArrayList;


public class EmailTemplateCreateFragment1 extends Fragment implements ItemAdapter.ItemInterface {

    private static int sCreatedItems = 0;
    private BoardView mBoardView;

    AppCompatImageView ivAdd;ImageView ivBack;

    private int mColumns;
    String mData = "<tr><td><p  data-block-id=\"background\" class=\"title\" style=\"color: rgb(117, 117, 117); padding: 0px 0px;\">" +
            "<h1 align=\"center\" data-block-id=\"main-title\" style=\"font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0px; color: rgb(68, 68, 68);\">" +
            "Sample</h1> <h4 align=\"center\" data-block-id=\"sub-title\" style=\"font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 0px; " +
            "color: rgb(68, 68, 68);\">Subtitle</h4></p></td></tr>";



    String mData1 ="<tr style=\"display: table-cell; font-family: Arial; font-size: 13px; color: rgb(0, 0, 0); line-height: 20px; padding: 15px;\"><td width=\"295\">" +
            "<img  align=\"left\" src=\"https://c.pxhere.com/photos/32/f4/water_lilies_pink_water_lake_flowers_wet_fountain_city_ornamental_plants-928659.jpg!d\" width=\"140\" " +
            "style=\"display: block; margin: 0px; max-width: 340px; padding: 5px 5px 0px 0px;\"></td> <td width=\"10\"></td> <td width=\"195\" valign=\"top\">" +
            "<content id=\"mce_0\" class=\"mce-content-body\" contenteditable=\"true\" style=\"position: relative;\" " +
            "spellcheck=\"false\"><div><p style=\"line-height: 20px; margin: 0;\" data-mce-style=\"line-height: 20px; margin: 0;\">" +
            "Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, " +
            "quis nostrum exercitationem ullam corporis suscipit laboriosam.</p></div></content></td></tr>";

    ArrayList<Pair<Long, String>> mItemArray = new ArrayList<>();
    ArrayList<Pair<Long, String>> mItemArray_temp = new ArrayList<>();
    ItemAdapter listAdapter;

    public EmailTemplateCreateFragment1() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_email_template_create, container, false);

        ivAdd = view.findViewById(R.id.ivAdd);
        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), EmailTemplateSelect.class));
            }
        });
         ivBack = view.findViewById(R.id.iv_back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("123","123");
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        mBoardView = view.findViewById(R.id.board_view);
        mBoardView.setSnapToColumnsWhenScrolling(true);
        mBoardView.setSnapToColumnWhenDragging(true);
        mBoardView.setSnapDragItemToTouch(true);
        mBoardView.setCustomDragItem(new MyDragItem(getActivity(), R.layout.column_item));
        mBoardView.setCustomColumnDragItem(new MyColumnDragItem(getActivity(), R.layout.column_drag_layout));
        mBoardView.setSnapToColumnInLandscape(false);
        mBoardView.setColumnSnapPosition(BoardView.ColumnSnapPosition.CENTER);
        mBoardView.setBoardListener(new BoardView.BoardListener() {
            @Override
            public void onItemDragStarted(int column, int row) {
                mItemArray_temp.clear();
                mItemArray_temp.addAll(mItemArray);
                Log.i("drag ","drag started");
                //Toast.makeText(getContext(), "Start - column: " + column + " row: " + row, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemDragEnded(int fromColumn, int fromRow, int toColumn, int toRow) {
                if (fromColumn != toColumn || fromRow != toRow) {
                    //   Toast.makeText(getContext(), "End - column: " + toColumn + " row: " + toRow, Toast.LENGTH_SHORT).show();

                    int old_row = fromRow, new_row = toRow;

                    //  mItemArray.set(fromRow,mItemArray_temp.get(toRow));
                    mItemArray.set(toRow,mItemArray_temp.get(fromRow));
                    Log.i("drag ", " fromRow: "+fromRow + " toRow: " + toRow);
                }
            }

            @Override
            public void onItemChangedPosition(int oldColumn, int oldRow, int newColumn, int newRow) {
                //   Toast.makeText(mBoardView.getContext(), "Position changed - column: " + newColumn + " row: " + newRow, Toast.LENGTH_SHORT).show();

                //   Log.i("drag ", "oldRow: "+oldRow + " newRow: " + newRow);
            }

            @Override
            public void onItemChangedColumn(int oldColumn, int newColumn) {
                TextView itemCount1 = mBoardView.getHeaderView(oldColumn).findViewById(R.id.item_count);
                itemCount1.setText(String.valueOf(mBoardView.getAdapter(oldColumn).getItemCount()));
                TextView itemCount2 = mBoardView.getHeaderView(newColumn).findViewById(R.id.item_count);
                itemCount2.setText(String.valueOf(mBoardView.getAdapter(newColumn).getItemCount()));
            }

            @Override
            public void onFocusedColumnChanged(int oldColumn, int newColumn) {
                //Toast.makeText(getContext(), "Focused column changed from " + oldColumn + " to " + newColumn, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onColumnDragStarted(int position) {
                //Toast.makeText(getContext(), "Column drag started from " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onColumnDragChangedPosition(int oldPosition, int newPosition) {
                //Toast.makeText(getContext(), "Column changed from " + oldPosition + " to " + newPosition, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onColumnDragEnded(int position) {
                //Toast.makeText(getContext(), "Column drag ended at " + position, Toast.LENGTH_SHORT).show();
            }
        });
        mBoardView.setBoardCallback(new BoardView.BoardCallback() {
            @Override
            public boolean canDragItemAtPosition(int column, int dragPosition) {
                // Add logic here to prevent an item to be dragged
                return true;
            }

            @Override
            public boolean canDropItemAtPosition(int oldColumn, int oldRow, int newColumn, int newRow) {
                // Add logic here to prevent an item to be dropped
                return true;
            }
        });
        addColumn();

        return view;

    }

    private void addColumn() {
        int addItems = 2;
        for (int i = 0; i < addItems; i++) {
            long id = sCreatedItems++;
            if(i==0) {
                mItemArray.add(new Pair<>(id, mData));
                mItemArray_temp.add(new Pair<>(id, mData));
            }else if(i==1) {
                mItemArray.add(new Pair<>(id, mData1));
                mItemArray_temp.add(new Pair<>(id, mData1));
            }

        }
        final int column = mColumns;
        listAdapter = new ItemAdapter(mItemArray, R.layout.column_item, R.id.item_layout, true);
        listAdapter.setItemInterface(EmailTemplateCreateFragment1.this);
        final View header = View.inflate(getActivity(), R.layout.column_header, null);
        ((TextView) header.findViewById(R.id.text)).setText("Column " + (mColumns + 1));
        ((TextView) header.findViewById(R.id.item_count)).setText("" + addItems);
        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long id = sCreatedItems++;
                Pair item = new Pair<>(id, "Test " + id);
                mBoardView.addItem(mBoardView.getColumnOfHeader(v), 0, item, true);
                ((TextView) header.findViewById(R.id.item_count)).setText(String.valueOf(mItemArray.size()));
            }
        });
        mBoardView.addColumn(listAdapter, header, header, false);
        mColumns++;
    }

    @Override
    public void onClickItem(int pos) {

        if(pos ==0)
        {
            startActivityForResult(new Intent(getActivity(),Temp_1_Activity.class).putExtra(Utils.TEMP_POS,  pos+""),0);
        }

    }

    /*@Override
    public void onDeleteClick(int pos) {
        mBoardView.removeColumn(0);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == 0)
        {
            if(data != null)
            {
                String mText = data.getStringExtra(Utils.TEMP_1);
                String mPos = data.getStringExtra(Utils.TEMP_POS);

                mItemArray.set(Integer.parseInt(mPos),new Pair<>(Long.parseLong(mPos),mText));
                listAdapter.notifyDataSetChanged();
            }
        }

    }

    private static class MyColumnDragItem extends DragItem {

        MyColumnDragItem(Context context, int layoutId) {
            super(context, layoutId);
            setSnapToTouch(false);
        }

        @Override
        public void onBindDragView(View clickedView, View dragView) {
            LinearLayout clickedLayout = (LinearLayout) clickedView;
            View clickedHeader = clickedLayout.getChildAt(0);
            RecyclerView clickedRecyclerView = (RecyclerView) clickedLayout.getChildAt(1);

            View dragHeader = dragView.findViewById(R.id.drag_header);
            ScrollView dragScrollView = dragView.findViewById(R.id.drag_scroll_view);
            LinearLayout dragLayout = dragView.findViewById(R.id.drag_list);
            dragLayout.removeAllViews();

            ((TextView) dragHeader.findViewById(R.id.text)).setText(((TextView) clickedHeader.findViewById(R.id.text)).getText());
            ((TextView) dragHeader.findViewById(R.id.item_count)).setText(((TextView) clickedHeader.findViewById(R.id.item_count)).getText());
            for (int i = 0; i < clickedRecyclerView.getChildCount(); i++) {
                View view = View.inflate(dragView.getContext(), R.layout.column_item, null);
                ((TextView) view.findViewById(R.id.text)).setText(((TextView) clickedRecyclerView.getChildAt(i).findViewById(R.id.text)).getText());
                dragLayout.addView(view);

                if (i == 0) {
                    dragScrollView.setScrollY(-clickedRecyclerView.getChildAt(i).getTop());
                }
            }

            dragView.setPivotY(0);
            dragView.setPivotX(clickedView.getMeasuredWidth() / 2);
        }

        @Override
        public void onStartDragAnimation(View dragView) {
            super.onStartDragAnimation(dragView);
            dragView.animate().scaleX(0.9f).scaleY(0.9f).start();
        }

        @Override
        public void onEndDragAnimation(View dragView) {
            super.onEndDragAnimation(dragView);
            dragView.animate().scaleX(1).scaleY(1).start();
        }
    }

    private static class MyDragItem extends DragItem {

        MyDragItem(Context context, int layoutId) {
            super(context, layoutId);
        }

        @Override
        public void onBindDragView(View clickedView, View dragView) {
            CharSequence text = ((TextView) clickedView.findViewById(R.id.text)).getText();
            ((TextView) dragView.findViewById(R.id.text)).setText(text);
            //   CardView dragCard = dragView.findViewById(R.id.card);
            //  CardView clickedCard = clickedView.findViewById(R.id.card);


            ImageView dragCard = dragView.findViewById(R.id.img_drag);
            ImageView clickedCard = clickedView.findViewById(R.id.img_drag);

            Log.i("drag ", dragCard.getParent()+" clicked " +clickedCard.getParent());

            // dragCard.setMaxCardElevation(40);
            // dragCard.setCardElevation(clickedCard.getCardElevation());
            // I know the dragView is a FrameLayout and that is why I can use setForeground below api level 23
//            dragCard.setForeground(clickedView.getResources().getDrawable(R.drawable.card_view_drag_foreground));
        }

        @Override
        public void onMeasureDragView(View clickedView, View dragView) {
            //   CardView dragCard = dragView.findViewById(R.id.card);
            //  CardView clickedCard = clickedView.findViewById(R.id.card);

            ImageView dragCard = dragView.findViewById(R.id.img_drag);
            ImageView clickedCard = clickedView.findViewById(R.id.img_drag);
            int widthDiff = dragCard.getPaddingLeft() - clickedCard.getPaddingLeft() + dragCard.getPaddingRight() -
                    clickedCard.getPaddingRight();
            int heightDiff = dragCard.getPaddingTop() - clickedCard.getPaddingTop() + dragCard.getPaddingBottom() -
                    clickedCard.getPaddingBottom();
            int width = clickedView.getMeasuredWidth() + widthDiff;
            int height = clickedView.getMeasuredHeight() + heightDiff;
            dragView.setLayoutParams(new FrameLayout.LayoutParams(width, height));

            int widthSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
            int heightSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
            dragView.measure(widthSpec, heightSpec);
        }

        @Override
        public void onStartDragAnimation(View dragView) {
            //   CardView dragCard = dragView.findViewById(R.id.card);

          /*  ImageView dragCard = dragView.findViewById(R.id.img_drag);
            ObjectAnimator anim = ObjectAnimator.ofFloat(dragCard, "CardElevation", dragCard.getCardElevation(), 40);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.setDuration(ANIMATION_DURATION);
            anim.start();*/
        }

        @Override
        public void onEndDragAnimation(View dragView) {
         /*   ImageView dragCard = dragView.findViewById(R.id.img_drag);
          //  CardView dragCard = dragView.findViewById(R.id.card);
            ObjectAnimator anim = ObjectAnimator.ofFloat(dragCard, "CardElevation", dragCard.getCardElevation(), 6);
            anim.setInterpolator(new DecelerateInterpolator());
            anim.setDuration(ANIMATION_DURATION);
            anim.start();*/
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
