package com.example.syneotek001.androidtabsexample25042018;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemAnimator;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.Adapter.NavigationDrawerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Fragment.AddEditContactMainFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.AnalyticHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.AnalyticsHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.AnnouncementFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.ChatListHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.ContactListFragmentone;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealListHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealManagerListFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.EmailInboxListFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.EmailTemplateCreateFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.EmailTemplateListFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.FragCarBuilder;
import com.example.syneotek001.androidtabsexample25042018.Fragment.FragmentComingSoon;
import com.example.syneotek001.androidtabsexample25042018.Fragment.FragmentSchedule;
import com.example.syneotek001.androidtabsexample25042018.Fragment.LeadListHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.LeadsListFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.LeadsListHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.ReceptionistListFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.SalesmanListFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.SelectLeadCategoryFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.SelectSalesmanFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.SettingsFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.SoldInactiveLeadListFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.TaskListFragment;
import com.example.syneotek001.androidtabsexample25042018.Utils.AppConstants;
import com.example.syneotek001.androidtabsexample25042018.Utils.BottomNavigationViewHelper;
import com.example.syneotek001.androidtabsexample25042018.Utils.CustomTypefaceSpan;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnExpandableRecyclerViewChildItemClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.NavigationDrawerDataFactory;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import java.util.HashMap;
import java.util.Stack;
import java.util.List;

import android.view.View;

@SuppressLint("Registered")
public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    public FrameLayout container;
    public DrawerLayout drawerLayout;
    public BottomNavigationView navigation;
    public NavigationView nvView;
    RecyclerView recyclerView;
    private int mMenuId;
    public NavigationDrawerAdapter adapter;
    boolean doubleBackToExitPressedOnce = false;
    public String mCurrentTab;
    Fragment fragment;
    private NavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new C05692();
    private HashMap<String, Stack<Fragment>> mStacks;
    //    ExpandableListAdapter listAdapter;
//    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ImageView groupIndicator;
    TextView conatctlist,salesmanlist,receptionistlist,txt_onlineLead,txt_offlineLead,txt_followupLead;
    LinearLayout ll_carBuilder,llSchedules;

    //==============Empand Nevigation =====
    RelativeLayout rel_LeadManager, rel_Email, rel_DealManager, rel_TaskManager;
    ExpandableRelativeLayout exp_rel_LeadManager, exp_rel_Email, exp_rel_DealManager, exp_rel_TaskManager;
    ImageView iv_expandLeadManager, iv_expandEmail, iv_expandDealManager, iv_expandTaskManager;

    boolean is_from_notification = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigation = findViewById(R.id.navigation);
        nvView = findViewById(R.id.nvView);
        recyclerView = findViewById(R.id.recyclerView);

        //==============Empand Nevigation =====
        rel_LeadManager = findViewById(R.id.rel_LeadManager);
        exp_rel_LeadManager = findViewById(R.id.exp_rel_LeadManager);
        iv_expandLeadManager = findViewById(R.id.iv_expandLeadManager);

        rel_Email = findViewById(R.id.rel_Email);
        exp_rel_Email = findViewById(R.id.exp_rel_Email);
        iv_expandEmail = findViewById(R.id.iv_expandEmail);

        rel_DealManager = findViewById(R.id.rel_DealManager);
        rel_TaskManager = findViewById(R.id.rel_TaskManager);
        iv_expandDealManager = findViewById(R.id.iv_expandDealManager);
        iv_expandTaskManager = findViewById(R.id.iv_expandTaskManager);
        exp_rel_DealManager = findViewById(R.id.exp_rel_DealManager);
        exp_rel_TaskManager = findViewById(R.id.exp_rel_TaskManager);

        rel_LeadManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getExpandOrCollapse(exp_rel_LeadManager);
            }
        });
        TextView txt_solidLead = findViewById(R.id.txt_solidLead);
        txt_solidLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoldInactiveLeadListFragment soldLeadListFragment = new SoldInactiveLeadListFragment();
                Bundle bundleSoldLead = new Bundle();
                bundleSoldLead.putString(SoldInactiveLeadListFragment.BUNDLE_LEAD_STATUS_TYPE, getResources().getString(R.string.sold_leads));
                soldLeadListFragment.setArguments(bundleSoldLead);
                replaceFragment(soldLeadListFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        TextView txt_InactiveLead = findViewById(R.id.txt_InactiveLead);
        txt_InactiveLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoldInactiveLeadListFragment soldLeadListFragment = new SoldInactiveLeadListFragment();
                Bundle bundleSoldLead = new Bundle();
                bundleSoldLead.putString(SoldInactiveLeadListFragment.BUNDLE_LEAD_STATUS_TYPE, getResources().getString(R.string.inactive_dead_leads));
                soldLeadListFragment.setArguments(bundleSoldLead);
                replaceFragment(soldLeadListFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        TextView txt_addNewLead = findViewById(R.id.txt_addNewLead);
        txt_addNewLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new AddEditContactMainFragment());
            }
        });

        TextView txt_DealManagerItem = findViewById(R.id.txt_DealManagerItem);
        txt_DealManagerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                replaceFragment(new DealManagerListFragment());
                replaceFragment(new SelectSalesmanFragment());
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        TextView txt_WaitingForReply = findViewById(R.id.txt_WaitingForReply);
        txt_WaitingForReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("task_manager_title", getString(R.string.str_waiting_for_reply));
                TaskListFragment taskListFragment = new TaskListFragment();
                taskListFragment.setArguments(bundle);
                replaceFragment(taskListFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

//        txtEmailTemplate

        TextView txtEmailTemplate = findViewById(R.id.txtEmailTemplate);
        txtEmailTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailTemplateCreateFragment taskListFragment = new EmailTemplateCreateFragment();
                replaceFragment(taskListFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
        TextView txtEmail = findViewById(R.id.txtEmail);
        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("task_manager_title", getString(R.string.str_email_template_title));
                EmailTemplateListFragment taskListFragment = new EmailTemplateListFragment();
                taskListFragment.setArguments(bundle);
                replaceFragment(taskListFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        TextView txt_WaitingForCoSinger = findViewById(R.id.txt_WaitingForCoSinger);
        txt_WaitingForCoSinger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("task_manager_title", getString(R.string.str_waiting_for_co_singer));
                TaskListFragment taskListFragment = new TaskListFragment();
                taskListFragment.setArguments(bundle);
                replaceFragment(taskListFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });


        TextView txt_DealBuilder = findViewById(R.id.txt_DealBuilder);
        txt_DealBuilder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new DealListHolderFragment());
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        rel_Email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getExpandOrCollapse(exp_rel_Email);
            }
        });

        rel_DealManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getExpandOrCollapse(exp_rel_DealManager);
            }
        });

        rel_TaskManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getExpandOrCollapse(exp_rel_TaskManager);
            }
        });

        navigation.setOnNavigationItemSelectedListener(this);
        navigation.getMenu().findItem(R.id.navigation_profile).setChecked(true);
        prepareTabs();
        setUpNavigationDrawer();
//        replaceFragment(new AnalyticHolderFragment());
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, new AnalyticHolderFragment());
//        ft.addToBackStack(null);
        ft.commit();

        conatctlist = (TextView) nvView.findViewById(R.id.conatctlist);
        conatctlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new ContactListFragmentone());
            }
        });

//

        salesmanlist = (TextView) nvView.findViewById(R.id.salesmanlist);
        salesmanlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new SalesmanListFragment());
            }
        });

        receptionistlist = (TextView) nvView.findViewById(R.id.receptionistlist);
        receptionistlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new ReceptionistListFragment());
            }
        });

        txt_onlineLead = (TextView) nvView.findViewById(R.id.txt_onlineLead);
        txt_onlineLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(GravityCompat.START);
                Bundle bundle = new Bundle();
                bundle.putString("lead_type","online");
                SelectLeadCategoryFragment selectLeadCategoryFragment = new SelectLeadCategoryFragment();
                selectLeadCategoryFragment.setArguments(bundle);
                replaceFragment(selectLeadCategoryFragment);
            }
        });

        txt_offlineLead = (TextView) nvView.findViewById(R.id.txt_offlineLead);
        txt_offlineLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(GravityCompat.START);
                Bundle bundle = new Bundle();
                bundle.putString("lead_type","offline");
                SelectLeadCategoryFragment selectLeadCategoryFragment = new SelectLeadCategoryFragment();
                selectLeadCategoryFragment.setArguments(bundle);
                replaceFragment(selectLeadCategoryFragment);
            }
        });

        txt_followupLead = (TextView) nvView.findViewById(R.id.txt_followupLead);
        txt_followupLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(GravityCompat.START);
//                replaceFragment(new ReceptionistListFragment());
            }
        });

        ll_carBuilder = findViewById(R.id.ll_carBuilder);
        ll_carBuilder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new FragCarBuilder());
            }
        });

        llSchedules = findViewById(R.id.llSchedules);
        llSchedules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new FragmentSchedule());
            }
        });

        TextView txt_Inbox = findViewById(R.id.txt_Inbox);
        txt_Inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new EmailInboxListFragment());
            }
        });
        TextView txt_activeLead = findViewById(R.id.txt_activeLead);
        txt_activeLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                replaceFragment(new LeadListHolderFragment());
            }
        });

        Menu m = navigation.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }

        bottomNavigationMenuView =
                (BottomNavigationMenuView) navigation.getChildAt(0);
        v = bottomNavigationMenuView.getChildAt(2);
        itemView = (BottomNavigationItemView) v;

        badge = LayoutInflater.from(this)
                .inflate(R.layout.notification_badge, bottomNavigationMenuView, false);

        itemView.addView(badge);

        tvBadgeNo = badge.findViewById(R.id.notifications_badge);

        registerReceiver(receiver, new IntentFilter(Utils.BROADCAST_LEAD_COUNT));

        if (getIntent().getExtras()!=null) {
            is_from_notification = getIntent().getExtras().getBoolean(Utils.EXTRA_IS_FROM_NOTIFICATION);
            Log.e("HomeActivity","onCreate is_from_notification "+is_from_notification);
            if (is_from_notification) {
                LeadsListHolderFragment leadsListHolderFragment = new LeadsListHolderFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(Utils.EXTRA_IS_FROM_NOTIFICATION,true);
                leadsListHolderFragment.setArguments(bundle);
                replaceFragment(leadsListHolderFragment);
            }
        }

    }

    BottomNavigationMenuView bottomNavigationMenuView;
    BottomNavigationItemView itemView;
    View v;
    View badge;
    TextView tvBadgeNo;

    BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("HomeActivity"," onReceive NOTIFICATION_LEAD_COUNT "+Utils.getInstance(HomeActivity.this).getInt(Utils.NOTIFICATION_LEAD_COUNT));
            tvBadgeNo.setVisibility(View.VISIBLE);
            tvBadgeNo.setText(Utils.getInstance(HomeActivity.this).getInt(Utils.NOTIFICATION_LEAD_COUNT)+"");

        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void getExpandOrCollapse(ExpandableRelativeLayout expandableRelativeLayout) {

        if (!expandableRelativeLayout.isExpanded()) {
            if (expandableRelativeLayout.equals(exp_rel_LeadManager)) {
                iv_expandLeadManager.setImageResource(R.drawable.ic_action_uparrow);
            } else if (expandableRelativeLayout.equals(exp_rel_Email)) {
                iv_expandEmail.setImageResource(R.drawable.ic_action_uparrow);
            } else if (expandableRelativeLayout.equals(exp_rel_DealManager)) {
                iv_expandDealManager.setImageResource(R.drawable.ic_action_uparrow);
            } else if (expandableRelativeLayout.equals(exp_rel_TaskManager)) {
                iv_expandTaskManager.setImageResource(R.drawable.ic_action_uparrow);
            }

        } else {

            if (expandableRelativeLayout.equals(exp_rel_LeadManager)) {
                iv_expandLeadManager.setImageResource(R.drawable.ic_action_downarrow);
            } else if (expandableRelativeLayout.equals(exp_rel_Email)) {
                iv_expandEmail.setImageResource(R.drawable.ic_action_downarrow);
            } else if (expandableRelativeLayout.equals(exp_rel_DealManager)) {
                iv_expandDealManager.setImageResource(R.drawable.ic_action_downarrow);
            } else if (expandableRelativeLayout.equals(exp_rel_TaskManager)) {
                iv_expandTaskManager.setImageResource(R.drawable.ic_action_downarrow);
            }
        }
        expandableRelativeLayout.toggle();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    class C05681 implements OnExpandableRecyclerViewChildItemClickListener {
        C05681() {
        }

        public void onParentItemClicked(String title) {
            Logger.m6e("Parent Clicked", title);
            if (HomeActivity.this.drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
                drawerLayout.closeDrawer((int) GravityCompat.START);
            }
            HomeActivity.this.openClickedNavigationDrawerMenuItem(title);
        }

        public void onChildItemClicked(String title) {
            Logger.m6e("Child Clicked", title);
            if (drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
                drawerLayout.closeDrawer((int) GravityCompat.START);
            }
            HomeActivity.this.openClickedNavigationDrawerMenuItem(title);
        }
    }

    class C05692 implements NavigationView.OnNavigationItemSelectedListener {
        C05692() {
        }

        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_analytics:
                    HomeActivity.this.selectedTab(AppConstants.TAB_ANALYTIC);
                    return true;
                case R.id.navigation_message:
                    HomeActivity.this.selectedTab(AppConstants.TAB_MESSAGE);
                    return true;
                case R.id.navigation_profile:
                    HomeActivity.this.selectedTab(AppConstants.TAB_PROFILE);
                    return true;
                case R.id.navigation_settings:
                    HomeActivity.this.selectedTab(AppConstants.TAB_SETTINGS);
                    return true;
                case R.id.navigation_users:
                    HomeActivity.this.selectedTab(AppConstants.TAB_USER);
                    return true;
                default:
                    return false;
            }
        }
    }

    class C05703 implements Runnable {
        C05703() {
        }

        public void run() {
            HomeActivity.this.doubleBackToExitPressedOnce = false;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.navigation_profile) {
            if (fragment instanceof AnalyticHolderFragment) {
            } else {
                replaceBottomTabFragment(new AnalyticHolderFragment());
            }

        } else if (id == R.id.navigation_analytics) {
            if (fragment instanceof AnalyticsHolderFragment) {

            } else {
                replaceBottomTabFragment(new AnalyticsHolderFragment());
            }

        } else if (id == R.id.navigation_users) {
            if (fragment instanceof AnnouncementFragment) {

            } else {
                replaceBottomTabFragment(new AnnouncementFragment());
                tvBadgeNo.setVisibility(View.GONE);
                Utils.getInstance(HomeActivity.this).setInt(Utils.NOTIFICATION_LEAD_COUNT,0);
            }

        } else if (id == R.id.navigation_message) {
            if (fragment instanceof ChatListHolderFragment) {

            } else {
                replaceBottomTabFragment(new ChatListHolderFragment());
            }

        } else if (id == R.id.navigation_settings) {
            if (fragment instanceof SettingsFragment) {

            } else {
                replaceBottomTabFragment(new SettingsFragment());
            }

        }
        return true;
    }

    public void replaceFragment(Fragment fragment) {

        this.fragment = fragment;
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void replaceBottomTabFragment(Fragment fragment) {
        this.fragment = fragment;
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
//        ft.addToBackStack(null);
        ft.commit();
    }

    private void prepareTabs() {
        this.mStacks = new HashMap();
        this.mStacks.put(AppConstants.TAB_PROFILE, new Stack());
        this.mStacks.put(AppConstants.TAB_ANALYTIC, new Stack());
        this.mStacks.put(AppConstants.TAB_USER, new Stack());
        this.mStacks.put(AppConstants.TAB_MESSAGE, new Stack());
        this.mStacks.put(AppConstants.TAB_SETTINGS, new Stack());

//        navigation.setOnNavigationItemSelectedListener((BottomNavigationView.OnNavigationItemSelectedListener) mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.removeShiftMode(navigation);
//        BottomNavigationViewHelper1.disableShiftMode(navigation);
        this.mCurrentTab = AppConstants.TAB_ANALYTIC;
//        navigation.setSelectedItemId(R.id.navigation_analytics);
    }

    private void setUpNavigationDrawer() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        ItemAnimator animator = recyclerView.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        this.adapter = new NavigationDrawerAdapter(NavigationDrawerDataFactory.makeGenres(), new C05681());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(this.adapter);
    }

    public void opendrawer() {
        if (drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
            drawerLayout.closeDrawer((int) GravityCompat.START);
        } else {
            drawerLayout.openDrawer((int) GravityCompat.START);
        }
    }

    private void selectedTab(String tabId) {
        mCurrentTab = tabId;
        if (((Stack) mStacks.get(tabId)).size() == 0) {
            boolean z = true;
            switch (tabId.hashCode()) {
                case -1893596835:
                    if (tabId.equals(AppConstants.TAB_MESSAGE)) {
                        z = true;
                        break;
                    }
                    break;
                case -970201715:
                    if (tabId.equals(AppConstants.TAB_SETTINGS)) {
                        z = true;
                        break;
                    }
                    break;
                case -906929611:
                    if (tabId.equals(AppConstants.TAB_USER)) {
                        z = true;
                        break;
                    }
                    break;
                case 865689591:
                    if (tabId.equals(AppConstants.TAB_ANALYTIC)) {
                        z = true;
                        break;
                    }
                    break;
                case 1137019647:
                    if (tabId.equals(AppConstants.TAB_PROFILE)) {
                        z = false;
                        break;
                    }
                    break;
            }
            if (!z) {
                pushFragment(tabId, new FragmentComingSoon(), false, true);
                return;
               /* case true:
                    pushFragment(tabId, new AnalyticHolderFragment(), false, true);
                    return;
                case true:
                    pushFragment(tabId, new FragmentComingSoon(), false, true);
                    return;
                case true:
                    pushFragment(tabId, new ChatListHolderFragment(), false, true);
                    return;
                case true:
                    pushFragment(tabId, new FragmentComingSoon(), false, true);
                    return;*/
            } else {
                return;
            }
        }
        pushFragment(tabId, (Fragment) ((Stack) this.mStacks.get(tabId)).lastElement(), false, false);
    }

    public void pushFragment(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd) {
            ((Stack) mStacks.get(tag)).push(fragment);
        }
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate) {
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        ft.replace(R.id.container, fragment);
        ft.commit();
        manager.executePendingTransactions();
    }

    public void popFragment() {
        Fragment fragment = (Fragment) ((Stack) mStacks.get(mCurrentTab)).elementAt(((Stack) mStacks.get(mCurrentTab)).size() - 2);
        ((Stack) mStacks.get(mCurrentTab)).pop();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.container, fragment);
        ft.commit();
    }


    public void onBackPressed() {

        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStack();
        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
//            super.onBackPressed();

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.str_press_back_again), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }

       /* try {
            if (drawerLayout.isDrawerOpen((int) GravityCompat.START)) {
                drawerLayout.closeDrawer((int) GravityCompat.START);
            } else if (((BaseFragment) ((Stack) mStacks.get(mCurrentTab)).lastElement()).onBackPressed()) {
                Logger.m5e("BackPress Manage By Fragment");
            } else if (((Stack) mStacks.get(mCurrentTab)).size() != 1) {
                popFragment();
            } else if (this.mCurrentTab.equals(AppConstants.TAB_ANALYTIC)) {
                try {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                        getSupportFragmentManager().popBackStack();
                    } else if (doubleBackToExitPressedOnce) {
                        finish();
                    } else {
                       doubleBackToExitPressedOnce = true;
                        Toast.makeText(this, getString(R.string.prompt_exit), Toast.LENGTH_LONG).show();
                        new Handler().postDelayed(new C05703(), 2000);
                    }
                } catch (Exception e) {
                    super.onBackPressed();
                }
            } else {
                mCurrentTab = AppConstants.TAB_ANALYTIC;
                navigation.getMenu().getItem(1).setChecked(true);
                navigation.setSelectedItemId(R.id.navigation_analytics);
            }
        } catch (Exception e2) {
            super.onBackPressed();
        }*/
    }

    private void openClickedNavigationDrawerMenuItem(String title) {
        boolean z = true;
        switch (title.hashCode()) {
            case -2101533192:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_IN_ACTIVE_LEADS)) {
                    z = true;
                    break;
                }
                break;
            case -2071445918:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_EMAIL_MARKETING)) {
                    z = true;
                    break;
                }
                break;
            case -2013462102:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LOGOUT)) {
                    z = true;
                    break;
                }
                break;
            case -1219698498:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_EMAIL_TEMPLATE)) {
                    z = true;
                    break;
                }
                break;
            case -79022321:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_CAR_BUILDER)) {
                    z = true;
                    break;
                }
                break;
            case 2245473:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_HELP)) {
                    z = true;
                    break;
                }
                break;
            case 70791782:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_EMAIL_INBOX)) {
                    z = true;
                    break;
                }
                break;
            case 92169981:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_ACTIVE_LEADS)) {
                    z = false;
                    break;
                }
                break;
            case 895701579:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_SOLD_LEADS)) {
                    z = true;
                    break;
                }
                break;
            case 1382865118:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_CONTACT_LIST)) {
                    z = true;
                    break;
                }
                break;
            case 1396656103:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_DEAL_MANAGER_DEAL_BUILDER)) {
                    z = true;
                    break;
                }
                break;
            case 1696047835:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_LEAD_MANAGER_ADD_NEW_LEAD)) {
                    z = true;
                    break;
                }
                break;
            case 2000971769:
                if (title.equals(NavigationDrawerDataFactory.NAV_MENU_DEAL_MANAGER_DEAL_MANAGER)) {
                    z = true;
                    break;
                }
                break;
        }
        /*switch (z) {
            case false:
                pushFragment(this.mCurrentTab, new LeadListHolderFragment(), false, true);
                return;
            case true:
                SoldInactiveLeadListFragment inactiveLeadListFragment = new SoldInactiveLeadListFragment();
                Bundle bundleInactiveLead = new Bundle();
                bundleInactiveLead.putString(SoldInactiveLeadListFragment.BUNDLE_LEAD_STATUS_TYPE, getResources().getString(R.string.inactive_dead_leads));
                inactiveLeadListFragment.setArguments(bundleInactiveLead);
                pushFragment(this.mCurrentTab, inactiveLeadListFragment, false, true);
                return;
            case true:
                SoldInactiveLeadListFragment soldLeadListFragment = new SoldInactiveLeadListFragment();
                Bundle bundleSoldLead = new Bundle();
                bundleSoldLead.putString(SoldInactiveLeadListFragment.BUNDLE_LEAD_STATUS_TYPE, getResources().getString(R.string.sold_leads));
                soldLeadListFragment.setArguments(bundleSoldLead);
                pushFragment(this.mCurrentTab, soldLeadListFragment, false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new EmailInboxListFragment(), false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new DealManagerListFragment(), false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new DealListHolderFragment(), true, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new ContactListFragment(), false, true);
                return;
            case true:
                pushFragment(this.mCurrentTab, new CarListHolderFragment(), false, true);
                return;
            default:
                return;
        }*/
    }
}
