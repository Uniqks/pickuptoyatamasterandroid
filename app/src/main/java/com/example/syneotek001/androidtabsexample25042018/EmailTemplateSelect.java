package com.example.syneotek001.androidtabsexample25042018;

import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterEmailTemplateItems;

public class EmailTemplateSelect extends AppCompatActivity implements AdapterEmailTemplateItems.ItemClick {
    ListView lvEmailTemplateItems;
    AdapterEmailTemplateItems adapter;
    String [] arrTitle;
//    int [] arrImages;
TypedArray arrImages;
    ImageView ivBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_template_select);
        lvEmailTemplateItems = findViewById(R.id.lvEmailTemplateItems);
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        arrTitle = getResources().getStringArray(R.array.arr_title_email_template_items);
        arrImages = getResources().obtainTypedArray(R.array.arr_img_email_template_items);
        adapter = new AdapterEmailTemplateItems(this,arrTitle,arrImages);
        adapter.setItemClick(this);
        lvEmailTemplateItems.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(this,arrTitle[position]+" clicked",Toast.LENGTH_SHORT).show();
    }
}
