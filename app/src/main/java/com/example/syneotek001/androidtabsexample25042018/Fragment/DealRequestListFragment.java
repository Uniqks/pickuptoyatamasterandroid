package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.DealRequestItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestModel;

import java.util.ArrayList;

public class DealRequestListFragment extends Fragment {
    DealRequestItemAdapter mAdapter;
    ArrayList<DealRequestModel> mDealRequestList = new ArrayList<>();
    public RecyclerView recyclerView;
    public TextView tvNoDealRequestFound;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_deal_request_list, container, false);
        recyclerView = mview.findViewById(R.id.recyclerView);
        tvNoDealRequestFound = mview.findViewById(R.id.tvNoDealRequestFound);
        setUpRecyclerView();
        setHasOptionsMenu(false);
        return mview;
    }

    private void setUpRecyclerView() {
        mDealRequestList.add(new DealRequestModel("Abc Patel", "158926194655", "1"));
        mDealRequestList.add(new DealRequestModel("Xyz Shah", "149556559955", "2"));
        mDealRequestList.add(new DealRequestModel("Test 123", "168946121555", "3"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new DealRequestItemAdapter(getActivity(), this.mDealRequestList);
        recyclerView.setAdapter(mAdapter);
        refreshData();
    }

    private void refreshData() {
        if (mDealRequestList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoDealRequestFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoDealRequestFound.setVisibility(View.VISIBLE);
    }
}
