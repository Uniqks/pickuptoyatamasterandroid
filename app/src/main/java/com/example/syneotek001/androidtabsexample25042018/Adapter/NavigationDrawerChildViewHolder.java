package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnExpandableRecyclerViewChildItemClickListener;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
class NavigationDrawerChildViewHolder extends ChildViewHolder {
    private TextView childTextView;
    private OnExpandableRecyclerViewChildItemClickListener mCallBack;

    class C05611 implements OnClickListener {
        C05611() {
        }

        public void onClick(View view) {
            if (NavigationDrawerChildViewHolder.this.mCallBack != null) {
                NavigationDrawerChildViewHolder.this.mCallBack.onChildItemClicked(String.valueOf(view.getTag()));
            }
        }
    }

    NavigationDrawerChildViewHolder(View itemView) {
        super(itemView);
        this.childTextView = (TextView) itemView.findViewById(R.id.list_item_child_name);
    }

    void setArtistName(String name, OnExpandableRecyclerViewChildItemClickListener listener) {
        this.childTextView.setText(name);
        this.childTextView.setTag(name);
        this.mCallBack = listener;
        this.childTextView.setOnClickListener(new C05611());
    }
}
