package com.example.syneotek001.androidtabsexample25042018.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
public class DealsFragment extends Fragment {

   ImageView iv_Close;
   TextView tv_btn_master_crm,tv_btn_business_manager,tv_deals,tv_btn_own_deals,tv_action,tv_deal_no,tv_deal_not,tv_deliverd,tv_status;
   Button tv_btn_details,tv_btn_send_business;
   LinearLayout staus_layout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_deals, container, false);
        iv_Close=view.findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        tv_btn_master_crm=view.findViewById(R.id.tv_btn_master_crm);
        tv_btn_business_manager=view.findViewById(R.id.tv_btn_business_manager);
        tv_btn_own_deals=view.findViewById(R.id.tv_btn_own_deals);
        staus_layout=view.findViewById(R.id.staus_layout);

        tv_deal_no=view.findViewById(R.id.tv_deal_no);
        tv_deals=view.findViewById(R.id.tv_deals);
        tv_deal_not=view.findViewById(R.id.tv_deal_not);
        tv_deliverd=view.findViewById(R.id.tv_deliverd);
        tv_status=view.findViewById(R.id.tv_status);
        tv_action=view.findViewById(R.id.tv_action);

        tv_btn_details=view.findViewById(R.id.tv_btn_details);
        tv_btn_send_business=view.findViewById(R.id.tv_btn_send_business);

        tv_btn_master_crm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tv_btn_master_crm.setBackgroundColor(Color.RED);
                tv_btn_business_manager.setBackgroundColor(Color.GRAY);
                tv_btn_own_deals.setBackgroundColor(Color.GRAY);
                tv_deal_not.setVisibility(View.INVISIBLE);

                tv_btn_send_business.setVisibility(View.VISIBLE);
                tv_deal_not.setVisibility(View.GONE);
                tv_deliverd.setVisibility(View.GONE);






            }
        });
        tv_btn_business_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_btn_business_manager.setBackgroundColor(Color.RED);
                tv_btn_master_crm.setBackgroundColor(Color.GRAY);
                tv_btn_own_deals.setBackgroundColor(Color.GRAY);

                tv_deals.setVisibility(View.VISIBLE);
                tv_deal_no.setVisibility(View.VISIBLE);
                tv_status.setVisibility(View.VISIBLE);
                tv_deliverd.setVisibility(View.VISIBLE);
                tv_btn_details.setVisibility(View.VISIBLE);
                tv_action.setVisibility(View.VISIBLE);

                tv_btn_send_business.setVisibility(View.INVISIBLE);
                tv_deal_not.setVisibility(View.INVISIBLE);
                tv_deal_not.setVisibility(View.GONE);

            }
        });

        tv_btn_own_deals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_btn_own_deals.setBackgroundColor(Color.RED);
                tv_btn_master_crm.setBackgroundColor(Color.GRAY);
                tv_btn_business_manager.setBackgroundColor(Color.GRAY);

                tv_deals.setVisibility(View.VISIBLE);
                tv_action.setVisibility(View.VISIBLE);
                tv_deal_not.setVisibility(View.VISIBLE);
                tv_deals.setVisibility(View.VISIBLE);
                tv_deliverd.setVisibility(View.INVISIBLE);


                tv_status.setVisibility(View.VISIBLE);
                tv_btn_details.setVisibility(View.INVISIBLE);
                tv_btn_details.setVisibility(View.GONE);

                tv_btn_send_business .setVisibility(View.INVISIBLE);
                tv_btn_send_business.setVisibility(View.GONE);

                tv_deal_no.setVisibility(View.INVISIBLE);
                tv_deal_no.setVisibility(View.GONE);
            }
        });


        return view;
    }


}
