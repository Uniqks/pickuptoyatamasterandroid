package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.example.syneotek001.androidtabsexample25042018.Fragment.AddTaskFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealRequestFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.DealsFragment;
//import com.example.syneotek001.androidtabsexample25042018.Fragment.TaskListFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LeadItemModel;

import java.util.ArrayList;
import java.util.Iterator;

public class LeadItemAdapter extends RecyclerView.Adapter<LeadItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<LeadItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivInfo:
                if (LeadItemAdapter.this.onItemClickListener != null) {
                    LeadItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivLeadLabel:
                if (LeadItemAdapter.this.onItemClickListener != null) {
                    LeadItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivEmail:
                if (LeadItemAdapter.this.onItemClickListener != null) {
                    LeadItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivSchedule:
                if (LeadItemAdapter.this.onItemClickListener != null) {
                    LeadItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivEmail, ivInactive, ivInfo, ivLeadLabel, ivSchedule;
        LinearLayout llActionItems, llRequest;
        LinearLayout ll_LastActivityDate, ll_Email, ll_Phone, ll_SourceLink;
        TextView tvAssignDate, tvBusinessRequest, tvCustomerId, tvDealRequest, tvEmail, tvLastActivityDate, tvName, tvPhone, tvSourceLink;
        View viewDummySpace;
        TextView ivAddTask, ivTask,ivdeal;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivEmail = view.findViewById(R.id.ivEmail);
            ivInactive = view.findViewById(R.id.ivInactive);
            ivInfo = view.findViewById(R.id.ivInfo);
            ivLeadLabel = view.findViewById(R.id.ivLeadLabel);
            ivSchedule = view.findViewById(R.id.ivSchedule);
            llActionItems = view.findViewById(R.id.llActionItems);
            llRequest = view.findViewById(R.id.llRequest);
            tvAssignDate = view.findViewById(R.id.tvAssignDate);
            tvBusinessRequest = view.findViewById(R.id.tvBusinessRequest);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvDealRequest = view.findViewById(R.id.tvDealRequest);
            tvEmail = view.findViewById(R.id.tvEmail);
            tvLastActivityDate = view.findViewById(R.id.tvLastActivityDate);
            tvName = view.findViewById(R.id.tvName);
            tvPhone = view.findViewById(R.id.tvPhone);
            tvSourceLink = view.findViewById(R.id.tvSourceLink);
            viewDummySpace = view.findViewById(R.id.viewDummySpace);

            ll_LastActivityDate = view.findViewById(R.id.ll_LastActivityDate);
            ll_Email = view.findViewById(R.id.ll_Email);
            ll_Phone = view.findViewById(R.id.ll_Phone);
            ll_SourceLink = view.findViewById(R.id.ll_SourceLink);

            tvDealRequest = view.findViewById(R.id.tvDealRequest);

            ivAddTask = view.findViewById(R.id.ivAddTask);
            ivTask = view.findViewById(R.id.ivTask);
            ivdeal = view.findViewById(R.id.ivdeal);

        }
        @Override
        public void onClick(View v) {

        }
    }

    public LeadItemAdapter(Context context, ArrayList<LeadItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }



    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lead, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        LeadItemModel mLeadItem = (LeadItemModel) mData.get(position);
        holder.tvCustomerId.setText(mLeadItem.getCustomerId());
        holder.tvAssignDate.setText(TimeStamp.millisToTimeFormat(mLeadItem.getAssignDate()));
        holder.tvLastActivityDate.setText(TimeStamp.millisToTimeFormat(mLeadItem.getLastActivity()));
        holder.tvName.setText(mLeadItem.getClientName());
        holder.tvEmail.setText(mLeadItem.getEmail());
        holder.tvPhone.setText(mLeadItem.getPhone());
        holder.tvSourceLink.setText(mLeadItem.getSourceLink());
        String leadLabel = mLeadItem.getLeadLabel();
        int i = -1;
        switch (leadLabel.hashCode()) {
            case 49:
                if (leadLabel.equals("1")) {
                    i = 0;
                    break;
                }
                break;
            case 50:
                if (leadLabel.equals("2")) {
                    i = 1;
                    break;
                }
                break;
            case 51:
                if (leadLabel.equals("3")) {
                    i = 2;
                    break;
                }
                break;
            case 52:
                if (leadLabel.equals("4")) {
                    i = 3;
                    break;
                }
                break;
            case 53:
                if (leadLabel.equals("5")) {
                    i = 4;
                    break;
                }
                break;
        }
        switch (i) {
            case 0:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_hot);
                break;
            case 1:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_warm);
                break;
            case 2:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_cold);
                break;
            case 3:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_dead);
                break;
            case 4:
                holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_closed);
                break;
        }
        Iterator it;
        ArrayList<View> viewList;
        if (mLeadItem.isSelected()) {
            viewList = new ArrayList<>();
            viewList.add(holder.ll_LastActivityDate);
            viewList.add(holder.ll_Email);
            viewList.add(holder.ll_Phone);
            viewList.add((holder.ll_SourceLink));
            viewList.add(holder.viewDummySpace);
            viewList.add(holder.llActionItems);
            viewList.add(holder.llRequest);
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        viewList.add(holder.ll_LastActivityDate);
        viewList.add(holder.ll_Email);
        viewList.add(holder.ll_Phone);
        viewList.add(holder.ll_SourceLink);
        viewList.add(holder.viewDummySpace);
        viewList.add(holder.llActionItems);
        viewList.add(holder.llRequest);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    LeadItemModel mLeadItem = (LeadItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });

        holder.ivAddTask.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((HomeActivity) mContext).replaceFragment(new AddTaskFragment());
            }
        });

        holder.ivTask.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((HomeActivity) mContext).replaceFragment(new TaskListFragment());
            }
        });
        holder.tvDealRequest.setOnClickListener(this);
        holder.tvBusinessRequest.setOnClickListener(this);
        holder.ivLeadLabel.setOnClickListener(this);
        holder.ivInfo.setOnClickListener(this);
        holder.ivEmail.setOnClickListener(this);
        holder.ivSchedule.setOnClickListener(this);
        holder.ivInactive.setOnClickListener(this);

        holder.tvDealRequest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((HomeActivity) mContext).replaceFragment(new DealRequestFragment());
            }
        });

        holder.ivdeal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((HomeActivity) mContext).replaceFragment(new DealsFragment());
            }
        });



    }

  /*  public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivInfo:
                if (LeadItemAdapter.this.onItemClickListener != null) {
                    LeadItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivLeadLabel:
                if (LeadItemAdapter.this.onItemClickListener != null) {
                    LeadItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }*/


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
