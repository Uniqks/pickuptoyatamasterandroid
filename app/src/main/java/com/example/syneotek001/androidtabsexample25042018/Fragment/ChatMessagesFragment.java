package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.ChatMessageItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.ChatListResponse.ChatListData;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageModel;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageResponse;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageResponse.ChatMessageData;
import com.example.syneotek001.androidtabsexample25042018.model.SendMessageResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SendMessageResponse.ChatMessageData.MessageData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiClient;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;

public class ChatMessagesFragment extends Fragment implements OnClickListener, OnBackPressed, ApiResponseInterface {
    public static String BUNDLE_CHAT_FRIEND_MODEL = "chat_friend_data";
    private Animation animScaleIn;
    private Animation animScaleOut;
    ChatMessageItemAdapter mAdapter;
    ArrayList<ChatMessageData> mChatMessageList = new ArrayList<>();
    ChatListData mChatModel;

    public EditText etMessage;
    public ImageView ivBack, ivFriendImage, ivOnlineIndicator, ivCamera, ivEmoticon, ivSendMessage, ivUploadFile;
    public LinearLayout llChatBox;
    public RecyclerView recyclerViewMessage;
    public RelativeLayout rel_NoMessageFound;
    public View viewDivider, viewDividerTop;
    TextView tvFriendName, tvLastSeen, tvNoMessageFound, tvFriendLetter;
    ApiManager apiManager;

    private int STORAGE_PERMISSION_CODE=11;
    String[] PERMISSIONS = { Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final int TAKENPHOTO = 0;
    public static final int SELECT_PHOTO = 1;
    Uri uri;
    File photofile;
    boolean isUpload;
    public String photo_path="";

    int fromlimit = 1;
    Handler handler;
    boolean loadMore = false;

    String friendId, friendName, friendImage;
    String strBase64Img;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.activity_chat_message_list, container, false);

        apiManager = new ApiManager(getActivity(), this);
        etMessage = mview.findViewById(R.id.etMessage);
        ivCamera = mview.findViewById(R.id.ivCamera);
        ivEmoticon = mview.findViewById(R.id.ivEmoticon);
        ivSendMessage = mview.findViewById(R.id.ivSendMessage);
        ivUploadFile = mview.findViewById(R.id.ivUploadFile);
        llChatBox = mview.findViewById(R.id.llChatBox);
        recyclerViewMessage = mview.findViewById(R.id.recyclerViewMessage);
        viewDivider = mview.findViewById(R.id.viewDivider);
        rel_NoMessageFound = mview.findViewById(R.id.rel_NoMessageFound);
        ivBack = mview.findViewById(R.id.ivBack);
        ivFriendImage = mview.findViewById(R.id.ivFriendImage);
        tvFriendName = mview.findViewById(R.id.tvFriendName);
        tvLastSeen = mview.findViewById(R.id.tvLastSeen);
        ivOnlineIndicator = mview.findViewById(R.id.ivOnlineIndicator);
        tvNoMessageFound = mview.findViewById(R.id.tvNoMessageFound);
        tvFriendLetter = mview.findViewById(R.id.tvFriendLetter);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        animScaleIn = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale_in);
        animScaleOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale_out);

        if (getArguments() != null) {
            this.mChatModel = (ChatListData) getArguments().getSerializable(BUNDLE_CHAT_FRIEND_MODEL);
            setupChatToolBarWithBackArrow(mChatModel, new GlideUtils(getActivity()));
            friendName = mChatModel.getFriendname();
            friendImage = mChatModel.getFriendimage();
            if (getArguments().getBoolean(Utils.EXTRA_IS_FROM_CHAT))
                friendId = mChatModel.getFriend();
            else
                friendId = mChatModel.getUser_id();
        }

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setReverseLayout(true);
        recyclerViewMessage.setLayoutManager(lm);
        recyclerViewMessage.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
        mAdapter = new ChatMessageItemAdapter(getActivity(), mChatMessageList, recyclerViewMessage, friendName, friendId, friendImage);

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                Log.e("ChatListFragment", " onLoadMore ");

                //add null , so the adapter will check view_type and show progress bar at bottom
                mChatMessageList.add(null);
                recyclerViewMessage.post(new Runnable() {
                    public void run() {
                        mAdapter.notifyItemInserted(mChatMessageList.size() - 1);
                    }
                });
                handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //   remove progress item
                        if (mChatMessageList.size() > 0)
                            mChatMessageList.remove(mChatMessageList.size() - 1);

                        mAdapter.notifyItemRemoved(mChatMessageList.size());

                        if (Utils.isOnline()) {
                            fromlimit = fromlimit + 1;
                            loadMore = true;
                            apiManager.getChatMessages(fromlimit, AppConstant.CHAT_MESSAGE_LIST_ACTION, Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID), friendId);

                        } else {
                            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                        }

                    }
                }, 1000);
            }
        });

        setUpRecyclerView();
        setEditTexEvent();
        setClickEvents();
        setHasOptionsMenu(false);



        /*SimpleDateFormat sdf3 = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);

        Date d1 = null;
        try{
            d1 = sdf3.parse(String.valueOf(new Date()));

        }catch (Exception e){ e.printStackTrace(); }


        System.out.println("check..." + d1);

        Log.e("ChatMessagesFragment"," onCreateView current time "+d1);*/

        return mview;
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void isError(String errorCode) {

    }

    @Override
    public void isSuccess(Object response, int ServiceCode) {

        if (ServiceCode == AppConstant.GET_CHAT_MESSAGES_LIST_REQUEST) {
            ChatMessageResponse chatListResponse = (ChatMessageResponse) response;
            Log.e("loadmore_Count", "" + loadMore);

            final List<ChatMessageData> taxiList = chatListResponse.getData();

            if (taxiList.size() > 0) {

                mChatMessageList.addAll(taxiList);

                if (loadMore) {
                    Log.e("loadmore_Count", "if");
                    mAdapter.notifyItemInserted(mChatMessageList.size());
                    mAdapter.notifyDataSetChanged();
                    mAdapter.setLoaded();
                    LogUtils.i("BookingHistory" + " getTripHistory onResponse loadMore arr_myrequestlist.size() " + mChatMessageList.size());

                } else {

                    Log.e("loadmore_Count", "else");
                    recyclerViewMessage.setVisibility(View.VISIBLE);

                    Log.i("json", mChatMessageList.size() + " arr_myrequestlist ");
                    recyclerViewMessage.setAdapter(mAdapter);
                    mAdapter.setLoaded();

                    LogUtils.i("BookingHistory" + " getTripHistory onResponse !loadMore arr_myrequestlist.size() " + mChatMessageList.size());

                }

            } else {

                if (!loadMore) {

                    if (mChatMessageList.isEmpty()) {
                        recyclerViewMessage.setVisibility(View.GONE);
                        tvNoMessageFound.setVisibility(View.VISIBLE);

                    } else {
                        recyclerViewMessage.setVisibility(View.VISIBLE);
                        tvNoMessageFound.setVisibility(View.GONE);
                    }
                }
            }
        } else if (ServiceCode == AppConstant.SEND_TEXT_MESSAGE_REQUEST) {

            SendMessageResponse sendMessageResponse = (SendMessageResponse) response;

            if (sendMessageResponse.getData().getMsg_data()!=null) {

                MessageData messageData = sendMessageResponse.getData().getMsg_data();

                mChatMessageList.add(0, new ChatMessageResponse().new ChatMessageData(messageData.getId(), messageData.getMessage(), messageData.getTime(), messageData.getUser_id()
                        , messageData.getReceiver(), messageData.getStorage_a(), messageData.getStorage_b(), messageData.getStatus(), messageData.getMsg_type(), messageData.getIs_received()));

                mAdapter.notifyDataSetChanged();
                etMessage.setText("");
                if (mChatMessageList.size() == 1) {
                    refreshData();
                    return;
                }

            }


//            Fri Aug 10 15:56:01 GMT+05:30 2018

            /*SimpleDateFormat fmt = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
            Date date = null;
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yyyy, HH:mm");

            try {
                date = fmt.parse(String.valueOf(new Date()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.e("ChatMessagesFragment", " onCreateView current time " + fmtOut.format(date));

            mChatMessageList.add(0, new ChatMessageResponse().new ChatMessageData(String.valueOf(mChatMessageList.size() + 1), etMessage.getText().toString(), fmtOut.format(date), Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID)
                    , friendId, "", "", "", AppConstant.CHAT_MESSAGE_TYPE_TEXT, ""));

            mAdapter.notifyDataSetChanged();
            etMessage.setText("");
            if (mChatMessageList.size() == 1) {
                refreshData();
                return;
            }*/
        }


    }

    class C05811 implements TextWatcher {

        class C05801 implements AnimationListener {
            C05801() {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ivSendMessage.setVisibility(View.VISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }
        }

        C05811() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.length() == 1 && ivSendMessage.getVisibility() != View.VISIBLE) {
                ivSendMessage.startAnimation(animScaleOut);
                animScaleOut.setAnimationListener(new C05801());
            } else if (charSequence.length() == 0 && ivSendMessage.getVisibility() != View.GONE) {
                ivSendMessage.startAnimation(animScaleIn);
                ivSendMessage.setVisibility(View.GONE);
            }
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    class C05822 implements OnClickListener {
        C05822() {
        }

        public void onClick(View view) {
            getActivity().onBackPressed();
        }
    }


    private void setClickEvents() {
        ivSendMessage.setOnClickListener(this);
        ivEmoticon.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        ivUploadFile.setOnClickListener(this);
    }

    private void setEditTexEvent() {
        etMessage.addTextChangedListener(new C05811());
    }

    private void setUpRecyclerView() {
        /*if (getArguments() != null) {
            this.mChatModel = (ChatModel) getArguments().getSerializable(BUNDLE_CHAT_FRIEND_MODEL);
            setupChatToolBarWithBackArrow(mChatModel, new GlideUtils(getActivity()));
            mChatMessageList.add(new ChatMessageModel(mChatModel.getName(), this.mChatModel.getImage(), "Hi", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Hi", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel("You", "", "Hello", 1526119235, ChatMessageModel.Type.SENT));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "How are you?", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel("You", "", "I am fine. \nWhat about you?", 1526119235, ChatMessageModel.Type.SENT));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Good!", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Let's go for party this weekend. \nSince long time we didn't do any party. \nWhat you think?", 1526119235, ChatMessageModel.Type.RECEIVED));
            LinearLayoutManager lm = new LinearLayoutManager(getActivity());
            lm.setReverseLayout(true);
            recyclerViewMessage.setLayoutManager(lm);
            recyclerViewMessage.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
            mAdapter = new ChatMessageItemAdapter(getActivity(), mChatMessageList);
            recyclerViewMessage.setAdapter(mAdapter);
            refreshData();
            return;
        }
        Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();*/

        if (Utils.isOnline()) {

            loadMore = false;
            mChatMessageList.clear();
            LogUtils.i("BookingHistory" + " loadData arr_myrequestlist size " + mChatMessageList.size());
            //getTripHistory(loadMore, 0, pos/*,TripHistoryActivity.this*/);
            apiManager.getChatMessages(fromlimit, AppConstant.CHAT_MESSAGE_LIST_ACTION, Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID), friendId);
        } else {

            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
        }

    }

    private void refreshData() {
        if (mChatMessageList.size() > 0) {
            recyclerViewMessage.setVisibility(View.VISIBLE);
            rel_NoMessageFound.setVisibility(View.GONE);
            return;
        }
        recyclerViewMessage.setVisibility(View.GONE);
        rel_NoMessageFound.setVisibility(View.VISIBLE);
        tvNoMessageFound.setText(getResources().getString(R.string.no_message_found));
        tvNoMessageFound.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vector_no_chats, 0, 0);
    }

    public void setupChatToolBarWithBackArrow(ChatListData chatModel, GlideUtils glideUtils) {
        /*title = tvFriendName;
        TextView tvLastSeen =tvLastSeen;
        TextView tvFriendLetter = tvFriendLetter;
        ImageView ivBack = ivBack;
        ImageView ivFriendImage = ivFriendImage;
        ImageView ivOnlineIndicator = ivOnlineIndicator;*/
        tvFriendName.setText(chatModel.getFriendname() != null ? chatModel.getFriendname() : "");
//        tvLastSeen.setText(chatModel.getSession_status().equals("online") ? getResources().getString(R.string.online) : getResources().getString(R.string.last_seen_, new Object[]{TimeStamp.millisToTimeFormat(Long.valueOf(chatModel.getLastActive() * 1000))}));
        ivOnlineIndicator.setVisibility(chatModel.getSession_status().equals("online") ? View.VISIBLE : View.GONE);
        if (chatModel.getFriendimage() == null || chatModel.getFriendimage().length() <= 0) {
            String title = chatModel.getFriendname().toUpperCase();
            if (title.length() > 0) {
                tvFriendLetter.setText(title.substring(0, 1).toUpperCase());
            } else {
                tvFriendLetter.setText("T");
            }
            glideUtils.loadImageCircular("", ivFriendImage, R.drawable.shape_circle_filled_white);
            tvFriendLetter.setVisibility(View.VISIBLE);
        } else {
            glideUtils.loadImageCircular(ApiClient.SALES_MAN_PROFILE_IMG + chatModel.getFriendimage(), ivFriendImage);
            tvFriendLetter.setVisibility(View.GONE);
        }
        ivBack.setOnClickListener(new C05822());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivSendMessage:
                if (etMessage.length() > 0) {
//                    mChatMessageList.add(0, new ChatMessageModel("You", "", etMessage.getText().toString(), new Date().getTime() / 1000, ChatMessageModel.Type.SENT));

                    if (Utils.isOnline()) {
                        apiManager.sendChatMessages(AppConstant.CHAT_MESSAGE_SEND_ACTION, etMessage.getText().toString(), Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID), friendId, AppConstant.CHAT_MESSAGE_TYPE_TEXT);
                    } else {
                        Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                    }


                    return;
                }
                return;
            case R.id.ivCamera:
                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions( getActivity(),PERMISSIONS, STORAGE_PERMISSION_CODE);
                }
                else {
                    show_popup();
                }
                return;
            default:
                return;
        }
    }

    public interface OnLoadMoreListener {

        void onLoadMore();

    }

    public void show_popup() {

        LogUtils.i("SPRegisterInfo"+" show_popup ");

        String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE/*,Manifest.permission.CAMERA*//*,Manifest.permission.MEDIA_CONTENT_CONTROL*/};

        {

            LogUtils.i("SPRegisterInfo"+" show_popup if (EasyPermissions.hasPermissions(this, perms)) ");

            PopupMenu popup = new PopupMenu(getActivity(), ivCamera);
            popup.getMenuInflater().inflate(R.menu.select_image, popup.getMenu());

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.camera:

                            LogUtils.i("camera", "camera");
                            takePhotoFromCamera();

                            break;

                        case R.id.gallery:

                            LogUtils.i("gallery", "gallery");

                            LogUtils.i("show_popup " + "gallery");
                            Intent intent = new Intent(Intent.ACTION_PICK);
                            intent.setType("image/*");
                            startActivityForResult(Intent.createChooser(intent, "Complete action using"), SELECT_PHOTO);
                            break;
                    }

                    return true;
                }
            });

            popup.show();

        }
       /* else
            {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.requires_storage),
                    RC_READ_STORAGE_PERM, perms);
        }*/

    }
    private void takePhotoFromCamera() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            photofile = Utils.getInstance(getActivity()).create_file(".jpg");
            cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            uri = FileProvider.getUriForFile(getActivity(), "com.pickeringtoyota.mastercrm.fileprovider", photofile);
        }
        else
        {
            photofile = Utils.getInstance(getActivity()).create_Image_File(".jpg");
            uri = Uri.fromFile(photofile);

        }
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(Intent.createChooser(cameraIntent, getString(R.string.lbl_capture)), TAKENPHOTO);


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if(requestCode == STORAGE_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            show_popup();
            //takePhotoFromCamera() ;
        else{

            Utils.getInstance(getActivity()).Toast(getActivity(),getString(R.string.enable_perm));
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == TAKENPHOTO) && (resultCode == Activity.RESULT_OK)) {
           /* try {

                photo_path = imageUri.getPath();

                LogUtils.i("", photo_path + " TAKENPHOTO " + imageUri);
                isUpload=true;

                Picasso.with(this).load(new File(photo_path)).placeholder(R.drawable.ub__emptystate_profile)*//*.noFade().fit()*//*.into(binding.imgProfile);

            } catch (Exception e) {
                e.printStackTrace();
            }*/

            if (uri != null) {
                isUpload=true;
                Log.e("photofile",""+photofile);
                try {
                    photofile = new Compressor(getActivity()).compressToFile(photofile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                photo_path= photofile.getAbsolutePath();//uri.getPath();

                LogUtils.e(uri.getPath()+" imageFilePath "+new File(uri.toString()).getAbsolutePath());
                if (photo_path != null && photo_path.length() > 0) {
                    // setImage((imageFilePath));
                    /*Picasso.with(getActivity()).load(new File(photo_path)).placeholder(R.drawable.ub__emptystate_profile).
                            resize(300,300).into(binding.imgProfile);*/

                    Bitmap bitmap;
                    try {
                        bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri));
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                        strBase64Img = ConvertBitmapToString(resizedBitmap);

                        if (Utils.isOnline()) {
                            apiManager.uploadImageChat(AppConstant.CHAT_MESSAGE_SEND_ACTION, strBase64Img, Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID), friendId, AppConstant.CHAT_MESSAGE_TYPE_IMAGE);
                        } else {
                            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                        }

//                        Upload();

                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            } else {

                Utils.getInstance(getActivity()).Toast(getActivity(),getString(R.string.toast_unable_to_selct_image));
            }

        } else if ((requestCode == SELECT_PHOTO) && (resultCode == Activity.RESULT_OK)) {


            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            photo_path = cursor.getString(columnIndex);
            isUpload=true;
            try {
                LogUtils.i("", "SELECT_PHOTO " + photo_path);
//                Utils.getInstance(EditProfile.this).decodeFile(photo_path,600,600);

//                Picasso.with(this).load(new File(photo_path)).placeholder(R.drawable.ub__emptystate_profile)/*.noFade().fit()*/.into(binding.imgProfile);

                Bitmap bitmap;
                try {
                    bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(selectedImage));
                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);
                    strBase64Img = ConvertBitmapToString(resizedBitmap);

                    if (Utils.isOnline()) {
                        apiManager.uploadImageChat(AppConstant.CHAT_MESSAGE_SEND_ACTION, strBase64Img, Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID), friendId, AppConstant.CHAT_MESSAGE_TYPE_IMAGE);
                    } else {
                        Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                    }

//                        Upload();

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    public static String ConvertBitmapToString(Bitmap bitmap){
        String encodedImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        try {
            encodedImage= URLEncoder.encode(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return encodedImage;
    }


}
