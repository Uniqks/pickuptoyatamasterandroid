package com.example.syneotek001.androidtabsexample25042018.Adapter.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;


import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.Calendar;

public class TimePickerDialog extends AppCompatDialogFragment {
    Calendar calendar = Calendar.getInstance();
    View view;

    public Button btnSave;
    public TimePicker timePicker;

    class OnTimePickerClick implements OnClickListener {
        OnTimePickerClick() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            dialog.dismiss();
        }
    }

    class DialogClick implements OnClickListener {
        DialogClick() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            int hour = timePicker.getCurrentHour();
            TimePickerDialog.this.getTargetFragment().onActivityResult(TimePickerDialog.this.getTargetRequestCode(), -1, new Intent().putExtra("hour", hour).putExtra("min", timePicker.getCurrentMinute().intValue()));
            dialog.dismiss();
        }
    }

    class onClickEvent implements View.OnClickListener {
        onClickEvent() {
        }

        public void onClick(View view) {
            int hour = timePicker.getCurrentHour();
            TimePickerDialog.this.getTargetFragment().onActivityResult(TimePickerDialog.this.getTargetRequestCode(), -1, new Intent().putExtra("hour", hour).putExtra("min", timePicker.getCurrentMinute().intValue()));
            TimePickerDialog.this.dismiss();
        }
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (this.view == null) {
            View view = inflater.inflate(R.layout.dialog_time_picker, null, false);
            btnSave = view.findViewById(R.id.btnSave);
            timePicker = view.findViewById(R.id.timePicker);
//            this.mBinding = (DialogTimePickerBinding) DataBindingUtil.inflate(inflater, R.layout.dialog_time_picker, null, false);
//            this.view = this.mBinding.getRoot();
            prepareLayout(getArguments());
        }
        Builder alertBuilder = new Builder(getActivity()).setPositiveButton(getActivity().getResources().getString(R.string.save), new DialogClick()).setNegativeButton(getActivity().getResources().getString(R.string.cancel), new OnTimePickerClick());
        alertBuilder.setView(this.view);
        AlertDialog dialog = alertBuilder.create();
        dialog.show();
        return dialog;
    }

    private void prepareLayout(Bundle bundle) {
        int hour;
        int min;
        if (bundle != null) {
            hour = bundle.getInt("hour");
            min = bundle.getInt("min");
        } else {
            hour = this.calendar.get(Calendar.HOUR);
            min = this.calendar.get(Calendar.MINUTE);
        }
        if (VERSION.SDK_INT >= 23) {
            timePicker.setHour(hour);
            timePicker.setMinute(min);
            return;
        }
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(min);
    }

    private void setClickEvent() {
        btnSave.setOnClickListener(new onClickEvent());
    }
}
