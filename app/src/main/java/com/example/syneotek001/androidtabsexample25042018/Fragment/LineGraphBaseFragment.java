package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;

import com.example.syneotek001.androidtabsexample25042018.BaseFragment;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.MyMarkerView;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener.ChartGesture;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LineGraphBaseFragment extends BaseFragment implements OnChartGestureListener, OnChartValueSelectedListener {
    ArrayList<LineDataSet> dataSet;
    String leadType = "";
    LineChart mChart;
    Typeface typeface;

    protected void setBinding(LineChart mChart) {
        this.mChart = mChart;
    }

    protected void setUpChart(int startYear, int endYear, ArrayList<LineGraphDataModel> leadsEntry) {
        setBasics();
        setXAxisYearValues(startYear, endYear);
        setData(leadsEntry);
        setLegend();
    }

    private void setBasics() {
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawGridBackground(false);
        mChart.getDescription().setEnabled(false);
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setPinchZoom(true);
        MyMarkerView mv = new MyMarkerView(this.mContext, R.layout.custom_marker_view);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
        LimitLine llXAxis = new LimitLine(10.0f, "Index 10");
        llXAxis.setTextColor(ContextCompat.getColor(this.mContext, R.color.white));
        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/montserrat_regular.otf");
//        llXAxis.setTypeface(typeface);
        llXAxis.setLineWidth(4.0f);
        llXAxis.enableDashedLine(10.0f, 10.0f, 0.0f);
        llXAxis.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10.0f);
//        Typeface tf = Typeface.createFromAsset(this.mContext.getAssets(), "montserrat_regular.otf");
        LimitLine ll2 = new LimitLine(0.0f, null);
        ll2.setLineWidth(2.0f);
        ll2.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10.0f);
//        ll2.setTypeface(tf);
        ll2.setTypeface(typeface);
        ll2.setLineColor(ContextCompat.getColor(this.mContext, R.color.white_opacity_80));
        ll2.setTextColor(ContextCompat.getColor(this.mContext, R.color.white));
        YAxis leftAxis = this.mChart.getAxisLeft();
        leftAxis.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        leftAxis.removeAllLimitLines();
        leftAxis.addLimitLine(ll2);
        leftAxis.setAxisMinimum(-10.0f);
        leftAxis.enableGridDashedLine(0.0f, 10.0f, 0.0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setGridColor(ContextCompat.getColor(this.mContext, R.color.white_opacity_25));
        leftAxis.setDrawLimitLinesBehindData(true);
        leftAxis.setTypeface(typeface);
        mChart.getAxisRight().setEnabled(false);
    }

    private void setLegend() {
        mChart.animateX(1500);
        Legend l = this.mChart.getLegend();
        l.setForm(LegendForm.LINE);
        l.setTextColor(ContextCompat.getColor(this.mContext, R.color.white));
        l.setTypeface(typeface);
    }

    private void setData(ArrayList<LineGraphDataModel> leadsEntry) {
        this.dataSet = new ArrayList<>();
        if (this.mChart.getData() == null || ((LineData) this.mChart.getData()).getDataSetCount() <= 0) {
            Iterator it = leadsEntry.iterator();
            while (it.hasNext()) {
                this.dataSet.add(prepareDataSet((LineGraphDataModel) it.next()));
            }
            setDataSet(this.dataSet);
            return;
        }
        for (ILineDataSet objDataSet : ((LineData) this.mChart.getData()).getDataSets()) {
            this.dataSet.add((LineDataSet) objDataSet);
            ((LineDataSet) this.dataSet.get(0)).setValues(((LineGraphDataModel) leadsEntry.get(0)).getValueList());
        }
        ((LineData) this.mChart.getData()).notifyDataChanged();
        this.mChart.notifyDataSetChanged();
    }

    private LineDataSet prepareDataSet(LineGraphDataModel lineGraphModel) {
        LineDataSet dataSet = new LineDataSet(lineGraphModel.getValueList(), lineGraphModel.getTitle());
        dataSet.setDrawIcons(true);
        dataSet.setValueTextColor(-1);
        dataSet.setHighlightEnabled(true);
        dataSet.setColor(lineGraphModel.getColorOpacity80());
        dataSet.setCircleColor(-1);
        dataSet.setLineWidth(1.0f);
        dataSet.setCircleRadius(3.0f);
        dataSet.setDrawCircleHole(true);
        dataSet.setValueTextSize(0.0f);
        dataSet.setDrawFilled(true);
        dataSet.setFormLineWidth(1.5f);
        dataSet.setFormSize(15.0f);
        if (Utils.getSDKInt() >= 18) {
            dataSet.setFillDrawable(lineGraphModel.getFadeDrawable());
        } else {
            dataSet.setFillColor(lineGraphModel.getColorOpacity50());
        }
        return dataSet;
    }

    private void setDataSet(ArrayList<LineDataSet> dataSet) {
        List dataSets = new ArrayList();
        dataSets.addAll(dataSet);
        this.mChart.setData(new LineData(dataSets));
    }

    private void setXAxisYearValues(int startYear, int endYear) {
        StringBuilder yearsBuilder = new StringBuilder();
        for (int i = startYear; i <= endYear; i++) {
            yearsBuilder.append(String.valueOf(i)).append(",");
        }
        String years = yearsBuilder.toString();
        if (years.length() > 1) {
            years = years.substring(0, years.length() - 1);
        }
        final String[] quarters = years.split(",");
        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            public String getFormattedValue(float value, AxisBase axis) {
                return quarters[(int) value];
            }
        };
        XAxis xAxis = this.mChart.getXAxis();
        xAxis.setTextColor(ContextCompat.getColor(this.mContext, R.color.white));
        xAxis.setTypeface(typeface);
        xAxis.setValueFormatter(formatter);
    }

    public void onChartGestureStart(MotionEvent me, ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    public void onChartGestureEnd(MotionEvent me, ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);
        if (lastPerformedGesture != ChartGesture.SINGLE_TAP) {
            this.mChart.highlightValues(null);
        }
    }

    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longPressed.");
    }

    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("LOW HIGH", "low: " + this.mChart.getLowestVisibleX() + ", high: " + this.mChart.getHighestVisibleX());
        Log.i("MIN MAX", "xMin: " + this.mChart.getXChartMin() + ", xMax: " + this.mChart.getXChartMax() + ", yMin: " + this.mChart.getYChartMin() + ", yMax: " + this.mChart.getYChartMax());
    }

    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }
}
