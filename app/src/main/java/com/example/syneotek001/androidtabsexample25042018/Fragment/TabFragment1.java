package com.example.syneotek001.androidtabsexample25042018.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.syneotek001.androidtabsexample25042018.R;
public class TabFragment1 extends Fragment {
    public TabFragment1() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_tab_fragment1, container, false);
        return view;
    }
}
