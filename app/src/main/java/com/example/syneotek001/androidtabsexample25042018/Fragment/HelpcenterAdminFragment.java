package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.HelpcenterAdminItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.HelpCenterAdminItemModel;

import java.util.ArrayList;

public class HelpcenterAdminFragment extends Fragment {
    public static final String BUNDLE_UPDATE_LABEL_POSITION = "position";
    public static final String BUNDLE_UPDATE_LABEL_VALUE = "labelValue";
    public static final int LEAD_LIST_UPDATE_LABEL = 1;
    ArrayList<HelpCenterAdminItemModel> leadItemArray = new ArrayList<>();
    HelpcenterAdminItemAdapter mAdapter;
    View view;
    public RecyclerView recyclerView;
    public TextView tvNoDataFound;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_helpcenter_admin, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
        setUpRecyclerView();
        return view;
    }

    private void setUpRecyclerView() {
        leadItemArray.clear();
        leadItemArray.add(new HelpCenterAdminItemModel("#1268952659","A lightweight, next-generation gas-electric hybrid sub-compact concept car.", "09-07-201809:55", "1"));
        leadItemArray.add(new HelpCenterAdminItemModel("#1268952659","A lightweight, next-generation gas-electric hybrid sub-compact concept car.", "09-07-201809:55", "0"));
        leadItemArray.add(new HelpCenterAdminItemModel("#1268952659","A lightweight, next-generation gas-electric hybrid sub-compact concept car.", "09-07-201809:55", "1"));
        leadItemArray.add(new HelpCenterAdminItemModel("#1268952659","A lightweight, next-generation gas-electric hybrid sub-compact concept car.", "09-07-201809:55", "0"));
        leadItemArray.add(new HelpCenterAdminItemModel("#1268952659","A lightweight, next-generation gas-electric hybrid sub-compact concept car.", "09-07-201809:55", "1"));
        leadItemArray.add(new HelpCenterAdminItemModel("#1268952659","A lightweight, next-generation gas-electric hybrid sub-compact concept car.", "09-07-201809:55", "1"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new HelpcenterAdminItemAdapter(getActivity(), leadItemArray);
        recyclerView.setAdapter(mAdapter);
    }



    @Override
    public void onResume() {
        super.onResume();
        setUpRecyclerView();
    }
}
