package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.ChatMessagesFragment.OnLoadMoreListener;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageResponse.ChatMessageData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiClient;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class ChatMessageItemAdapter extends Adapter {
    private GlideUtils glideUtils;
    private Context mContext;
    ArrayList<ChatMessageData> mData;
    OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = AppConstant.LIST_API_VISIBLE_THRESHHOLD;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    String friendName,friendId,friendImage,userProfileImage,userName;

    private final int VIEW_ITEM_RECEIVED = 0;
    private final int VIEW_ITEM_SENT = 1;
    private final int VIEW_PROG = 2;

    public static class ProgressViewHolder extends ViewHolder {

        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    private class ReceivedViewHolder extends ViewHolder {
        TextView tvFriendName, tvMessage, tvDateTime, tvFriendLetter;
        ImageView ivFriendImage,ivMessage;

        ReceivedViewHolder(View view) {
            super(view);
            tvFriendName = view.findViewById(R.id.tvFriendName);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvFriendLetter = view.findViewById(R.id.tvFriendLetter);
            ivFriendImage = view.findViewById(R.id.ivFriendImage);
            ivMessage = view.findViewById(R.id.ivMessage);
//            this.mBinder = mBinder;
        }
    }

    private class SentViewHolder extends ViewHolder {
        TextView tvMyName, tvMessage, tvDateTime, tvMyLetter;
        ImageView ivMyImage,ivMessage;

        SentViewHolder(View view) {
            super(view);
            tvMyName = view.findViewById(R.id.tvMyName);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvMyLetter = view.findViewById(R.id.tvMyLetter);
            ivMyImage = view.findViewById(R.id.ivMyImage);
            ivMessage = view.findViewById(R.id.ivMessage);
//            this.mBinder = mBinder;
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public ChatMessageItemAdapter(Context mContext, ArrayList<ChatMessageData> mData, RecyclerView recyclerView,String friendName,String friendId,String friendImage) {
        this.mContext = mContext;
        this.mData = mData;
        this.glideUtils = new GlideUtils(mContext);
        this.friendName = friendName;
        this.friendId = friendId;
        this.friendImage = friendImage;

        this.userProfileImage = Utils.getInstance(mContext).getString(Utils.PREF_USER_IMG);
        this.userName = Utils.getInstance(mContext).getString(Utils.PREF_USER_NAME);

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    Log.e("onLoadMoreListener"," "+onLoadMoreListener+" "+totalItemCount+" "+lastVisibleItem+" "+visibleThreshold+" "+loading);

                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        Log.e("onLoadMoreListener","if");
                        if (onLoadMoreListener != null) {
                            Log.e("onLoadMoreListener","if2");
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });

        }

    }

    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*switch (viewType) {
            case 0:
                View rview = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_message_received, parent, false);
                return new ReceivedViewHolder(rview);
            default:
                View sview = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_message_sent, parent, false);
                return new SentViewHolder(sview);
        }*/

        ViewHolder vh;

        if (viewType == VIEW_ITEM_RECEIVED) {
            View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_received, parent, false);
            vh = new ReceivedViewHolder(v);
        } else if (viewType == VIEW_ITEM_SENT) {
            View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_sent, parent, false);
            vh = new SentViewHolder(v);
        } else {
            View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_item, parent, false);
            vh = new ProgressViewHolder(v);
        }

        return vh;

    }

//    7/8/2018, 06:09

    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatMessageData mMessage = (ChatMessageData) this.mData.get(position);
        String title;
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy, HH:mm");
        Date date = null;
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yyyy - hh:mm a");
        switch (holder.getItemViewType()) {
            case 0:
                ReceivedViewHolder receivedHolder = (ReceivedViewHolder) holder;

                Log.e("ChatMessageItemAdapter"," onBindViewHolder receivedHolder position "+position+" Msg_type "+mMessage.getMsg_type());

                if (mMessage.getMsg_type().equals(AppConstant.CHAT_MESSAGE_TYPE_TEXT)) {
                    receivedHolder.tvMessage.setText(mMessage.getMessage());
                } else if (mMessage.getMsg_type().equals(AppConstant.CHAT_MESSAGE_TYPE_IMAGE)) {
                    Log.e("ChatMessageItemAdapter"," onBindViewHolder receivedHolder Message "+mMessage.getMessage());
                    if (mMessage.getMessage() == null || mMessage.getMessage().length() <= 0) {
                        this.glideUtils.loadImageChat(ApiClient.CHAT_USER_UPLOADS+mMessage.getMessage(), receivedHolder.ivMessage);
                    }
                }



                if (mMessage.getTime() != null && !mMessage.getTime().equals("")) {
                    try {
                        date = fmt.parse(mMessage.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    receivedHolder.tvDateTime.setText(fmtOut.format(date));
//											LogUtils.i("RequestFragment getIncomingRequests"+" loadMore Mbooking_date"+paymentHistory.getMbooking_date());
                }

                if (mMessage.getReceiver().equals(friendId)) {
                    receivedHolder.tvFriendName.setText(friendName);
                    if (friendImage == null || friendImage.length() <= 0) {
                        title = friendName.toUpperCase();
                        if (title.length() > 0) {
                            receivedHolder.tvFriendLetter.setText(title.substring(0, 1).toUpperCase());
                        } else {
                            receivedHolder.tvFriendLetter.setText("T");
                        }
                        receivedHolder.tvFriendLetter.setVisibility(View.VISIBLE);
                        glideUtils.loadImageCircular("", receivedHolder.ivFriendImage, R.drawable.shape_circle_filled_light_gray);
                        return;
                    }
                    this.glideUtils.loadImageCircular(ApiClient.SALES_MAN_PROFILE_IMG+friendImage, receivedHolder.ivFriendImage, R.drawable.shape_circle_filled_light_gray);
                    receivedHolder.tvFriendLetter.setVisibility(View.GONE);
                }
                else {
                    receivedHolder.tvFriendName.setText(mContext.getString(R.string.str_you));
                    if (userProfileImage == null || userProfileImage.length() <= 0) {
                        title = userName.toUpperCase();
                        if (title.length() > 0) {
                            receivedHolder.tvFriendLetter.setText(title.substring(0, 1).toUpperCase());
                        } else {
                            receivedHolder.tvFriendLetter.setText("T");
                        }
                        receivedHolder.tvFriendLetter.setVisibility(View.VISIBLE);
                        glideUtils.loadImageCircular("", receivedHolder.ivFriendImage, R.drawable.shape_circle_filled_light_gray);
                        return;
                    }
                    // change prefix url
                    this.glideUtils.loadImageCircular(ApiClient.SALES_MAN_PROFILE_IMG+userProfileImage, receivedHolder.ivFriendImage, R.drawable.shape_circle_filled_light_gray);
                    receivedHolder.tvFriendLetter.setVisibility(View.GONE);
                }


                return;
            case 1:
                SentViewHolder sentHolder = (SentViewHolder) holder;

                if (mMessage.getTime() != null && !mMessage.getTime().equals("")) {
                    try {
                        date = fmt.parse(mMessage.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    sentHolder.tvDateTime.setText(fmtOut.format(date));
//											LogUtils.i("RequestFragment getIncomingRequests"+" loadMore Mbooking_date"+paymentHistory.getMbooking_date());
                }

                Log.e("ChatMessageItemAdapter"," onBindViewHolder sentHolder position "+position+" Msg_type "+mMessage.getMsg_type());

                if (mMessage.getMsg_type().equals(AppConstant.CHAT_MESSAGE_TYPE_TEXT)) {
                    sentHolder.tvMessage.setText(mMessage.getMessage());
                } else if (mMessage.getMsg_type().equals(AppConstant.CHAT_MESSAGE_TYPE_IMAGE)) {
                    Log.e("ChatMessageItemAdapter"," onBindViewHolder sentHolder Message "+mMessage.getMessage());
                    if (mMessage.getMessage() == null || mMessage.getMessage().length() <= 0) {
                        this.glideUtils.loadImageChat(ApiClient.CHAT_USER_UPLOADS+mMessage.getMessage(), sentHolder.ivMessage);
                    }
                }

                //                sentHolder.tvMessage.setText(mMessage.getMessage());

                if (mMessage.getReceiver().equals(friendId)) {
                    sentHolder.tvMyName.setText(friendName);
                    if (friendImage == null || friendImage.length() <= 0) {
                        title = friendName.toUpperCase();
                        if (title.length() > 0) {
                            sentHolder.tvMyLetter.setText(title.substring(0, 1).toUpperCase());
                        } else {
                            sentHolder.tvMyLetter.setText("Y");
                        }
                        sentHolder.tvMyLetter.setVisibility(View.VISIBLE);
                        this.glideUtils.loadImageCircular("", sentHolder.ivMyImage, R.drawable.shape_circle_filled_light_gray);
                        return;
                    }
                    this.glideUtils.loadImageCircular(ApiClient.SALES_MAN_PROFILE_IMG+friendImage, sentHolder.ivMyImage, R.drawable.shape_circle_filled_light_gray);
                    sentHolder.tvMyLetter.setVisibility(View.GONE);
                } else {
                    sentHolder.tvMyName.setText(mContext.getString(R.string.str_you));
                    if (userProfileImage == null || userProfileImage.length() <= 0) {
                        title = userName.toUpperCase();
                        if (title.length() > 0) {
                            sentHolder.tvMyLetter.setText(title.substring(0, 1).toUpperCase());
                        } else {
                            sentHolder.tvMyLetter.setText("T");
                        }
                        sentHolder.tvMyLetter.setVisibility(View.VISIBLE);
                        glideUtils.loadImageCircular("", sentHolder.ivMyImage, R.drawable.shape_circle_filled_light_gray);
                        return;
                    }
                    // change prefix url
                    this.glideUtils.loadImageCircular(ApiClient.SALES_MAN_PROFILE_IMG+userProfileImage, sentHolder.ivMyImage, R.drawable.shape_circle_filled_light_gray);
                    sentHolder.tvMyLetter.setVisibility(View.GONE);
                }


                return;
            case 2:
                ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
                progressViewHolder.progressBar.setIndeterminate(true);
                return;
        }
    }

    public int getItemCount() {
        return this.mData.size();
    }

    public int getItemViewType(int position) {
        return mData.get(position) == null ? VIEW_PROG : (!((ChatMessageData) this.mData.get(position)).getReceiver().equals(friendId) ? VIEW_ITEM_RECEIVED : VIEW_ITEM_SENT);
    }
}
