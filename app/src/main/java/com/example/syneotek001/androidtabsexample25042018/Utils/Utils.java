package com.example.syneotek001.androidtabsexample25042018.Utils;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Base64;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.main.MainApplication;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiClient;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jatin on 9/13/2016.
 */
public class Utils {
    // Created A Static Retrofit Service Method For Getting reference to the retrofit service method


    public static final String PREF_NAME = "pref_name";
    public static final String PREF_USER_ID = "pref_user_id";
    public static final String PREF_USER_IMG = "pref_user_img";
    public static final String PREF_USER_NAME = "pref_user_name";
    Context context;
    static Utils utils;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    public static final boolean SHOULD_PRINT_LOG = true;

    public static String TEMP_1= "temp_text_1";
    public static String TEMP_POS= "temp_pos";

    public static String PREF_IS_LOGGED_IN= "pref_is_logged_in";
    public static String EXTRA_IS_FROM_CHAT= "extra_is_from_chat";
    public static String EXTRA_DEAL_REQUEST_LIST= "extra_deal_request_list";
    public static String EXTRA_DEAL_DETAIL_FRIEND_ID= "extra_deal_detail_friend_id";
    public static String EXTRA_DEAL_DETAIL_FRIEND_NAME= "extra_deal_detail_friend_name";
    public static String EXTRA_DEAL_DETAIL_INFO_DEAL_ID= "extra_deal_detail_info_deal_id";
    public static String EXTRA_IS_FROM_NOTIFICATION= "extra_is_from_notification";

    public static String EXTRA_COLOR = "extra_color";
    public static String EXTRA_POS = "extra_pos";
    public static ApiInterface apiService = null;

    public static String BROADCAST_LEAD_COUNT = "broadcast_lead_count";
    public static String NOTIFICATION_LEAD_COUNT = "notification_lead_count";


    public static boolean checkData(String string) {
        return !string.isEmpty() && string.trim().length() != 0;
    }


    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }
    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public static String getStringFromBitmap(Bitmap bitmap){
        return Base64.encodeToString(getBytesFromBitmap(bitmap),
                Base64.NO_WRAP);
    }

    public static  Bitmap getBitmapFromBytes(byte[] bytes){
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public Utils(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        edit = pref.edit();
    }


    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) MainApplication.getGlobalContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }



    public synchronized static Utils getInstance(Context context) {
        if (utils == null) {
            utils = new Utils(context);

        }
        return utils;
    }
    public void setInt(String key, int value) {
        edit.putInt(key, value);
        edit.commit();
    }

    public int getInt(String key) {
        return pref.getInt(key, 0);
    }
    public void setString(String key, String value) {
        edit.putString(key, value);
        edit.commit();
    }

    public String getString(String key) {
        return pref.getString(key, "");
    }

    public boolean getBoolean(String key) {
        return pref.getBoolean(key, false);
    }


    public static void Toast(Activity act, String msg)
    {
        Toast.makeText(act,msg,Toast.LENGTH_SHORT).show();
    }

    public void setBoolean(String key, boolean value) {
        edit.putBoolean(key, value);
        edit.commit();
    }

    public void clearPref() {

        edit.clear();
        edit.commit();
    }

    public static String toHex(int argb) {
        StringBuilder sb = new StringBuilder();
        //sb.append(toHexString((byte) Color.alpha(argb)));
        sb.append(toHexString((byte) Color.red(argb)));
        sb.append(toHexString((byte) Color.green(argb)));
        sb.append(toHexString((byte) Color.blue(argb)));
        return sb.toString();
    }

    private static String toHexString(byte v) {
        String hex = Integer.toHexString(v & 0xff);
        if (hex.length() == 1) {
            hex = "0" + hex; //$NON-NLS-1$
        }
        return hex.toUpperCase();
    }


    public synchronized static ApiInterface getAPIInstance() {
        if (apiService == null) {
            apiService = ApiClient.getClient().create(ApiInterface.class);
        }
        return apiService;
    }


    public File create_Image_File(String ext)
    {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+"/"+ context.getString(R.string.app_name));
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        // String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault()).format(new Date());
        File mediaFile = new File(fmt.format(now) + ext);

        return mediaFile;

    }
    public File create_file(String ext)
    {
        // File folderPath = new File(Environment.getExternalStorageDirectory() +"/"+ getString(R.string.app_name));
        File shareQr = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!shareQr.exists()) {
            shareQr.mkdirs();
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        File newFile =null;//=  File.createTempFile( fmt.format(now) + ".png",shareQr);

        try {
            newFile= File.createTempFile(fmt.format(now).toString(),  /* prefix */ext,         /* suffix */shareQr      /* directory */);
            LogUtils.e(" newFile "+ newFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newFile;
    }

}

