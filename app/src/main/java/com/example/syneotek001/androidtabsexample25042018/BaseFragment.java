package com.example.syneotek001.androidtabsexample25042018;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;


public class BaseFragment extends Fragment {
    public Activity mActivity;
    public Context mContext;
    public TextView title;

    class C05651 implements OnClickListener {
        C05651() {
        }

        public void onClick(View view) {
            ((MainActivity) BaseFragment.this.mActivity).toggleDrawer();
        }
    }

    class C05662 implements OnClickListener {
        C05662() {
        }

        public void onClick(View view) {
            ((MainActivity) BaseFragment.this.mActivity).toggleDrawer();
        }
    }

    class C05673 implements OnClickListener {
        C05673() {
        }

        public void onClick(View view) {
            BaseFragment.this.mActivity.onBackPressed();
        }
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mActivity = getActivity();
    }

    public void setupToolBar(Toolbar toolbar, String Title) {
        this.title = (TextView) toolbar.findViewById(R.id.tvTitle);
        TextView textView = this.title;
        if (Title == null) {
            Title = "";
        }
        textView.setText(Title);
    }

    public void setupToolBarWithCustomMenu(Toolbar toolbar, String Title) {
        this.title = (TextView) toolbar.findViewById(R.id.tvTitle);
        TextView textView = this.title;
        if (Title == null) {
            Title = "";
        }
        textView.setText(Title);
        ImageView ivMenu = (ImageView) toolbar.findViewById(R.id.ivMenu);
        ivMenu.setVisibility(View.VISIBLE);
        ivMenu.setOnClickListener(new C05651());
    }

    public void setupToolBarWithMenu(Toolbar toolbar, @Nullable String Title) {
        ((BaseActivity) this.mActivity).setSupportActionBar(toolbar);
        ActionBar actionBar = ((BaseActivity) this.mActivity).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator((int) R.drawable.shape_bottom_left_rounded_rectangle_new);
        }
        toolbar.setNavigationOnClickListener(new C05662());
        this.title = (TextView) toolbar.findViewById(R.id.tvTitle);
        TextView textView = this.title;
        if (Title == null) {
            Title = "";
        }
        textView.setText(Title);
    }

    public void setupToolBarWithBackArrow(Toolbar toolbar) {
        setupToolBarWithBackArrow(toolbar, null);
    }

    public void setupToolBarWithBackArrow(Toolbar toolbar, @Nullable String Title) {
        ((BaseActivity) this.mActivity).setSupportActionBar(toolbar);
        ActionBar actionBar = ((BaseActivity) this.mActivity).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator((int) R.drawable.ic_vector_arrow_back);
        }
        toolbar.setNavigationOnClickListener(new C05673());
        this.title = (TextView) toolbar.findViewById(R.id.tvTitle);
        TextView textView = this.title;
        if (Title == null) {
            Title = "";
        }
        textView.setText(Title);
    }

    public void updateToolBarTitle(@Nullable String Title) {
        TextView textView = this.title;
        if (Title == null) {
            Title = this.mContext.getResources().getString(R.string.app_name);
        }
        textView.setText(Title);
    }
    public boolean onBackPressed() {
        return false;
    }
}
