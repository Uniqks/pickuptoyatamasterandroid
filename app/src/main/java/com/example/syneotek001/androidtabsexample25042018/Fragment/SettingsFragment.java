package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    LinearLayout lyProfile,lyHelpCenter,lyAnnouncement;
    ImageView ivProfile,ivHelpCenter,ivAnnouncement;
    TextView tvProfile,tvHelpCenter,tvAnnouncement;

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        lyProfile = view.findViewById(R.id.lyProfile);
        lyHelpCenter = view.findViewById(R.id.lyHelpCenter);
        lyAnnouncement = view.findViewById(R.id.lyAnnouncement);
        ivProfile = view.findViewById(R.id.ivProfile);
        ivHelpCenter = view.findViewById(R.id.ivHelpCenter);
        ivAnnouncement = view.findViewById(R.id.ivAnnouncement);
        tvProfile = view.findViewById(R.id.tvProfile);
        tvHelpCenter = view.findViewById(R.id.tvHelpCenter);
        tvAnnouncement = view.findViewById(R.id.tvAnnouncement);
        ImageView ivBack=view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        ImageView ivMenu;
        ivMenu= view.findViewById(R.id.ivMenu);
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).opendrawer();
            }
        });
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.action_settings));
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lyProfile.setOnClickListener(this);
        lyHelpCenter.setOnClickListener(this);
        lyAnnouncement.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.lyProfile:
                lyAnnouncement.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings));
                lyProfile.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_clicked));
                lyHelpCenter.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings));
                tvAnnouncement.setTextColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_txt));
                tvProfile.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                tvHelpCenter.setTextColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_txt));
                ivAnnouncement.setImageResource(R.drawable.ic_announcement);
                ivProfile.setImageResource(R.drawable.ic_profile_clicked);
                ivHelpCenter.setImageResource(R.drawable.ic_help_center);
                replaceFragment(new EditProfileFragment());
                break;
            case R.id.lyHelpCenter:
                lyAnnouncement.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings));
                lyProfile.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings));
                lyHelpCenter.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_clicked));
                tvAnnouncement.setTextColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_txt));
                tvProfile.setTextColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_txt));
                tvHelpCenter.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                ivAnnouncement.setImageResource(R.drawable.ic_announcement);
                ivProfile.setImageResource(R.drawable.ic_profile);
                ivHelpCenter.setImageResource(R.drawable.ic_help_center_clicked);
                replaceFragment(new HelpCenterFragment());
                break;
            case R.id.lyAnnouncement:
                Log.i("SettingsFragment","onClick lyAnnouncement");
                lyAnnouncement.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_clicked));
                lyProfile.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings));
                lyHelpCenter.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.bg_settings));
                tvAnnouncement.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                tvProfile.setTextColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_txt));
                tvHelpCenter.setTextColor(ContextCompat.getColor(getActivity(),R.color.bg_settings_item_txt));
                ivAnnouncement.setImageResource(R.drawable.ic_announcement_clicked);
                ivProfile.setImageResource(R.drawable.ic_profile);
                ivHelpCenter.setImageResource(R.drawable.ic_help_center);
                replaceFragment(new AnnouncementFragment());
                break;
        }
    }

    public void replaceFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

}
