package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.EnumsAlign;
import com.anychart.anychart.LegendLayout;
import com.anychart.anychart.Pyramid;
import com.anychart.anychart.UiLegend;
import com.anychart.anychart.ValueDataEntry;
import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterLegend;
import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterLinechartLegend;
import com.example.syneotek001.androidtabsexample25042018.Adapter.AdapterPyramidchartLegend;
import com.example.syneotek001.androidtabsexample25042018.Adapter.DashboardItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Support.NotificationIntentService;
import com.example.syneotek001.androidtabsexample25042018.Ui.pyramidchart.ChartData;
import com.example.syneotek001.androidtabsexample25042018.Ui.pyramidchart.PyramidChart;
import com.example.syneotek001.androidtabsexample25042018.Utils.CustomTypefaceSpan;
import com.example.syneotek001.androidtabsexample25042018.Utils.ExpandableHeightGridView;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnRecyclerViewItemClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class DashboardFragment extends DashboardLineChartBaseFragment implements OnClickListener {
    ArrayList<LineGraphDataModel> leadsEntry;
    RecyclerView recyclerView;
//    private SlidingUpPanelLayout mLayout;

    public LinearLayout llGraph;
    public LineChart mChart;
    private long mDirtyFlags = -1;
    private LinearLayout mboundView0;
    public TextView tvMoreGraphs;
    public TextView xAxisTitle, pieChartSelectedValue, tvPrint;
    RelativeLayout rlLeadManager;
    //    public VerticalLabelView yAxisTitle;
    PyramidChart pyramidChart;

    private PieChart mChart1;
    private int STORAGE_PERMISSION_CODE = 11;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    ExpandableHeightGridView gv_legend, gv_linechart_legend, gv_pyramidchart_legend;
    String[] arr_labels = new String[11];
    int[] arr_colors = new int[11];


    String[] arr_linechart_labels = new String[11];
    int[] arr_linechart_colors = new int[11];

    String[] arr_pyramidchart_labels = new String[11];
    int[] arr_pyramidchart_colors = new int[11];

    RelativeLayout relativeLayout;

    ImageView ivPieChartOptions, ivLineChartOptions;

    protected String[] mParties = new String[]{
            "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z"
    };

    Typeface typeface;


    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_analytics, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
//        mLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);

        relativeLayout = view.findViewById(R.id.rlPieChartView);
        ivPieChartOptions = view.findViewById(R.id.ivPieChartOptions);
        ivLineChartOptions = view.findViewById(R.id.ivLineChartOptions);

        tvMoreGraphs = view.findViewById(R.id.tvMoreGraphs);
        xAxisTitle = view.findViewById(R.id.xAxisTitle);
        pieChartSelectedValue = view.findViewById(R.id.pieChartSelectedValue);
        tvPrint = view.findViewById(R.id.tvPrint);
//        yAxisTitle=view.findViewById(R.id.yAxisTitle);
        llGraph = view.findViewById(R.id.llGraph);
        mChart = view.findViewById(R.id.mChart);
        rlLeadManager = view.findViewById(R.id.rlLeadManager);
        mChart1 = view.findViewById(R.id.chart1);
        gv_legend = view.findViewById(R.id.gv_legend);
        gv_linechart_legend = view.findViewById(R.id.gv_linechart_legend);
        gv_pyramidchart_legend = view.findViewById(R.id.gv_pyramidchart_legend);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/montserrat_regular.otf");
        initPieChart();

        /*mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.d("onPanelSlide", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.d("onPanelStateChanged", "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });*/

        /*DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;
        mLayout.setPanelHeight(height/2);
*/
        setUpNavigationDrawer();
        setBinding(mChart);
        prepareGraphLayout(8, 800.0f);
        setClickEvents();
        setAnimation(view);


        AnyChartView anyChartView = view.findViewById(R.id.any_chart_view);
//        anyChartView.setProgressBar(view.findViewById(R.id.progress_bar));

        Pyramid pyramid = AnyChart.pyramid();

        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry("TV promotion", 6371664));
        data.add(new ValueDataEntry("Radio promotion", 7216301));
        data.add(new ValueDataEntry("Advertising leaflets", 1486621));
        data.add(new ValueDataEntry("Before advertising started", 1386622));

        pyramid.setData(data);

        UiLegend legend = pyramid.getLegend();
        legend.setEnabled(true);
        legend.setPosition("outside-right");
        legend.setItemsLayout(LegendLayout.VERTICAL);
        legend.setAlign(EnumsAlign.TOP);

        pyramid.setLabels(false);

        anyChartView.setChart(pyramid);

        pyramidChart = view.findViewById(R.id.pyramid);

        List<ChartData> values = new ArrayList<>();

        values.add(new ChartData("log out", 4000, R.color.yellow_opacity_80));
        values.add(new ChartData("unique session", 2000, R.color.orange_opacity_80));
        values.add(new ChartData("unique visitors", 2000, R.color.sky_opacity_80));
        values.add(new ChartData("unique visits", 2000, R.color.green_opacity_80));
        values.add(new ChartData("visits", 2000, R.color.pink_opacity_80));

        pyramidChart.setData(values);

        for (int i = 0; i < values.size(); i++) {
            arr_pyramidchart_colors[i] = values.get(i).getPyramid_color();
            arr_pyramidchart_labels[i] = values.get(i).getLabels();
        }


        AdapterPyramidchartLegend adapterLegend = new AdapterPyramidchartLegend(getActivity(), arr_pyramidchart_labels, arr_pyramidchart_colors);
        gv_pyramidchart_legend.setAdapter(adapterLegend);

        Log.i("DashboardFragment", " onCreateView");

        String FILE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/invoice.pdf"; // add permission in your manifest...

        LogUtils.i("DashboardFragment" + " printPieChart FILE " + FILE);
        Log.e("DashboardFragment", " printPieChart FILE " + FILE);


        return view;
    }


    public void initPieChart() {

        mChart1.setUsePercentValues(true);
        mChart1.getDescription().setEnabled(false);
        mChart1.setExtraOffsets(5, 10, 5, 5);
        mChart1.setDrawSliceText(false);

        mChart1.setDragDecelerationFrictionCoef(0.95f);

        mChart1.setCenterTextTypeface(typeface);
//        mChart1.setCenterText(generateCenterSpannableText());

        mChart1.setDrawHoleEnabled(true);
        mChart1.setHoleColor(Color.WHITE);

        mChart1.setTransparentCircleColor(Color.WHITE);
        mChart1.setTransparentCircleAlpha(110);

        mChart1.setHoleRadius(0.0f);
        mChart1.setTransparentCircleRadius(0.0f);

        mChart1.setDrawCenterText(false);

        mChart1.setRotationAngle(335);
        // enable rotation of the chart by touch
        mChart1.setRotationEnabled(true);
        mChart1.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart1.setOnChartValueSelectedListener(this);

        setPieChartData(11, 100);

        mChart1.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);


        Legend l = mChart1.getLegend();
        mChart1.getLegend().setEnabled(false);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(7f);
        l.setTextColor(Color.WHITE);
        l.setTypeface(typeface);


        // entry label styling
        mChart1.getDescription().setEnabled(false);
        mChart1.setEntryLabelColor(Color.WHITE);
        mChart1.setEntryLabelTypeface(typeface);
        mChart1.setEntryLabelTextSize(12f);


    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e != null) {

            int pos = Integer.parseInt(String.valueOf(h.getX()).replace(".0", ""));
            pieChartSelectedValue.setText(mParties[pos] + " " + e.getY());
            pieChartSelectedValue.setTextColor(arr_colors[pos]);
//			binding.tvSelectedLegend.setTextColor(h.getC);
            Log.i("VAL SELECTED",
                    "Value: " + e.getY() + ", index: " + h.getX()
                            + ", DataSet index: " + h.getDataSetIndex());
            /*printPieChart();*/


        }
    }

    private void setPieChartData(int count, float range) {

        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count; i++) {
            entries.add(new PieEntry((float) ((Math.random() * mult) + mult / 5),
                    mParties[i % mParties.length],
                    getResources().getDrawable(R.drawable.star)));
        }

        PieDataSet dataSet = new PieDataSet(entries, "Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(1.5f);
        dataSet.setDrawValues(false);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);


        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(typeface);
        mChart1.setData(data);


        Legend l = mChart1.getLegend();
        arr_labels = l.getLabels();
        arr_colors = l.getColors();

        AdapterLegend adapterLegend = new AdapterLegend(getActivity(), arr_labels, arr_colors);
        gv_legend.setAdapter(adapterLegend);
        mChart1.highlightValue(0, 0);

        // undo all highlights
//        mChart1.highlightValues(null);

        mChart1.invalidate();


    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("DashboardFragment",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        Log.d("DashboardFragment",
                "pictureFile path: " + pictureFile.getAbsolutePath());
        Log.d("DashboardFragment",
                "image: " + image.toString());
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("DashboardFragment", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("DashboardFragment", "Error accessing file: " + e.getMessage());
        }

        String FILE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/invoice.pdf"; // add permission in your manifest...

        LogUtils.i("DashboardFragment" + " printPieChart FILE " + FILE);

        try {
//            Document document = new Document();

//            Document document = new Document(new RectangleReadOnly(1000, 1000), 20.0f, 20.0f,20.0f, 20.0f);

            Document document = new Document(PageSize.A3, 20.0f, 20.0f, 20.0f, 20.0f);

            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            addImage(document, byteArray);
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveAsImage(Bitmap image, String file_type) {
        File pictureFile = getOutputImageFile(file_type);
        if (pictureFile == null) {
            Log.d("DashboardFragment",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        Log.d("DashboardFragment",
                "pictureFile path: " + pictureFile.getAbsolutePath());
        Log.d("DashboardFragment",
                "image: " + image.toString());
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("DashboardFragment", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("DashboardFragment", "Error accessing file: " + e.getMessage());
        }

    }

    private File getOutputImageFile(String file_type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        /*File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getActivity().getPackageName()
                + "/files");*/

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Toyota");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            /*if (!mediaStorageDir.mkdirs()) {
                return null;
            }*/
            mediaStorageDir.mkdir();
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + "." + file_type;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getActivity().getPackageName()
                + "/files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }


    private void setAnimation(View mview) {
        TextView tvLeadManager = mview.findViewById(R.id.tvLeadManager);
        TextView tvLeadManagerCount = mview.findViewById(R.id.tvLeadManagerCount);
        TextView tvTaskManager = mview.findViewById(R.id.tvTaskManager);
        TextView tvTaskManagerCount = mview.findViewById(R.id.tvTaskManagerCount);
        TextView tvOfflineLeads = mview.findViewById(R.id.tvOfflineLeads);
        TextView tvOfflineLeadsCount = mview.findViewById(R.id.tvOfflineLeadsCount);
        TextView tvLoremIpsum = mview.findViewById(R.id.tvLoremIpsum);
        TextView tvLoremIpsumCount = mview.findViewById(R.id.tvLoremIpsumCount);

        Animation a = AnimationUtils.loadAnimation(this.mContext, R.anim.slide_in_bottom_with_fade);
        a.reset();
        a.setStartOffset(200);
        tvLeadManager.clearAnimation();
        tvLeadManager.startAnimation(a);
        tvLeadManagerCount.clearAnimation();
        tvLeadManagerCount.startAnimation(a);
        tvTaskManager.clearAnimation();
        tvTaskManager.startAnimation(a);
        tvTaskManagerCount.clearAnimation();
        tvTaskManagerCount.startAnimation(a);
        tvOfflineLeads.clearAnimation();
        tvOfflineLeads.startAnimation(a);
        tvOfflineLeadsCount.clearAnimation();
        tvOfflineLeadsCount.startAnimation(a);
        tvLoremIpsum.clearAnimation();
        tvLoremIpsum.startAnimation(a);
        tvLoremIpsumCount.clearAnimation();
        tvLoremIpsumCount.startAnimation(a);
    }

    /*private void setClickEvents() {
        this.mBinding.data.rlLeadManager.setOnClickListener(this);
        this.mBinding.data.rlLoremIpsum.setOnClickListener(this);
        this.mBinding.data.rlOfflineLeads.setOnClickListener(this);
        this.mBinding.data.rlTaskManager.setOnClickListener(this);
        this.mBinding.chart.tvMoreGraphs.setOnClickListener(this);
    }*/

    private void setClickEvents() {
        rlLeadManager.setOnClickListener(this);
        tvMoreGraphs.setOnClickListener(this);
        ivPieChartOptions.setOnClickListener(this);
        ivLineChartOptions.setOnClickListener(this);
    }

    private void setUpNavigationDrawer() {
        final ArrayList<String> dashboardItemArray = new ArrayList();
        dashboardItemArray.add(getResources().getString(R.string.account));
        dashboardItemArray.add(getResources().getString(R.string.dashboard));
        dashboardItemArray.add(getResources().getString(R.string.message));
        dashboardItemArray.add(getResources().getString(R.string.email));
        dashboardItemArray.add(getResources().getString(R.string.help_center));
        dashboardItemArray.add(getResources().getString(R.string.announcement));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        DashboardItemAdapter ndAdapter = new DashboardItemAdapter(getActivity(), dashboardItemArray, new OnRecyclerViewItemClickListener() {
            public void onItemClicked(int position) {
                Logger.m6e("Selected Item", (String) dashboardItemArray.get(position));
                if (position == 5) {
                    ((HomeActivity) getActivity()).replaceFragment(new AnnouncementFragment());
                } else if (position == 4) {
                    ((HomeActivity) getActivity()).replaceFragment(new HelpCenterFragment());
                } else if (position == 3) {
                    ((HomeActivity) getActivity()).replaceFragment(new EmailInboxListFragment());
                } else if (position == 2) {
                    ((HomeActivity) getActivity()).replaceFragment(new ChatListHolderFragment());
                }
            }
        });
        recyclerView.setAdapter(ndAdapter);
        ndAdapter.setLastSelectedPosition(1);
    }

    private void prepareGraphLayout(int count, float range) {
        ArrayList<Entry> newLeads = new ArrayList<>();
        ArrayList<Entry> followUpLeads = new ArrayList<>();
        ArrayList<Entry> underContractLeads = new ArrayList<>();
        ArrayList<Entry> deliveryLeads = new ArrayList<>();
        ArrayList<Entry> soldLeads = new ArrayList<>();
        ArrayList<Entry> inactiveLeads = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            newLeads.add(new Entry((float) i, ((float) (Math.random() * ((double) range))) + 3.0f, getResources().getDrawable(R.drawable.ic_chart_marker_yellow)));
            followUpLeads.add(new Entry((float) i, 20.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_orange)));
            underContractLeads.add(new Entry((float) i, 50.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_sky)));
            deliveryLeads.add(new Entry((float) i, 30.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_green)));
            soldLeads.add(new Entry((float) i, 10.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_pink)));
            inactiveLeads.add(new Entry((float) i, 25.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_red)));
        }
        this.leadsEntry = new ArrayList<>();
        this.leadsEntry.add(new LineGraphDataModel("New", ContextCompat.getColor(this.mContext, R.color.yellow_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_yellow), ContextCompat.getColor(this.mContext, R.color.yellow_opacity_50), newLeads));
        this.leadsEntry.add(new LineGraphDataModel("FollowUp", ContextCompat.getColor(this.mContext, R.color.orange_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_orange), ContextCompat.getColor(this.mContext, R.color.orange_opacity_50), followUpLeads));
        this.leadsEntry.add(new LineGraphDataModel("UnderContract", ContextCompat.getColor(this.mContext, R.color.sky_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_sky), ContextCompat.getColor(this.mContext, R.color.sky_opacity_50), underContractLeads));
        this.leadsEntry.add(new LineGraphDataModel("Delivery", ContextCompat.getColor(this.mContext, R.color.green_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_green), ContextCompat.getColor(this.mContext, R.color.green_opacity_50), deliveryLeads));
        this.leadsEntry.add(new LineGraphDataModel("Sold", ContextCompat.getColor(this.mContext, R.color.pink_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_pink), ContextCompat.getColor(this.mContext, R.color.pink_opacity_50), soldLeads));
        this.leadsEntry.add(new LineGraphDataModel("Inactive", ContextCompat.getColor(this.mContext, R.color.red_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_red), ContextCompat.getColor(this.mContext, R.color.red_opacity_50), inactiveLeads));
        setUpChart(2010, count + 2010, this.leadsEntry);

        for (int i = 0; i < leadsEntry.size(); i++) {
            arr_linechart_labels[i] = leadsEntry.get(i).getTitle();
            arr_linechart_colors[i] = leadsEntry.get(i).getColorOpacity80();
        }

        AdapterLinechartLegend adapterLegend = new AdapterLinechartLegend(getActivity(), arr_linechart_labels, arr_linechart_colors);
        gv_linechart_legend.setAdapter(adapterLegend);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlLeadManager:

                ((HomeActivity) getActivity()).replaceFragment(new LeadListHolderFragment());
                return;
            case R.id.tvMoreGraphs:
                ((HomeActivity) getActivity()).replaceFragment(new AnalyticsHolderFragment());
                return;
            case R.id.ivPieChartOptions:
//                printPieChart();
                show_popup("piechart");
                return;
            case R.id.ivLineChartOptions:
                show_popup("linechart");
                break;
            default:
                return;
        }
    }
    Bitmap bm = null;
    public void show_popup(String chart_type) {

        LogUtils.i("SPRegisterInfo" + " show_popup ");

        PopupMenu popup = null;

//        Bitmap bm = null;

        if (chart_type.equals("piechart")) {
            popup = new PopupMenu(getActivity(), ivPieChartOptions);
            bm = getBitmapFromView(relativeLayout);
        } else if (chart_type.equals("linechart")) {
            popup = new PopupMenu(getActivity(), ivLineChartOptions);
            bm = getBitmapFromView(llGraph);
        }

//        PopupMenu popup = new PopupMenu(getActivity(), ivPieChartOptions);
        popup.getMenuInflater().inflate(R.menu.menu_piechart_print, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.sendMessage:
                        createNotification("New Lead - Pickering Toyota");
                        break;
                    case R.id.print:
                        LogUtils.i("show_popup" + " print ");
                        if (!hasPermissions(getActivity(), PERMISSIONS)) {
                            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, STORAGE_PERMISSION_CODE);
                        } else {
//                            Bitmap bm = getBitmapFromView(relativeLayout);
                            if (bm!=null)
                            storeImage(bm);
                        }
                        break;
                    case R.id.save_as_jpeg:
                        LogUtils.i("show_popup" + " save_as_jpeg ");
                        if (!hasPermissions(getActivity(), PERMISSIONS)) {
                            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, STORAGE_PERMISSION_CODE);
                        } else {
//                            Bitmap bm = getBitmapFromView(relativeLayout);
                            if (bm!=null)
                            saveAsImage(bm, "jpg");
                        }
                        break;
                    case R.id.save_as_png:
                        LogUtils.i("show_popup" + " save_as_jpeg ");
                        if (!hasPermissions(getActivity(), PERMISSIONS)) {
                            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, STORAGE_PERMISSION_CODE);
                        } else {
//                            Bitmap bm = getBitmapFromView(relativeLayout);
                            if (bm!=null)
                            saveAsImage(bm, "png");
                        }
                        break;
                }

                return true;
            }
        });

        popup.show();

    }

    private NotificationManager notifManager;
    public void createNotification(String aMessage) {
        final int NOTIFY_ID = 0; // ID of notification
        String id = getString(R.string.default_notification_channel_id); // default_channel_id
        String title = getString(R.string.default_notification_channel_title); // Default Channel
        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;
        intent = new Intent(getActivity(), HomeActivity.class);
        intent.putExtra(Utils.EXTRA_IS_FROM_NOTIFICATION,true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);
        RemoteViews notificationView = new RemoteViews(getActivity().getPackageName(), R.layout.notification_leads_layout);

        Bitmap bitmap = textAsBitmap(getActivity(), "152244891563", 30, ContextCompat.getColor(getActivity(),R.color.black));
        Bitmap bitmap1 = textAsBitmap(getActivity(), "qwerty Panchal", 30, ContextCompat.getColor(getActivity(),R.color.black));
        Bitmap bitmap2 = textAsBitmap(getActivity(), "18-01-2018 06:24 PM", 30, ContextCompat.getColor(getActivity(),R.color.black));
        Bitmap bitmap3 = textAsBitmap(getActivity(), "New Lead", 30, ContextCompat.getColor(getActivity(),R.color.black));

//Show generated bitmap in Imageview
        notificationView.setImageViewBitmap(R.id.iv_about_value, bitmap);
        notificationView.setImageViewBitmap(R.id.ivUserValue, bitmap1);
        notificationView.setImageViewBitmap(R.id.ivAssignDateValue, bitmap2);
        notificationView.setImageViewBitmap(R.id.ivTitle, bitmap3);

        notificationView.setTextViewText(R.id.tvSource,"152244891563");
        notificationView.setTextViewText(R.id.tvCity, "qwerty Panchal");
        notificationView.setTextViewText(R.id.tvAssignDate, "18-01-2018 06:24 PM");
        notificationView.setOnClickPendingIntent(R.id.btnAccept, pendingIntent);
        notificationView.setOnClickPendingIntent(R.id.btnReject, pendingIntent);
        int m = new Random().nextInt(8999) + 1000;
        Intent leftIntent = new Intent(getActivity(), NotificationIntentService.class);
        leftIntent.setAction("left");
        leftIntent.putExtra("leadId", "12");
        leftIntent.putExtra("notificationId", m);
        notificationView.setOnClickPendingIntent(R.id.btnAccept, PendingIntent.getService(getActivity(), m, leftIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        Intent rightIntent = new Intent(getActivity(), NotificationIntentService.class);
        rightIntent.setAction("right");
        leftIntent.putExtra("leadId", "12");
        rightIntent.putExtra("notificationId", m);
        notificationView.setOnClickPendingIntent(R.id.btnReject, PendingIntent.getService(getActivity(), m, rightIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        Intent linkIntent = new Intent(getActivity(), NotificationIntentService.class);
        linkIntent.setAction("link");
        linkIntent.putExtra("leadId", "12");
        linkIntent.putExtra("notificationId", m);
        linkIntent.putExtra("Link", "https://www.google.com/");
        notificationView.setOnClickPendingIntent(R.id.tvSource, PendingIntent.getService(getActivity(), m, linkIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        if (notifManager == null) {
            notifManager = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, title, importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(getActivity(), id);

            builder.setContentTitle(aMessage)  // required
                    .setSmallIcon(R.drawable.ic_notify_toyota) // required
//                    .setContentText(this.getString(R.string.app_name))  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(aMessage)).setCustomBigContentView(notificationView);
        } else {
            builder = new NotificationCompat.Builder(getActivity());
            builder.setContentTitle(aMessage)                           // required
                    .setSmallIcon(R.drawable.ic_notify_toyota) // required
//                    .setContentText(this.getString(R.string.app_name))  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(aMessage)).setCustomBigContentView(notificationView);
        }
        Notification notification = builder.build();
        notifManager.notify(NOTIFY_ID, notification);

        Intent intent1 = new Intent(Utils.BROADCAST_LEAD_COUNT);
        Utils.getInstance(getActivity()).setInt(Utils.NOTIFICATION_LEAD_COUNT,Utils.getInstance(getActivity()).getInt(Utils.NOTIFICATION_LEAD_COUNT)+1);
//        intent1.putExtra(Utils.NOTIFICATION_LEAD_COUNT,Utils.getInstance(getActivity()).getInt(Utils.NOTIFICATION_LEAD_COUNT)+1);
        getActivity().sendBroadcast(intent1);

    }

    public static Bitmap textAsBitmap(Context context,String messageText,float textSize,int textColor){
        String fontName="montserrat_regular.otf";
        Typeface font=Typeface.createFromAsset(context.getAssets(),fontName);
        Paint paint=new Paint();
        paint.setTextSize(textSize);
        paint.setTypeface(font);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline=-paint.ascent(); // ascent() is negative
        int width=(int)(paint.measureText(messageText)+0.5f); // round
        int height=(int)(baseline+paint.descent()+0.5f);
        Bitmap image=Bitmap.createBitmap(width,height,Bitmap.Config.ARGB_8888);
        Canvas canvas=new Canvas(image);
        canvas.drawText(messageText,0,baseline,paint);
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == STORAGE_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            show_popup();
            //takePhotoFromCamera() ;
        } else {

            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.enable_perm));
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static Bitmap loadBitmapFromView(View v) {
        if (v.getMeasuredHeight() <= 0) {
            v.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
            v.draw(c);
            return b;
        }
        return null;
    }

    public void printPieChart() {

        LogUtils.i("DashboardFragment" + " printPieChart ");

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        RelativeLayout root = (RelativeLayout) inflater.inflate(R.layout.fragment_analytics, null); //RelativeLayout is root view of my UI(xml) file.
        root.setDrawingCacheEnabled(true);
        Bitmap screen = getBitmapFromView(getActivity().getWindow().findViewById(R.id.rlPieChartView));


    }

    private static void addImage(Document document, byte[] byteArray) {
        Image image = null;
        try {
            image = Image.getInstance(byteArray);
        } catch (BadElementException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // image.scaleAbsolute(150f, 150f);
        try {
            document.add(image);
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.BLACK);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    String dirpath;

    public void layoutToImage() {
        Log.e("DashboardFragment", " layoutToImage ");
        // get view group using reference

        // convert view group to bitmap
        relativeLayout.setDrawingCacheEnabled(true);
        relativeLayout.buildDrawingCache();
        Bitmap bm = relativeLayout.getDrawingCache();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void imageToPDF() throws FileNotFoundException {
        try {
            Document document = new Document();
            dirpath = Environment.getExternalStorageDirectory().toString();
            PdfWriter.getInstance(document, new FileOutputStream(dirpath + "/NewPDF.pdf")); //  Change pdf's name.
            document.open();
            Image img = Image.getInstance(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / img.getWidth()) * 100;
            img.scalePercent(scaler);
            img.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
            document.add(img);
            document.close();
            Toast.makeText(getActivity(), "PDF Generated successfully!..", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

}
