package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.SelectLeadCategoryItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.LeadNoteModel;

import java.util.ArrayList;

public class SelectLeadCategoryFragment extends Fragment implements OnClickListener {
    SelectLeadCategoryItemAdapter mAdapter;
    ArrayList<LeadNoteModel> mNoteList = new ArrayList<>();
    public RecyclerView recyclerView;
    public TextView tvNoLeadNoteFound,tvTitle;
    ImageView ivBack;
    String type;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_select_salesman, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoLeadNoteFound = view.findViewById(R.id.tvNoLeadNoteFound);
        ivBack = view.findViewById(R.id.ivBack);
        tvTitle = view.findViewById(R.id.tvTitle);
        FloatingActionButton fab = view.findViewById(R.id.fab);

        tvTitle.setText(getString(R.string.str_select_lead_category));

        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        setUpRecyclerView();
        return view;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llNote:
                ((HomeActivity) getActivity()).replaceFragment(new NoteDetailFragment());
                return;
            default:
                return;
        }
    }

    private void setUpRecyclerView() {
        mNoteList.clear();
        if (getArguments() != null) {
            if (getArguments().getString("lead_type") != null &&
                    !getArguments().getString("lead_type").equals("")) {
                if (getArguments().getString("lead_type").equals("online")) {
                    mNoteList.add(new LeadNoteModel("1", "Build", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("2", "Overview", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("3", "Sales Department", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("4", "Career Opportunities", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("5", "Report Safety", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("6", "Warranty", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("7", "Test Drive", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("8", "New Vehicles", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("9", "Current Offers", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("10", "Contact Us", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("11", "Showroom Quote", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("12", "Appraise Vehicle", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                    mNoteList.add(new LeadNoteModel("13", "Financing", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                } else if (getArguments().getString("lead_type").equals("offline")) {
                    mNoteList.add(new LeadNoteModel("1", "Reception", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
                mAdapter = new SelectLeadCategoryItemAdapter(getActivity(), mNoteList);
                recyclerView.setAdapter(this.mAdapter);
                refreshData();
            }
        }

    }

    private void refreshData() {
        if (mNoteList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoLeadNoteFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoLeadNoteFound.setVisibility(View.VISIBLE);
    }
}
