package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.model.CarModel;

import java.util.ArrayList;
import java.util.List;

public class CarItemAdapter extends RecyclerView.Adapter<CarItemAdapter.MyViewHolder> {
    private final Animation anim;
    private boolean isItemSelectable;
    private int lastSelectedPosition = -1;
    private Context mContext;
    private List<CarModel> mData = new ArrayList();

    class MyViewHolder extends ViewHolder {
        public ImageView ivImage, ivSelectIndicator;
        public TextView tvName, tvPrice;

        MyViewHolder(View view) {
            super(view);
            ivImage = view.findViewById(R.id.ivImage);
            ivSelectIndicator = view.findViewById(R.id.ivSelectIndicator);
            tvName = view.findViewById(R.id.tvName);
            tvPrice = view.findViewById(R.id.tvPrice);
        }
    }

    public CarItemAdapter(Context context, ArrayList<CarModel> list /*boolean isSelectable*/) {
        this.mContext = context;
        this.mData = list;
//        this.isItemSelectable = isSelectable;
        this.anim = AnimationUtils.loadAnimation(this.mContext, R.anim.anim_scale_in_out);
    }

    public int getItemViewType(int position) {
        return 1;
    }

   /* public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder((ItemCarBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_car, parent, false));
    }*/

    @NonNull
    public CarItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car, parent, false);

        return new CarItemAdapter.MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        CarModel mCarModel = (CarModel) this.mData.get(position);
        holder.tvName.setText(mCarModel.getName());
        holder.tvPrice.setText(this.mContext.getResources().getString(R.string.dollar_sign_, new Object[]{mCarModel.getPrice()}));
        holder.ivImage.setImageResource(mContext.getResources().getIdentifier(mCarModel.getImage(), "drawable", mContext.getPackageName()));
        holder.ivSelectIndicator.setImageDrawable(mCarModel.isSelected() ? mContext.getResources().getDrawable(R.drawable.ic_right_with_green_bg) : this.mContext.getResources().getDrawable(R.drawable.ic_right_with_gray_bg));

        /*if (CarItemAdapter.this.isItemSelectable) */{
            holder.ivSelectIndicator.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Logger.m5e("Car Clicked");
                    CarModel mCarModel = mData.get(position);
                    mCarModel.setSelected(!mCarModel.isSelected());
                    CarItemAdapter.this.notifyItemChanged(position, mCarModel);
                    if (position != CarItemAdapter.this.lastSelectedPosition && CarItemAdapter.this.lastSelectedPosition != -1) {
                        mCarModel = (CarModel) CarItemAdapter.this.mData.get(CarItemAdapter.this.lastSelectedPosition);
                        mCarModel.setSelected(false);
                        CarItemAdapter.this.notifyItemChanged(CarItemAdapter.this.lastSelectedPosition, mCarModel);
                        CarItemAdapter.this.lastSelectedPosition = position;
                    } else if (position == CarItemAdapter.this.lastSelectedPosition) {
                        CarItemAdapter.this.lastSelectedPosition = -1;
                    } else {
                        CarItemAdapter.this.lastSelectedPosition = position;
                    }
                }
            });
        }
    }

    public int getItemCount() {
        return mData.size();
    }
}
