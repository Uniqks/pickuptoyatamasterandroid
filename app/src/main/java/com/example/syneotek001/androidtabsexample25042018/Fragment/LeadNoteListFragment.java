package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.LeadNoteItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.LeadNoteModel;

import java.util.ArrayList;

public class LeadNoteListFragment extends Fragment implements OnClickListener {
    LeadNoteItemAdapter mAdapter;
    ArrayList<LeadNoteModel> mNoteList = new ArrayList<>();
    public RecyclerView recyclerView;
    public TextView tvNoLeadNoteFound;
    ImageView ivBack;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lead_note_list, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoLeadNoteFound = view.findViewById(R.id.tvNoLeadNoteFound);
        ivBack = view.findViewById(R.id.ivBack);
        FloatingActionButton fab = view.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).replaceFragment(new AddNotesFragment());
            }
        });
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        setUpRecyclerView();
        return view;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llNote:
                ((HomeActivity)getActivity()).replaceFragment(new NoteDetailFragment());
                return;
            default:
                return;
        }
    }

    private void setUpRecyclerView() {
        mNoteList.clear();
        mNoteList.add(new LeadNoteModel("1", "Note Title", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        mNoteList.add(new LeadNoteModel("2", "My Note", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        mNoteList.add(new LeadNoteModel("3", "Test Note", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s", "", 1486961877, 1503061200, 1490101200));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down));
        mAdapter = new LeadNoteItemAdapter(getActivity(), mNoteList);
        recyclerView.setAdapter(this.mAdapter);
        refreshData();
    }

    private void refreshData() {
        if (mNoteList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoLeadNoteFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoLeadNoteFound.setVisibility(View.VISIBLE);
    }
}
