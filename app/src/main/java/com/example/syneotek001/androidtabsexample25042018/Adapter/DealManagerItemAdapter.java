package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.model.DealManagerModel;

import java.util.ArrayList;

public class DealManagerItemAdapter extends RecyclerView.Adapter<DealManagerItemAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<DealManagerModel> mData = new ArrayList();

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAllocate, ivDescription;
        TextView tvCustomerId, tvDateTime, tvRequestCount, tvSalesman;

        MyViewHolder(View view) {
            super(view);
            ivAllocate = view.findViewById(R.id.ivAllocate);
            ivDescription = view.findViewById(R.id.ivDescription);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvRequestCount = view.findViewById(R.id.tvRequestCount);
            tvSalesman = view.findViewById(R.id.tvSalesman);
        }
    }

    public DealManagerItemAdapter(Context context, ArrayList<DealManagerModel> list) {
        this.mContext = context;
        this.mData = list;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_deal_manager, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DealManagerModel mDealManagerItem = mData.get(position);
        holder.tvCustomerId.setText(mDealManagerItem.getCustomerId());
        holder.tvSalesman.setText(mDealManagerItem.getSalesmanName());
        holder.tvDateTime.setText(TimeStamp.millisToTimeFormat(mDealManagerItem.getDateTime() * 1000));
        holder.tvRequestCount.setText(String.valueOf(mDealManagerItem.getDealRequestCount()));
      /* itemView.setOnClickListener(new OnClickListener(DealManagerItemAdapter.this) {
            public void onClick(View view) {
            }
        });*/
    }

   /* public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder((ItemDealManagerBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_deal_manager, parent, false));
    }
*/

    public int getItemCount() {
        return mData.size();
    }
}
