package com.example.syneotek001.androidtabsexample25042018.model;

public class DealLeadsModel {
    private String customerId = "";
    private String customerName = "";
    private String dateTime = "";

    public DealLeadsModel(String customerName, String customerId, String dateTime) {
        this.customerName = customerName;
        this.customerId = customerId;
        this.dateTime = dateTime;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getDateTime() {
        return this.dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String toString() {
        return "DealLeadsModel{customerName='" + this.customerName + '\'' + ", customerId='" + this.customerId + '\'' + ", dateTime='" + this.dateTime + '\'' + '}';
    }
}
