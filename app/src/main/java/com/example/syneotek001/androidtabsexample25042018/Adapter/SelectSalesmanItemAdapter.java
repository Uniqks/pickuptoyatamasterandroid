package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.DealAsignmentHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.Fragment.SelectSalesmanFragment.OnLoadMoreListener;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanListResponse.SalesmanListData;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.util.ArrayList;
import java.util.List;

public class SelectSalesmanItemAdapter extends Adapter<ViewHolder> {
    private final Animation anim;
    private Context mContext;
    private List<SalesmanListData> mData = new ArrayList();
    OnLoadMoreListener onLoadMoreListener;
    private boolean loading;
    private int visibleThreshold = AppConstant.LIST_API_VISIBLE_THRESHHOLD;
    private int lastVisibleItem, totalItemCount;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public static class ProgressViewHolder extends ViewHolder {

        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    class MyViewHolder extends ViewHolder {
        LinearLayout llNote;
        TextView tvTitle;

        MyViewHolder(View view) {
            super(view);
            llNote = view.findViewById(R.id.llNote);
            tvTitle = view.findViewById(R.id.tvTitle);
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public SelectSalesmanItemAdapter(Context context, ArrayList<SalesmanListData> list , RecyclerView recyclerView) {
        this.mContext = context;
        this.mData = list;
        this.anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_in_out);

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    Log.e("onLoadMoreListener"," "+onLoadMoreListener+" "+totalItemCount+" "+lastVisibleItem+" "+visibleThreshold+" "+loading);

                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        Log.e("onLoadMoreListener","if");
                        if (onLoadMoreListener != null) {
                            Log.e("onLoadMoreListener","if2");
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });

        }

    }

    public void setLoaded() {
        loading = false;
    }

    public int getItemViewType(int position) {

        return mData.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_email_folder, parent, false);

        return new MyViewHolder(v);*/

        ViewHolder vh;

        if (viewType == VIEW_ITEM) {

            View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_email_folder, parent, false);

            vh = new MyViewHolder(v);


        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }


        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder)
        {
            final SalesmanListData mSalesmanListData = (SalesmanListData) this.mData.get(position);
            if (mSalesmanListData==null)
                return;
            ((MyViewHolder)holder).tvTitle.setText(mSalesmanListData.getFirstname()+" "+mSalesmanListData.getLastname());

            holder.itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    DealAsignmentHolderFragment dealAsignmentHolderFragment = new DealAsignmentHolderFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("sales_man_name",mSalesmanListData.getFirstname()+" "+mSalesmanListData.getLastname());
                    bundle.putString("sales_man_id",mSalesmanListData.getId());
                    dealAsignmentHolderFragment.setArguments(bundle);

                    ((HomeActivity) mContext).replaceFragment(dealAsignmentHolderFragment);

                }
            });
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }




    }

    public int getItemCount() {
        return this.mData.size();
    }
}
