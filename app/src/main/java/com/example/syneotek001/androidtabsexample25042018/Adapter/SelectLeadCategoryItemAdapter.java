package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.LeadsListHolderFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.LeadNoteModel;

import java.util.ArrayList;
import java.util.List;

public class SelectLeadCategoryItemAdapter extends Adapter<SelectLeadCategoryItemAdapter.MyViewHolder> {
    private final Animation anim;
    private Context mContext;
    private List<LeadNoteModel> mData = new ArrayList();

    class MyViewHolder extends ViewHolder {
        LinearLayout llNote;
        TextView tvTitle;

        MyViewHolder(View view) {
            super(view);
            llNote = view.findViewById(R.id.llNote);
            tvTitle = view.findViewById(R.id.tvTitle);
        }
    }

    public SelectLeadCategoryItemAdapter(Context context, ArrayList<LeadNoteModel> list) {
        this.mContext = context;
        this.mData = list;
        this.anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_in_out);
    }

    public int getItemViewType(int position) {

        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_email_folder, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final LeadNoteModel mLeadNoteModel = (LeadNoteModel) this.mData.get(position);
        holder.tvTitle.setText(mLeadNoteModel.getTitle());

        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                ((HomeActivity) mContext).replaceFragment(new LeadsListHolderFragment());

            }
        });

    }

    public int getItemCount() {
        return this.mData.size();
    }
}
