package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.RecyclerViewFastScroller;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
public class CarExteriorInteriorListHolderFragment extends Fragment implements OnBackPressed {
    public static String BUNDLE_DATA_IS_SELECTABLE = "is_item_selectable";
    String carModel = "";
    boolean isItemSelectable;
    FragmentTabsPagerAdapter mAdapter;
    ImageView ivBack;
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public RecyclerViewFastScroller rvFastScroller;

    TextView tvTitle;
    Context context;
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_list_holdertwo, container, false);
        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);
        tvTitle = view.findViewById(R.id.tvTitle);
        ivBack=view.findViewById(R.id.ivBack);

        if(getArguments()!=null){
            if(getArguments().getString("car_model_name")!=null && !getArguments().getString("car_model_name").equals(""))
                tvTitle.setText(getArguments().getString("car_model_name")+" "+getString(R.string.str_accessories));
        }

        ImageView ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        setupViewPager(viewPager);
        setHasOptionsMenu(true);
        Log.e("ExteriorInteriorFragmen","onCreateView");
        return view;
    }
    private void setupViewPager(ViewPager viewPager) {
//           mAdapter = new FragmentTabsPagerAdapter(getFragmentManager());
           mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
            CarExteriorInteriorListFragment fragmentExterior = new CarExteriorInteriorListFragment();
            Bundle bundleExterior = new Bundle();
            bundleExterior.putString("accessoryType", getResources().getString(R.string.exterior_accessories));
            bundleExterior.putBoolean(BUNDLE_DATA_IS_SELECTABLE, isItemSelectable);
            fragmentExterior.setArguments(bundleExterior);
            mAdapter.addFragment(fragmentExterior, getResources().getString(R.string.str_exterior));
            CarExteriorInteriorListFragment fragmentInterior = new CarExteriorInteriorListFragment();
            Bundle bundleInterior = new Bundle();
            bundleInterior.putString("accessoryType", getResources().getString(R.string.interior_accessories));
            bundleInterior.putBoolean(BUNDLE_DATA_IS_SELECTABLE, isItemSelectable);
            fragmentInterior.setArguments(bundleInterior);
            mAdapter.addFragment(fragmentInterior, getResources().getString(R.string.str_interior));
            viewPager.setAdapter(mAdapter);
            viewPager.setOffscreenPageLimit(2);
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.setTabMode(1);
            setupTabIcons();
            return;
    }

    private void setupTabIcons() {
        int tabCount = mAdapter.getCount();
        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                View llTab = LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.layout_tab, null);
                ((TextView) llTab.findViewById(R.id.tvTabTitle)).setText(mAdapter.getPageTitle(i));
                tab.setCustomView(llTab);
            }
        }
    }

    @Override
    public void onBackPressed(){
        getActivity().getSupportFragmentManager().popBackStack();

    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
        Log.e("ExteriorInteriorFragmen","onResume");
    }
}
