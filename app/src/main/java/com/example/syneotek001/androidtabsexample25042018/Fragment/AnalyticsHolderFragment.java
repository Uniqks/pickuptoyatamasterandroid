package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.data.Entry;

import java.io.Serializable;
import java.util.ArrayList;

public class AnalyticsHolderFragment extends Fragment implements OnBackPressed{
    FragmentTabsPagerAdapter mAdapter;
    public TabLayout tabLayout;
    public ViewPager viewPager;
    ImageView ivBack;
    ArrayList<LineGraphDataModel> leadsEntry;
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_tab_list_holdermoregraph, container, false);
        tabLayout = mview.findViewById(R.id.tabLayout);
        viewPager = mview.findViewById(R.id.viewPager);
        ivBack = mview.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        ImageView ivMenu;
        ivMenu= mview.findViewById(R.id.ivMenu);
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).opendrawer();
            }
        });
//        tab_analytics
        TextView tvTitle = mview.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.tab_analytics));

        leadsEntry = new ArrayList();
        int count = 8;
        float range = 800.0f;

        ArrayList<Entry> soldLeads = new ArrayList();
        for (int i = 0; i < count; i++) {
            soldLeads.add(new Entry((float) i, 10.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_pink)));
        }
        leadsEntry.add(new LineGraphDataModel("Sold", ContextCompat.getColor(getActivity(), R.color.pink_opacity_80), ContextCompat.getDrawable(getActivity(), R.drawable.fade_pink), ContextCompat.getColor(getActivity(), R.color.pink_opacity_50), soldLeads));
        setupViewPager(viewPager);
        setHasOptionsMenu(false);
        return mview;
    }
    private void setupViewPager(ViewPager viewPager) {
//        mAdapter = new FragmentTabsPagerAdapter(getFragmentManager());

        LeadsIndividualDummyFragment leadsIndividualFragment = new LeadsIndividualDummyFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(LeadsIndividualFragment.BUNDLE_DATA_SET, this.leadsEntry.get(0));
        bundle.putString(LeadsIndividualFragment.BUNDLE_DATA_SET_TYPE, getActivity().getResources().getString(R.string.sold_leads));
        leadsIndividualFragment.setArguments(bundle);

        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());

        LeadsIndividualDummyFragment leadsIndividualFragment1 = new LeadsIndividualDummyFragment();
        leadsIndividualFragment1.setArguments(bundle);

        LeadsIndividualDummyFragment leadsIndividualFragment2 = new LeadsIndividualDummyFragment();
        leadsIndividualFragment2.setArguments(bundle);

        LeadsIndividualDummyFragment leadsIndividualFragment3 = new LeadsIndividualDummyFragment();
        leadsIndividualFragment3.setArguments(bundle);

        LeadsIndividualDummyFragment leadsIndividualFragment4 = new LeadsIndividualDummyFragment();
        leadsIndividualFragment4.setArguments(bundle);

        LeadsIndividualDummyFragment leadsIndividualFragment5 = new LeadsIndividualDummyFragment();
        leadsIndividualFragment5.setArguments(bundle);

        LeadsIndividualDummyFragment leadsIndividualFragment6 = new LeadsIndividualDummyFragment();
        leadsIndividualFragment6.setArguments(bundle);


        mAdapter.addFragment(new LeadsAllTimeFragment(), "All Leads");

        /*mAdapter.addFragment(new FragmentComingSoon(), "Closing Ratio");
        mAdapter.addFragment(new FragmentComingSoon(), "In Progress Leads");
        mAdapter.addFragment(new FragmentComingSoon(), "Failure & FollowUp");
        mAdapter.addFragment(new FragmentComingSoon(), "Response Time");
        mAdapter.addFragment(new FragmentComingSoon(), "Sold");
        mAdapter.addFragment(new FragmentComingSoon(), "Income");
        mAdapter.addFragment(new FragmentComingSoon(), "Sales");*/

        /*mAdapter.addFragment(leadsIndividualFragment, "Closing Ratio");
        mAdapter.addFragment(leadsIndividualFragment1, "In Progress Leads");
        mAdapter.addFragment(leadsIndividualFragment2, "Failure & FollowUp");
        mAdapter.addFragment(leadsIndividualFragment3, "Response Time");
        mAdapter.addFragment(leadsIndividualFragment4, "Sold");
        mAdapter.addFragment(leadsIndividualFragment5, "Income");
        mAdapter.addFragment(leadsIndividualFragment6, "Sales");*/

        mAdapter.addFragment(new LeadsIndividualDummyFragment(), "Closing Ratio");
        mAdapter.addFragment(new LeadsIndividualDummyFragment(), "In Progress Leads");
        mAdapter.addFragment(new LeadsIndividualDummyFragment(), "Failure & FollowUp");
        mAdapter.addFragment(new LeadsIndividualDummyFragment(), "Response Time");
        mAdapter.addFragment(new LeadsIndividualDummyFragment(), "Sold");
        mAdapter.addFragment(new LeadsIndividualDummyFragment(), "Income");
        mAdapter.addFragment(new LeadsIndividualDummyFragment(), "Sales");

        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupTabIcons() {
        int tabCount = mAdapter.getCount();
        for (int i = 0; i < tabCount; i++) {
            Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                View llTab = (LinearLayout) LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.layout_tab_with_badge, null);
                ((TextView) llTab.findViewById(R.id.tvTabTitle)).setText(mAdapter.getPageTitle(i));
                ((TextView) llTab.findViewById(R.id.tvTabTitleBadge)).setText(String.valueOf((i + 2) * 2));
                tab.setCustomView(llTab);
            }
        }
    }
    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
    }
}
