package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.charts.LineChart;

import java.util.ArrayList;

public class LeadsIndividualFragment extends LineGraphBaseFragment implements OnClickListener {
    public static String BUNDLE_DATA_SET = "data_set";
    public static String BUNDLE_DATA_SET_TYPE = "data_set_type";
    LineGraphDataModel lineGraphModel;
    public LinearLayout llGraphContainer, llYMWContainer, rlMonth, rlWeek, rlYear, rlYearRange;
    public TextView tvMonth, tvMonthValue, tvWeek, tvWeekValue, tvYear, tvYearRangeValue, tvYearValue;
    public LinearLayout llGraph;
    public LineChart mChart;
    ImageView ivBack;
    public TextView xAxisTitle, txt_leadCount, tvTitle;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_lead_individual, container, false);

        llGraphContainer = mview.findViewById(R.id.llGraphContainer);
        llYMWContainer = mview.findViewById(R.id.llYMWContainer);
        rlMonth = mview.findViewById(R.id.rlMonth);
        rlWeek = mview.findViewById(R.id.rlWeek);
        rlYear = mview.findViewById(R.id.rlYear);
        rlYearRange = mview.findViewById(R.id.rlYearRange);
        tvMonth = mview.findViewById(R.id.tvMonth);
        tvMonthValue = mview.findViewById(R.id.tvMonthValue);
        tvWeek = mview.findViewById(R.id.tvWeek);
        tvWeekValue = mview.findViewById(R.id.tvWeekValue);
        tvYear = mview.findViewById(R.id.tvYear);
        tvYearRangeValue = mview.findViewById(R.id.tvYearRangeValue);
        tvYearValue = mview.findViewById(R.id.tvYearValue);
        llGraph = mview.findViewById(R.id.llGraph);
        mChart = mview.findViewById(R.id.mChart);
        xAxisTitle = mview.findViewById(R.id.xAxisTitle);
        txt_leadCount = mview.findViewById(R.id.txt_leadCount);
        tvTitle = mview.findViewById(R.id.tvTitle);
        ivBack = mview.findViewById(R.id.ivBack);

        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        setBinding(mChart);
        prepareLayouts(getArguments());
        setClickEvents();
        return mview;
    }

    private void setClickEvents() {
        tvYear.setOnClickListener(this);
        tvMonth.setOnClickListener(this);
        tvWeek.setOnClickListener(this);
    }

    private void prepareLayouts(Bundle bundle) {
        if (bundle != null) {
            leadType = bundle.getString(BUNDLE_DATA_SET_TYPE);
            tvTitle.setText(leadType);
//            setupToolBarWithBackArrow(toolbar.toolbar, leadType);
            lineGraphModel = (LineGraphDataModel) bundle.getSerializable(BUNDLE_DATA_SET);
            ArrayList<LineGraphDataModel> leadsEntry = new ArrayList<>();
            leadsEntry.add(lineGraphModel);
            if (lineGraphModel != null) {
                setUpChart(2010, lineGraphModel.getValueList().size() + 2010, leadsEntry);
                return;
            }
            Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            getActivity().onBackPressed();
            return;
        }
        Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvMonth:
                rlYearRange.setVisibility(View.GONE);
                rlYear.setVisibility(View.VISIBLE);
                rlMonth.setVisibility(View.VISIBLE);
                rlWeek.setVisibility(View.GONE);
                tvYear.setTextColor(ContextCompat.getColor(this.mContext, R.color.black));
                tvMonth.setTextColor(ContextCompat.getColor(this.mContext, R.color.sky));
                tvWeek.setTextColor(ContextCompat.getColor(this.mContext, R.color.black));
                return;
            case R.id.tvWeek:
                rlYearRange.setVisibility(View.GONE);
                rlYear.setVisibility(View.VISIBLE);
                rlMonth.setVisibility(View.VISIBLE);
                rlWeek.setVisibility(View.VISIBLE);
                tvYear.setTextColor(ContextCompat.getColor(this.mContext, R.color.black));
                tvMonth.setTextColor(ContextCompat.getColor(this.mContext, R.color.black));
                tvWeek.setTextColor(ContextCompat.getColor(this.mContext, R.color.sky));
                return;
            case R.id.tvYear:
                rlYearRange.setVisibility(View.VISIBLE);
                rlYear.setVisibility(View.GONE);
                rlMonth.setVisibility(View.GONE);
                rlWeek.setVisibility(View.GONE);
                tvYear.setTextColor(ContextCompat.getColor(this.mContext, R.color.sky));
                tvMonth.setTextColor(ContextCompat.getColor(this.mContext, R.color.black));
                tvWeek.setTextColor(ContextCompat.getColor(this.mContext, R.color.black));
                return;
            default:
                return;
        }
    }
}
