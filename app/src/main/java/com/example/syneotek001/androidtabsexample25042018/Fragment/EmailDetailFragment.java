package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.model.EmailListModel;


public class EmailDetailFragment extends Fragment {
    public static String BUNDLE_EMAIL_DATA = "email_data";
    EmailListModel emailModel;
//    FragmentEmailDetailBinding mBinding;

    public  ImageView ivUserLetterBg;
    private long mDirtyFlags = -1;

    public  TextView tvDateTime,tvFrom,tvMessage,tvSubjectemail,tvTo,tvUserLetter;

    class C05961 implements OnClickListener {
        C05961() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            dialog.dismiss();
            Logger.m6e("Position", String.valueOf(((AlertDialog) dialog).getListView().getCheckedItemPosition()));
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       
            View view = inflater.inflate(R.layout.fragment_email_detail, container, false);

        tvDateTime=view.findViewById(R.id.tvDateTime);
        tvFrom=view.findViewById(R.id.tvFrom);
        tvMessage=view.findViewById(R.id.tvMessage);
        tvDateTime=view.findViewById(R.id.tvDateTime);
        tvSubjectemail=view.findViewById(R.id.tvSubjectemail);
        tvTo=view.findViewById(R.id.tvTo);
        tvUserLetter=view.findViewById(R.id.tvUserLetter);
        ivUserLetterBg=view.findViewById(R.id.ivUserLetterBg);
        ImageView ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
//            this.mBinding = (FragmentEmailDetailBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_email_detail, container, false);
//            this.view = this.mBinding.getRoot();
//            setupToolBarWithBackArrow(this.mBinding.toolbar.toolbar);
            prepareView(getArguments());
       
        setHasOptionsMenu(true);
        return view;
    }

    private void prepareView(Bundle bundle) {
        if (bundle != null) {
           emailModel = (EmailListModel) bundle.getSerializable(BUNDLE_EMAIL_DATA);
            setData();
            return;
        }
      //  Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
    }

    private void setData() {
        tvFrom.setText("\"" + emailModel.getFromEmail() + "\"");
       tvTo.setText("\"" + emailModel.getToEmail() + "\"");
       tvSubjectemail.setText(emailModel.getTitle());
       tvDateTime.setText(TimeStamp.millisToTimeFormat(Long.valueOf(emailModel.getDateTime() * 1000)));
        String title = emailModel.getTitle().toUpperCase();
        if (title.length() > 0) {
           tvUserLetter.setText(title.substring(0, 1).toUpperCase());
        } else {
           tvUserLetter.setText("T");
        }
      tvMessage.setText(emailModel.getSubject());
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_email, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_email_delete:
            case R.id.menu_email_forward:
            case R.id.menu_email_reply:
                return true;
            case R.id.menu_email_folder:
                displayDialog("Select Folder", new String[]{"Test", "Draft New", "MyTrash", "BankingEmail", "Job Emails"});
                return true;
            default:
                return false;
        }
    }

    private void displayDialog(String title, String[] items) {
        new Builder( getActivity()).setTitle((CharSequence) title).setSingleChoiceItems((CharSequence[]) items, 0, null).setPositiveButton((int) R.string.ok, new C05961()).show();
    }
}
