package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 9/8/18.
 */

public class SalesmanListResponse implements Serializable {
    public ArrayList<SalesmanListData> salesmanList;

    public ArrayList<SalesmanListData> getSalesmanList() {
        return salesmanList;
    }

    public void setSalesmanList(ArrayList<SalesmanListData> salesmanList) {
        this.salesmanList = salesmanList;
    }

    public class SalesmanListData implements Serializable {

        String id;
        String firstname;
        String lastname;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }
    }
}
