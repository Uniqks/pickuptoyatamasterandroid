package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.syneotek001.androidtabsexample25042018.R;


public class ContactDetailFragment extends Fragment implements OnClickListener {

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            View rootview = inflater.inflate(R.layout.fragment_contact_detail, container, false);
        ImageView ivBack=rootview.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });


       // setClickEvents();
        return rootview;
    }

   /* private void setClickEvents() {
       tvFirstName.setText("Spouse Name");
      tvTitle.setText("Mrs.");
      tvEmail.setText("spouse@sit.com");
      tvMobile.setText("9977662364");
    }*/

    public void onClick(View view) {
        view.getId();
    }
}
