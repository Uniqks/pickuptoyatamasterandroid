package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DealRequestFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final int SELECT_PICTURE = 1;
    TextView tv_btn_master_crm, tv_btn_business_manager;
    TextView tv_deal, tv_deal_no, tv_note, tv_notewrite, tv_notes, tv_write_notes, tv_status, tv_deliverd;
    TextView tv_date, tv_date_time, tv_attachment, tv_messages, tv_type_message, tv_send;
    CircleImageView profile_image, profile_image1;
    TextView tv_message_txt, tv_date_time_message, tv_message_txt1, tv_date_time_message1;
    LinearLayout tv_deal_layout, tv_note_layout, tv_notes1_layout, tve_date_time_layout, tv_attached_layout,
            tv_layout_message_type, tv_first_layout_message, tv_second_massage_layout, tv_status_layout, btn_layout;

    Spinner spinner_deal;
    Context mContext;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deal_request, container, false);
        ImageView iv_Close = view.findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        profile_image = view.findViewById(R.id.profile_image);
//        profile_image1 = view.findViewById(R.id.profile_image1);

        tv_btn_master_crm = view.findViewById(R.id.tv_btn_master_crm);
        tv_btn_business_manager = view.findViewById(R.id.tv_btn_business_manager);

//        tv_deal = view.findViewById(R.id.tv_deal);
        tv_deal_no = view.findViewById(R.id.tv_deal_no);
//        tv_note = view.findViewById(R.id.tv_note);
//        tv_notewrite = view.findViewById(R.id.tv_notewrite);
//        tv_notes = view.findViewById(R.id.tv_notes);
//        tv_write_notes = view.findViewById(R.id.tv_write_notes);
        tv_status = view.findViewById(R.id.tv_status);
        tv_deliverd = view.findViewById(R.id.tv_deliverd);
//        tv_date = view.findViewById(R.id.tv_date);
//        tv_date_time = view.findViewById(R.id.tv_date_time);
//        tv_attachment = view.findViewById(R.id.tv_attachment);
//        tv_messages = view.findViewById(R.id.tv_messages);
//        tv_type_message = view.findViewById(R.id.tv_type_message);
//        tv_send = view.findViewById(R.id.tv_send);
//        tv_message_txt = view.findViewById(R.id.tv_message_txt);
//        tv_date_time_message = view.findViewById(R.id.tv_date_time_message);
//        tv_message_txt1 = view.findViewById(R.id.tv_message_txt1);
//        tv_date_time_message1 = view.findViewById(R.id.tv_date_time_message1);
        /*layout*/
//        tv_deal_layout = view.findViewById(R.id.tv_deal_layout);
//        tv_note_layout = view.findViewById(R.id.tv_note_layout);
//        tv_notes1_layout = view.findViewById(R.id.tv_notes1_layout);
//        tv_status_layout = view.findViewById(R.id.tv_status_layout);
//        tve_date_time_layout = view.findViewById(R.id.tve_date_time_layout);
//        tv_attached_layout = view.findViewById(R.id.tv_attached_layout);
//        tv_attached_layout = view.findViewById(R.id.tv_attached_layout);
//        tv_layout_message_type = view.findViewById(R.id.tv_layout_message_type);
//        tv_first_layout_message = view.findViewById(R.id.tv_first_layout_message);
//        tv_second_massage_layout = view.findViewById(R.id.tv_second_massage_layout);
        btn_layout = view.findViewById(R.id.btn_layout);

        tv_btn_business_manager.setBackgroundColor(Color.RED);
        tv_btn_master_crm.setBackgroundColor(Color.GRAY);

        tv_btn_business_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_btn_business_manager.setBackgroundColor(Color.RED);
                tv_btn_master_crm.setBackgroundColor(Color.GRAY);
                btn_layout.setBackgroundColor(Color.RED);
                tv_deal_layout.setVisibility(View.VISIBLE);
                tv_note_layout.setVisibility(View.VISIBLE);
                tv_notes1_layout.setVisibility(View.VISIBLE);
                tv_status_layout.setVisibility(View.VISIBLE);
                tve_date_time_layout.setVisibility(View.VISIBLE);
                tv_attached_layout.setVisibility(View.VISIBLE);
                tv_layout_message_type.setVisibility(View.VISIBLE);
                tv_first_layout_message.setVisibility(View.VISIBLE);
                tv_second_massage_layout.setVisibility(View.VISIBLE);
                tv_messages.setVisibility(View.VISIBLE);

            }
        });
        tv_btn_master_crm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_btn_master_crm.setBackgroundColor(Color.RED);
                tv_btn_business_manager.setBackgroundColor(Color.GRAY);
                btn_layout.setBackgroundColor(Color.RED);
                tv_deal_layout.setVisibility(View.INVISIBLE);
                tv_deal_layout.setVisibility(View.GONE);

                tv_status_layout.setVisibility(View.INVISIBLE);
                tv_status_layout.setVisibility(View.GONE);

                tve_date_time_layout.setVisibility(View.INVISIBLE);
                tve_date_time_layout.setVisibility(View.GONE);

                tv_first_layout_message.setVisibility(View.INVISIBLE);
                tv_first_layout_message.setVisibility(View.GONE);

              /*  tv_deal_layout.setVisibility(View.INVISIBLE);
                tv_deal_layout.setVisibility(View.GONE);*/

            }
        });
//        spinner_deal = view.findViewById(R.id.spinner_deal);
        spinner_deal.setOnItemSelectedListener(this);
        List<String> request = new ArrayList<String>();
        request.add("Request1");
        request.add("Request2 ");
        request.add("Request3");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, request);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_deal.setAdapter(dataAdapter);

        tv_attached_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image*//*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
            }
        });
        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        // On selecting a spinner item
        String item = (String) adapterView.getItemAtPosition(i);

        // Showing selected spinner item
        Toast.makeText(getActivity(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
