package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.ContactIndex;

import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 19/7/18.
 */

public class ContactIndexAdapter extends RecyclerView.Adapter<ContactIndexAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<ContactIndex> mData;
    public interface ContactIndexClick {
        public void onContactIndexClick(int position);
    }
    ContactIndexClick contactIndexClick;
    public void setContactIndexClick(ContactIndexClick contactIndexClick){
        this.contactIndexClick=contactIndexClick;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvIndexName, tvIndexValue;
        LinearLayout lyIndex;
        ImageView ivTriangle;

        MyViewHolder(View view) {
            super(view);
            tvIndexName = view.findViewById(R.id.tvIndexName);
            tvIndexValue = view.findViewById(R.id.tvIndexValue);
            lyIndex = view.findViewById(R.id.lyIndex);
            ivTriangle = view.findViewById(R.id.ivTriangle);

        }

        @Override
        public void onClick(View v) {

        }
    }

    public ContactIndexAdapter(Context context, ArrayList<ContactIndex> list) {
        this.mContext = context;
        this.mData = list;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_contact_index, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        ContactIndex mContactIndexItem = (ContactIndex) mData.get(position);
        holder.tvIndexName.setText(mContactIndexItem.getIndex_name());
        holder.tvIndexValue.setText("("+mContactIndexItem.getIndex_value()+")");
        holder.tvIndexName.setTextColor(ContextCompat.getColor(mContext,R.color.white));
        holder.tvIndexValue.setTextColor(ContextCompat.getColor(mContext,R.color.white));

        if (mContactIndexItem.isIs_selected()) {
            holder.lyIndex.setBackgroundResource(R.drawable.bg_oval_contact_index);
            holder.ivTriangle.setVisibility(View.VISIBLE);
        } else {
            holder.lyIndex.setBackground(null);
            holder.ivTriangle.setVisibility(View.GONE);
        }

        holder.lyIndex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactIndexClick.onContactIndexClick(position);
            }
        });


    }


    public int getItemCount() {
        return mData.size();
    }

}
