package com.example.syneotek001.androidtabsexample25042018.rest;

public interface ApiResponseInterface {
    public void isError(String errorCode);
    public void isSuccess(Object response, int ServiceCode);
}