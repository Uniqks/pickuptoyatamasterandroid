package com.example.syneotek001.androidtabsexample25042018.model;

public class CarExteriorInteriorModel {
    private String description = "";
    private String id = "";
    private String image = "";
    private boolean isSelected;
    private String price = "";
    private int starCount = 0;
    private String title = "";
    private String type = "";

    public CarExteriorInteriorModel(String id, String title, String price, String image, String type, String description, int starCount, boolean isSelected) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.image = image;
        this.type = type;
        this.description = description;
        this.starCount = starCount;
        this.isSelected = isSelected;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStarCount() {
        return this.starCount;
    }

    public void setStarCount(int starCount) {
        this.starCount = starCount;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    public String toString() {
        return "CarExteriorInteriorModel{id='" + this.id + '\'' + ", title='" + this.title + '\'' + ", price='" + this.price + '\'' + ", image='" + this.image + '\'' + ", type='" + this.type + '\'' + ", description='" + this.description + '\'' + ", starCount=" + this.starCount + ", isSelected=" + this.isSelected + '}';
    }
}
