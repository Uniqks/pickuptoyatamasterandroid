package com.example.syneotek001.androidtabsexample25042018.model;

import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 9/8/18.
 */

public class SendMessageResponse {
    public ChatMessageData data;

    public ChatMessageData getData() {
        return data;
    }

    public void setData(ChatMessageData data) {
        this.data = data;
    }

    public class ChatMessageData {

        public MessageData msg_data;

        public MessageData getMsg_data() {
            return msg_data;
        }

        public void setMsg_data(MessageData msg_data) {
            this.msg_data = msg_data;
        }

        public class MessageData {
            String id;
            String message;
            String time;
            String user_id;
            String receiver;
            String storage_a;
            String storage_b;
            String status;
            String msg_type;
            String is_received;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getReceiver() {
                return receiver;
            }

            public void setReceiver(String receiver) {
                this.receiver = receiver;
            }

            public String getStorage_a() {
                return storage_a;
            }

            public void setStorage_a(String storage_a) {
                this.storage_a = storage_a;
            }

            public String getStorage_b() {
                return storage_b;
            }

            public void setStorage_b(String storage_b) {
                this.storage_b = storage_b;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getMsg_type() {
                return msg_type;
            }

            public void setMsg_type(String msg_type) {
                this.msg_type = msg_type;
            }

            public String getIs_received() {
                return is_received;
            }

            public void setIs_received(String is_received) {
                this.is_received = is_received;
            }
        }

        String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
