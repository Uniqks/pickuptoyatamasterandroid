package com.example.syneotek001.androidtabsexample25042018.Utils;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;

public class HeaderItemDecoration extends ItemDecoration {
    private StickyHeaderInterface mListener;

    public interface StickyHeaderInterface {
        void bindHeaderData(View view, int i);

        int getHeaderLayout(int i);

        int getHeaderPositionForItem(int i);

        void headerClickListener(View view, int i);

        boolean isHeader(int i);
    }

    public HeaderItemDecoration(RecyclerView recyclerView, @NonNull StickyHeaderInterface listener) {
        this.mListener = listener;
    }

    public void onDrawOver(Canvas c, RecyclerView parent, State state) {
        super.onDrawOver(c, parent, state);
        View topChild = parent.getChildAt(0);
        if (topChild != null) {
            int topChildPosition = parent.getChildAdapterPosition(topChild);
            if (topChildPosition != -1) {
                View currentHeader = getHeaderViewForItem(topChildPosition, parent);
                fixLayoutSize(parent, currentHeader);
                View childInContact = getChildInContact(parent, currentHeader.getBottom());
                if (childInContact == null) {
                    return;
                }
                if (this.mListener.isHeader(parent.getChildAdapterPosition(childInContact))) {
                    moveHeader(c, currentHeader, childInContact);
                } else {
                    drawHeader(c, currentHeader);
                }
            }
        }
    }

    private View getHeaderViewForItem(int itemPosition, RecyclerView parent) {
        int headerPosition = this.mListener.getHeaderPositionForItem(itemPosition);
        View header = LayoutInflater.from(parent.getContext()).inflate(this.mListener.getHeaderLayout(headerPosition), parent, false);
        this.mListener.bindHeaderData(header, headerPosition);
        return header;
    }

    private void drawHeader(Canvas c, View header) {
        c.save();
        c.translate(0.0f, 0.0f);
        header.draw(c);
        c.restore();
    }

    private void moveHeader(Canvas c, View currentHeader, View nextHeader) {
        c.save();
        c.translate(0.0f, (float) (nextHeader.getTop() - currentHeader.getHeight()));
        currentHeader.draw(c);
        c.restore();
    }

    private View getChildInContact(RecyclerView parent, int contactPoint) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            if (child.getBottom() > contactPoint && child.getTop() <= contactPoint) {
                return child;
            }
        }
        return null;
    }

    @SuppressLint("WrongConstant")
    private void fixLayoutSize(ViewGroup parent, View view) {
        view.measure(ViewGroup.getChildMeasureSpec(MeasureSpec.makeMeasureSpec(parent.getWidth(),1073741824), parent.getPaddingLeft() + parent.getPaddingRight(), view.getLayoutParams().width), ViewGroup.getChildMeasureSpec(MeasureSpec.makeMeasureSpec(parent.getHeight(), 0), parent.getPaddingTop() + parent.getPaddingBottom(), view.getLayoutParams().height));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
    }
}
