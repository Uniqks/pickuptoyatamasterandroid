package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.ChatItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Support.RecyclerItemClickListener;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.model.ChatListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.ChatListResponse.ChatListData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChatListFragment extends Fragment implements ApiResponseInterface {
    ChatItemAdapter mAdapter;
    ArrayList<ChatListData> mCarList = new ArrayList<>();
    View view;

    public RecyclerView recyclerView;
    public TextView tvNoDataFound;
    ApiManager apiManager;
    int fromlimit = 1;
    Handler handler;
    boolean loadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mview = inflater.inflate(R.layout.fragment_common_recycler_view_list, container, false);
        apiManager = new ApiManager(getActivity(), this);
        recyclerView = mview.findViewById(R.id.recyclerView);
        tvNoDataFound = mview.findViewById(R.id.tvNoDataFound);
        handler = new Handler();
        /*recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ChatListFragment", " onClick recyclerView ");
            }
        });*/
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
        mAdapter = new ChatItemAdapter(getActivity(), this.mCarList, recyclerView);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                Log.e("ChatListFragment", " onLoadMore ");

                //add null , so the adapter will check view_type and show progress bar at bottom
                mCarList.add(null);
                recyclerView.post(new Runnable() {
                    public void run() {
                        mAdapter.notifyItemInserted(mCarList.size() - 1);
                    }
                });

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //   remove progress item
                        if (mCarList.size() > 0)
                            mCarList.remove(mCarList.size() - 1);

                        mAdapter.notifyItemRemoved(mCarList.size());

                        if (Utils.isOnline()) {
                            fromlimit = fromlimit + 1;
                            loadMore = true;
                            apiManager.getChatList(fromlimit, AppConstant.CHAT_LIST_ACTION, Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID));

                        } else {
                            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                        }

                    }
                }, 1000);
            }
        });


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        ChatMessagesFragment chatMessages = new ChatMessagesFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(ChatMessagesFragment.BUNDLE_CHAT_FRIEND_MODEL, (Serializable) mCarList.get(position));
                        bundle.putBoolean(Utils.EXTRA_IS_FROM_CHAT, true);
                        chatMessages.setArguments(bundle);
                        ((HomeActivity) getActivity()).replaceFragment(chatMessages);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        setUpRecyclerView();
        setHasOptionsMenu(false);
        return mview;
    }

    private void setUpRecyclerView() {
        if (Utils.isOnline()) {

            loadMore = false;
            mCarList.clear();
            LogUtils.i("BookingHistory" + " loadData arr_myrequestlist size " + mCarList.size());
            //getTripHistory(loadMore, 0, pos/*,TripHistoryActivity.this*/);
            apiManager.getChatList(fromlimit, AppConstant.CHAT_LIST_ACTION, Utils.getInstance(getActivity()).getString(Utils.PREF_USER_ID));
        } else {

            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
        }
    }


    @Override
    public void isError(String errorCode) {

    }

    @Override
    public void isSuccess(Object response, int ServiceCode) {
        ChatListResponse chatListResponse = (ChatListResponse) response;
        Log.e("loadmore_Count", "" + loadMore);

        final List<ChatListData> taxiList = chatListResponse.getData();


        if (taxiList.size() > 0) {

            mCarList.addAll(taxiList);

            if (loadMore) {
                Log.e("loadmore_Count", "if");
                mAdapter.notifyItemInserted(mCarList.size());
                mAdapter.notifyDataSetChanged();
                mAdapter.setLoaded();
                LogUtils.i("BookingHistory" + " getTripHistory onResponse loadMore arr_myrequestlist.size() " + mCarList.size());

            } else {

                Log.e("loadmore_Count", "else");
                recyclerView.setVisibility(View.VISIBLE);

                Log.i("json", mCarList.size() + " arr_myrequestlist ");
                recyclerView.setAdapter(mAdapter);
                mAdapter.setLoaded();

                LogUtils.i("BookingHistory" + " getTripHistory onResponse !loadMore arr_myrequestlist.size() " + mCarList.size());

            }

        } else {

            if (!loadMore) {

                if (mCarList.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
//                                    empty_view.setVisibility(View.VISIBLE);

                    tvNoDataFound.setVisibility(View.VISIBLE);

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    tvNoDataFound.setVisibility(View.GONE);
                }
            }
        }

    }

    public interface OnLoadMoreListener {

        void onLoadMore();

    }

}
