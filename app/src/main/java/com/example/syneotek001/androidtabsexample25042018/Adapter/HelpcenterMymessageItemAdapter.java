package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.HelpCenterMymessageItemModel;

import java.util.ArrayList;

//import com.example.syneotek001.androidtabsexample25042018.Fragment.AddTaskFragment;
//import com.example.syneotek001.androidtabsexample25042018.Fragment.TaskListFragment;

public class HelpcenterMymessageItemAdapter extends RecyclerView.Adapter<HelpcenterMymessageItemAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<HelpCenterMymessageItemModel> mData;

    class MyViewHolder extends ViewHolder implements OnClickListener {

        TextView tvMessage, tvDateTime,tvTicketId;
        LinearLayout lyAction;
        ImageView ivAction;

        MyViewHolder(View view) {
            super(view);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvTicketId = view.findViewById(R.id.tvTicketId);
            lyAction = view.findViewById(R.id.lyAction);
            ivAction = view.findViewById(R.id.ivAction);

        }

        @Override
        public void onClick(View v) {

        }
    }

    public HelpcenterMymessageItemAdapter(Context context, ArrayList<HelpCenterMymessageItemModel> list) {
        this.mContext = context;
        this.mData = list;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_helpcenter_mymessages, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        HelpCenterMymessageItemModel mAnnouncementItem = (HelpCenterMymessageItemModel) mData.get(position);
        holder.tvTicketId.setText(mAnnouncementItem.getTicket_id());
        holder.tvMessage.setText(mAnnouncementItem.getMessage());
        holder.tvDateTime.setText(mAnnouncementItem.getDatetime());

        if (mAnnouncementItem.getAction().equals("0")) {
            holder.lyAction.setBackgroundResource(R.drawable.bg_circle_red);
            holder.ivAction.setBackgroundResource(R.drawable.ic_unread);
        } else if (mAnnouncementItem.getAction().equals("1")) {
            holder.lyAction.setBackgroundResource(R.drawable.bg_circle_green);
            holder.ivAction.setBackgroundResource(R.drawable.ic_read);
        }

    }


    public int getItemCount() {
        return mData.size();
    }

}
