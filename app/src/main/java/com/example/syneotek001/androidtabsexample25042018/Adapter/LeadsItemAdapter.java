package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LeadItemModel;

import java.util.ArrayList;
import java.util.Iterator;


public class LeadsItemAdapter extends RecyclerView.Adapter<LeadsItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<LeadItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;
    private boolean isBlinked = false;
    private boolean is_from_notification = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivDescription:
                if (LeadsItemAdapter.this.onItemClickListener != null) {
                    LeadsItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivAssign:
                if (LeadsItemAdapter.this.onItemClickListener != null) {
                    LeadsItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivCall:
                if (LeadsItemAdapter.this.onItemClickListener != null) {
                    LeadsItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivAssign, ivDescription, ivCall;
        LinearLayout llActionItems;
        LinearLayout ll_LastActivityDate, ll_Email, ll_Phone, ll_SourceLink;
        TextView tvAssignDate, tvCustomerId, tvEmail, tvLastActivityDate, tvName, tvPhone, tvSourceLink;
        View viewDummySpace;
        RelativeLayout rlBorder;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivAssign = view.findViewById(R.id.ivAssign);
            ivDescription = view.findViewById(R.id.ivDescription);
            ivCall = view.findViewById(R.id.ivCall);
            llActionItems = view.findViewById(R.id.llActionItems);
            tvAssignDate = view.findViewById(R.id.tvAssignDate);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvEmail = view.findViewById(R.id.tvEmail);
            tvLastActivityDate = view.findViewById(R.id.tvLastActivityDate);
            tvName = view.findViewById(R.id.tvName);
            tvPhone = view.findViewById(R.id.tvPhone);
            tvSourceLink = view.findViewById(R.id.tvSourceLink);
            viewDummySpace = view.findViewById(R.id.viewDummySpace);

            ll_LastActivityDate = view.findViewById(R.id.ll_LastActivityDate);
            ll_Email = view.findViewById(R.id.ll_Email);
            ll_Phone = view.findViewById(R.id.ll_Phone);
            ll_SourceLink = view.findViewById(R.id.ll_SourceLink);
            rlBorder = view.findViewById(R.id.rlBorder);



        }

        @Override
        public void onClick(View v) {

        }
    }

    public LeadsItemAdapter(Context context, ArrayList<LeadItemModel> list, OnLeadItemViewClickListener onItemClickListener,boolean is_from_notification) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
        this.is_from_notification = is_from_notification;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_leads, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        LeadItemModel mLeadItem = (LeadItemModel) mData.get(position);
        holder.tvCustomerId.setText(mLeadItem.getCustomerId());
        holder.tvAssignDate.setText(TimeStamp.millisToTimeFormat(mLeadItem.getAssignDate()));
        holder.tvLastActivityDate.setText(TimeStamp.millisToTimeFormat(mLeadItem.getLastActivity()));
        holder.tvName.setText(mLeadItem.getClientName());
        holder.tvEmail.setText(mLeadItem.getEmail());
        holder.tvPhone.setText(mLeadItem.getPhone());
        holder.tvSourceLink.setText(mLeadItem.getSourceLink());

        Iterator it;
        ArrayList<View> viewList;
        if (mLeadItem.isSelected()) {
            viewList = new ArrayList<>();
            viewList.add(holder.ll_LastActivityDate);
            viewList.add(holder.ll_Email);
            viewList.add(holder.ll_Phone);
            viewList.add((holder.ll_SourceLink));
            viewList.add(holder.viewDummySpace);
            viewList.add(holder.llActionItems);
            it = viewList.iterator();

            while (it.hasNext()) {
                View view = (View) it.next();
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0.0f);
                view.animate().alpha(1.0f).setListener(null);
            }
            return;
        }
        viewList = new ArrayList<>();
        viewList.add(holder.ll_LastActivityDate);
        viewList.add(holder.ll_Email);
        viewList.add(holder.ll_Phone);
        viewList.add(holder.ll_SourceLink);
        viewList.add(holder.viewDummySpace);
        viewList.add(holder.llActionItems);
        it = viewList.iterator();
        while (it.hasNext()) {
            final View view = (View) it.next();
            view.animate().alpha(0.0f).setListener(new AnimatorListenerAdapter() {
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    view.setVisibility(View.GONE);
                }
            });
        }

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    LeadItemModel mLeadItem = (LeadItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });


        holder.ivDescription.setOnClickListener(this);
        holder.ivAssign.setOnClickListener(this);
        holder.ivCall.setOnClickListener(this);


        if (position == 3 && !isBlinked && is_from_notification)
            manageBlinkEffect(holder.rlBorder);

    }

    private void manageBlinkEffect(final RelativeLayout cardView) {

        int colorFrom;
        int colorTo = mContext.getResources().getColor(R.color.white);

        ValueAnimator colorAnimation;

        colorFrom = mContext.getResources().getColor(R.color.red);
        colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), new Object[]{Integer.valueOf(colorFrom), Integer.valueOf(colorTo)});
        colorAnimation.setDuration(1000);
        colorAnimation.setRepeatCount(3);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animator) {
                cardView.setBackgroundColor(((Integer) animator.getAnimatedValue()).intValue());
            }
        });
        colorAnimation.start();
        isBlinked = true;

    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
