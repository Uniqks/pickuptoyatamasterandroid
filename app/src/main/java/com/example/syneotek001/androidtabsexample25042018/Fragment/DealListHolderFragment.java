package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.Tab;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;


public class DealListHolderFragment extends Fragment implements OnBackPressed {
    FragmentTabsPagerAdapter mAdapter;
    FragmentTabListHolderBinding mBinding;

    public TabLayout tabLayout;
    public ViewPager viewPager;
    ImageView ivBack;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_tab_list_holderthree, container, false);
        tabLayout = mview.findViewById(R.id.tabLayout);
        viewPager = mview.findViewById(R.id.viewPager);
        ivBack = mview.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        setupViewPager(viewPager);
        setHasOptionsMenu(true);
        return mview;
    }

    private void setupViewPager(ViewPager viewPager) {
        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
        mAdapter.addFragment(new DealRequestListFragment(), getResources().getString(R.string.deal_request));
        mAdapter.addFragment(new DealsListFragment(), getResources().getString(R.string.deal_list));
        mAdapter.addFragment(new DealsListFragment(), getResources().getString(R.string.assigned_deal));
        mAdapter.addFragment(new DealsListFragment(), getResources().getString(R.string.draft));
        mAdapter.addFragment(new DealLeadsListFragment(), getResources().getString(R.string.leads));
        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
    }

    private void setupTabIcons() {
        int tabCount = mAdapter.getCount();
        for (int i = 0; i < tabCount; i++) {
            Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                View llTab = LayoutInflater.from(tabLayout.getContext()).inflate(R.layout.layout_tab_with_badge, null);
                ((TextView) llTab.findViewById(R.id.tvTabTitle)).setText(mAdapter.getPageTitle(i));
                ((TextView) llTab.findViewById(R.id.tvTabTitleBadge)).setText(String.valueOf((i + 2) * 2));
                tab.setCustomView(llTab);
            }
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
    }
}
