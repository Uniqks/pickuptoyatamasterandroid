package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;


/**
 * Created by bluegenie-24 on 12/6/18.
 */

public class AdapterLinechartLegend extends BaseAdapter {
    Context context;
    String[] labels;
    int[] colors;
    LayoutInflater layoutInflater;

    public AdapterLinechartLegend(Context context, String[] labels, int[] colors) {
        this.context = context;
        this.labels = labels;
        this.colors = colors;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return labels.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        convertView = layoutInflater.inflate(R.layout.adapter_linechart_legend, null);

        TextView tv_legend_value = (TextView) convertView.findViewById(R.id.tv_legend_value);
        ImageView iv_legend_shape = (ImageView) convertView.findViewById(R.id.iv_legend_shape);

        tv_legend_value.setText(labels[i]+"");
        tv_legend_value.setTextColor(colors[i]);
//        iv_legend_shape.setBackgroundColor(colors[i]);

        GradientDrawable bgShape = (GradientDrawable)iv_legend_shape.getBackground();
        bgShape.setColor(colors[i]);

        return convertView;
    }
}
