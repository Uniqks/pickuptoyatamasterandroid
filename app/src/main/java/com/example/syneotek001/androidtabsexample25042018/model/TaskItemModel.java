package com.example.syneotek001.androidtabsexample25042018.model;

public class TaskItemModel {

    String task_id;
    String task_title;
    String task_category;
    String task_status;
    String task_date;
    String task_notes;
    boolean isSelected = false;

    public TaskItemModel(String task_id, String task_title, String task_category, String task_status, String task_date, String task_notes) {
        this.task_id = task_id;
        this.task_title = task_title;
        this.task_category = task_category;
        this.task_status = task_status;
        this.task_date = task_date;
        this.task_notes = task_notes;
        this.isSelected = false;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_title() {
        return task_title;
    }

    public void setTask_title(String task_title) {
        this.task_title = task_title;
    }

    public String getTask_category() {
        return task_category;
    }

    public void setTask_category(String task_category) {
        this.task_category = task_category;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public String getTask_date() {
        return task_date;
    }

    public void setTask_date(String task_date) {
        this.task_date = task_date;
    }

    public String getTask_notes() {
        return task_notes;
    }

    public void setTask_notes(String task_notes) {
        this.task_notes = task_notes;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }
}
