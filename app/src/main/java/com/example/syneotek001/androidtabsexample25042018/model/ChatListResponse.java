package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 9/8/18.
 */

public class ChatListResponse implements Serializable {
    public ArrayList<ChatListData> data;

    public ArrayList<ChatListData> getData() {
        return data;
    }

    public void setData(ArrayList<ChatListData> data) {
        this.data = data;
    }

    public class ChatListData implements Serializable {

        String id;
        String user_id;
        String friend;
        String friendname;
        String friendimage;
        String session_status;
        String total_unread_messages;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFriend() {
            return friend;
        }

        public void setFriend(String friend) {
            this.friend = friend;
        }

        public String getFriendname() {
            return friendname;
        }

        public void setFriendname(String friendname) {
            this.friendname = friendname;
        }

        public String getSession_status() {
            return session_status;
        }

        public void setSession_status(String session_status) {
            this.session_status = session_status;
        }

        public String getFriendimage() {
            return friendimage;
        }

        public void setFriendimage(String friendimage) {
            this.friendimage = friendimage;
        }

        public String getTotal_unread_messages() {
            return total_unread_messages;
        }

        public void setTotal_unread_messages(String total_unread_messages) {
            this.total_unread_messages = total_unread_messages;
        }
    }
}
