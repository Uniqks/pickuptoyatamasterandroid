package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.ContactIndex;

import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 19/7/18.
 */

public class ContactIndexAdapter1 extends BaseAdapter {
    LayoutInflater inflater;
    ArrayList<ContactIndex> arrContactIndex = new ArrayList<>();
    Context context;

    public ContactIndexAdapter1(Context context, ArrayList<ContactIndex> arrContactIndex){
        this.context = context;
        this.arrContactIndex = arrContactIndex;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return arrContactIndex.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.adapter_contact_index, parent, false);
        TextView tvIndexName,tvIndexValue;
        tvIndexName = convertView.findViewById(R.id.tvIndexName);
        tvIndexValue = convertView.findViewById(R.id.tvIndexValue);
        tvIndexName.setTextColor(ContextCompat.getColor(context,R.color.white));
        tvIndexValue.setTextColor(ContextCompat.getColor(context,R.color.white));
        tvIndexName.setText(arrContactIndex.get(position).getIndex_name());
        tvIndexValue.setText(arrContactIndex.get(position).getIndex_value());
        return convertView;
    }
}
