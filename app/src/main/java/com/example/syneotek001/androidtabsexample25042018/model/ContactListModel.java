package com.example.syneotek001.androidtabsexample25042018.model;

import android.support.annotation.NonNull;

public class ContactListModel implements Comparable<ContactListModel> {
    private String email = "";
    private String firstName = "";
    private String id = "";
    private boolean isHeader;
    private String lastName = "";
    private String letter = "";
    private String phone = "";

    public ContactListModel(String id, String firstName, String lastName, String email, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
    }

    public ContactListModel(String id, String firstName, String lastName, String letter, String email, String phone, boolean isHeader) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.letter = letter;
        this.email = email;
        this.phone = phone;
        this.isHeader = isHeader;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLetter() {
        return this.letter.toUpperCase();
    }

    public void setLetter(String letter) {
        this.letter = letter.toUpperCase();
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isHeader() {
        return this.isHeader;
    }

    public void setHeader(boolean header) {
        this.isHeader = header;
    }

    public String toString() {
        return getFirstName();
    }

    public int compareTo(@NonNull ContactListModel contactListModel) {
        int compare = getFirstName().compareTo(contactListModel.getFirstName());
        if (compare == 0) {
            return 1;
        }
        return compare;
    }
}
