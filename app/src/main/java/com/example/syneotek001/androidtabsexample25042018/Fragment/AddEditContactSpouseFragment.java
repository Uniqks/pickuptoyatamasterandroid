package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;


public class AddEditContactSpouseFragment extends Fragment implements OnClickListener, OnBackPressed {
    public Button btnContinue;
    public EditText etEmail, etFirstName, etHomePhone, etLastName, etMobile, etTitle, etWorkFax, etWorkPhone;
    public ImageView ivBack, ivContact, ivEmail, ivMobile, ivTitle;
    public TextView tvPageIndicator, tvTitle;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_add_contact_spouse, container, false);
        btnContinue = mview.findViewById(R.id.btnContinue);
        etEmail = mview.findViewById(R.id.etEmail);
        etFirstName = mview.findViewById(R.id.etFirstName);
        etHomePhone = mview.findViewById(R.id.etHomePhone);
        etLastName = mview.findViewById(R.id.etLastName);
        etMobile = mview.findViewById(R.id.etMobile);
        etTitle = mview.findViewById(R.id.etTitle);
        etWorkFax = mview.findViewById(R.id.etWorkFax);
        etWorkPhone = mview.findViewById(R.id.etWorkPhone);
        ivContact = mview.findViewById(R.id.ivContact);
        ivEmail = mview.findViewById(R.id.ivEmail);
        ivMobile = mview.findViewById(R.id.ivMobile);
        ivTitle = mview.findViewById(R.id.ivTitle);
        tvPageIndicator = mview.findViewById(R.id.tvPageIndicator);
        tvTitle = mview.findViewById(R.id.tvTitle);
        ivBack = mview.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity()!=null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        setHasOptionsMenu(false);
        setClickEvents();
        return mview;
    }

    private void setClickEvents() {
        btnContinue.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnContinue:
                ((HomeActivity) getActivity()).replaceFragment(new AddEditContactHomeAddress());
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }
}
