package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.DealLeadsItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.DealLeadsModel;

import java.util.ArrayList;

public class DealLeadsListFragment extends Fragment {
    DealLeadsItemAdapter mAdapter;
    ArrayList<DealLeadsModel> mDealRequestList = new ArrayList<>();
    public RecyclerView recyclerView;
    public TextView tvNoDealRequestFound;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.fragment_deal_request_list, container, false);
        recyclerView = mview.findViewById(R.id.recyclerView);
        tvNoDealRequestFound = mview.findViewById(R.id.tvNoDealRequestFound);
        setUpRecyclerView();
        setHasOptionsMenu(false);
        return mview;
    }

    private void setUpRecyclerView() {
        mDealRequestList.add(new DealLeadsModel("Rikkin Modi", "168946121555", "21 Apr 2018"));
        mDealRequestList.add(new DealLeadsModel("Aakash Patel", "158926194655", "29 Mar 2018"));
        mDealRequestList.add(new DealLeadsModel("Paresh Panchal", "149556559955", "21 Mar 2018"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new DealLeadsItemAdapter(getActivity(), mDealRequestList);
        recyclerView.setAdapter(mAdapter);
        refreshData();
    }

    private void refreshData() {
        if (mDealRequestList.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoDealRequestFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
        tvNoDealRequestFound.setVisibility(View.VISIBLE);
    }
}
