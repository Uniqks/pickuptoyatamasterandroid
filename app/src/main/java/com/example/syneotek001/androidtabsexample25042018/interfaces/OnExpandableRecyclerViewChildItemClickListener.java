package com.example.syneotek001.androidtabsexample25042018.interfaces;

public interface OnExpandableRecyclerViewChildItemClickListener {
    void onChildItemClicked(String str);

    void onParentItemClicked(String str);
}
