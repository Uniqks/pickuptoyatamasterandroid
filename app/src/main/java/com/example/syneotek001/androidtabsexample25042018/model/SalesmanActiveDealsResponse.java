package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 9/8/18.
 */

public class SalesmanActiveDealsResponse implements Serializable {
    public ArrayList<SalesmanActiveDealsData> activeDeals;

    public ArrayList<SalesmanActiveDealsData> getActiveDeals() {
        return activeDeals;
    }

    public void setActiveDeals(ArrayList<SalesmanActiveDealsData> activeDeals) {
        this.activeDeals = activeDeals;
    }

    public class SalesmanActiveDealsData implements Serializable {

        String deal_id;
        String customer_id;
        String firstname;
        String lastname;

        public String getDeal_id() {
            return deal_id;
        }

        public void setDeal_id(String deal_id) {
            this.deal_id = deal_id;
        }

        public String getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(String customer_id) {
            this.customer_id = customer_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }
    }
}
