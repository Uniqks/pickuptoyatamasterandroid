package com.example.syneotek001.androidtabsexample25042018.rest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.LogInActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.model.ChatListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageResponse;
import com.example.syneotek001.androidtabsexample25042018.model.DealInfoDetailResponse;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestDetailResponse;
import com.example.syneotek001.androidtabsexample25042018.model.LoginResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanActiveDealsResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanSoldDealsResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SendMessageResponse;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiManager {
    private Context mContext;
//    private ProgressDialog dialog;
    KProgressHUD hud;
    private ApiResponseInterface mApiResponseInterface;
    Handler handler;

    public ApiManager(Context context, ApiResponseInterface apiResponseInterface) {
        this.mContext = context;
        this.mApiResponseInterface = apiResponseInterface;
//        dialog = new ProgressDialog(mContext);
        hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        handler = new Handler(context.getMainLooper());
    }


    public void makeLoginRequest(String email, String password) {

        showDialog(mContext.getString(R.string.please_wait));
        Log.e("makeLoginRequest", "email " + email + " password " + password);
        Call<LoginResponse> call = Utils.getAPIInstance().loginUser(email, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                closeDialog();
                Log.e("makeLoginRequest", "code " + response.code() + " isSuccessful " + response.isSuccessful());
                if (!response.body().getData().getUser_id().equals("0")) {
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.LOGIN_REQUEST);
                } else {
                    mApiResponseInterface.isError(response.body().getMsg());
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                closeDialog();
                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getChatList(int fromlimit, String action,String user_id) {
        if (fromlimit==0) {
            showDialog(mContext.getString(R.string.please_wait));
        }
        Log.e("makeServiceRequest"," fromlimit "+fromlimit+" user_id "+user_id+" action "+action);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ChatListResponse> call = Utils.getAPIInstance().getChatList(action, user_id,String.valueOf(fromlimit));
        call.enqueue(new Callback<ChatListResponse>() {
            @Override
            public void onResponse(Call<ChatListResponse> call, Response<ChatListResponse> response) {
                closeDialog();
                if (response.body().getData()!=null && response.body().getData().size()>0) {
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_CHAT_LIST_REQUEST);
                } else {

                    mApiResponseInterface.isError(mContext.getString(R.string.err_chat_list));
                }

            }

            @Override
            public void onFailure(Call<ChatListResponse> call, Throwable t) {
                closeDialog();

                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getSalemanDealRequestList(int fromlimit, String action,String salesman_id) {
        if (fromlimit==1) {
            showDialog(mContext.getString(R.string.please_wait));
        }
        Log.e("makeServiceRequest"," fromlimit "+fromlimit+" action "+action+ " salesman_id "+salesman_id);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SalesmanDealRequestListResponse> call = Utils.getAPIInstance().getSalesmanDealRequestList(action,salesman_id,String.valueOf(fromlimit));
        call.enqueue(new Callback<SalesmanDealRequestListResponse>() {
            @Override
            public void onResponse(Call<SalesmanDealRequestListResponse> call, Response<SalesmanDealRequestListResponse> response) {
                closeDialog();
                if (response.body().getDealRequests()!=null && response.body().getDealRequests().size()>0) {
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_SALESMAN_DEAL_REQUESTS_LIST_REQUEST);
                } else {

                    mApiResponseInterface.isError(mContext.getString(R.string.err_salesman_deal_request_list));
                }

            }

            @Override
            public void onFailure(Call<SalesmanDealRequestListResponse> call, Throwable t) {
                closeDialog();

                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getDealInfoDetail(String action,String deal_id) {

        showDialog(mContext.getString(R.string.please_wait));

        Log.e("getDealInfoDetail"," deal_id "+deal_id+" action "+action);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<DealInfoDetailResponse> call = Utils.getAPIInstance().getDealInfoDetail(action,deal_id);
        call.enqueue(new Callback<DealInfoDetailResponse>() {
            @Override
            public void onResponse(Call<DealInfoDetailResponse> call, Response<DealInfoDetailResponse> response) {
                Log.e("getDealInfoDetail"," code "+response.code()+" isSuccessful "+response.isSuccessful());
                Log.e("getDealInfoDetail"," Msg "+response.body().getMsg());
                Log.e("getDealInfoDetail"," getData "+response.body().getData());
                closeDialog();
                if (response.body().getData()!=null) {
                    Log.e("getDealInfoDetail"," getData "+response.body().getData());
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_DEAL_INFO_DETAIL_REQUEST);
                } else {
                    mApiResponseInterface.isError(mContext.getString(R.string.err_deal_info_detail));
                }

            }

            @Override
            public void onFailure(Call<DealInfoDetailResponse> call, Throwable t) {
                closeDialog();
                Log.e("getSalemanSoldDeals"," err msg "+t.getMessage());
                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getDealRequestDetail(String action,String dr_id) {

            showDialog(mContext.getString(R.string.please_wait));

        Log.e("getSalemanSoldDeals"," dr_id "+dr_id+" action "+action);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<DealRequestDetailResponse> call = Utils.getAPIInstance().getDealRequestDetail(action,dr_id);
        call.enqueue(new Callback<DealRequestDetailResponse>() {
            @Override
            public void onResponse(Call<DealRequestDetailResponse> call, Response<DealRequestDetailResponse> response) {
                Log.e("getSalemanSoldDeals"," code "+response.code()+" isSuccessful "+response.isSuccessful());
                Log.e("getSalemanSoldDeals"," getSoldDeals().size() "+response.body().getDeal_request_detail());
                closeDialog();
                if (response.body().getDeal_request_detail()!=null) {
                    Log.e("getSalemanSoldDeals"," getSoldDeals().size() "+response.body().getDeal_request_detail());
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_DEAL_REQUEST_DETAIL_REQUEST);
                } else {
                    mApiResponseInterface.isError(mContext.getString(R.string.err_salesman_deal_request_list));
                }

            }

            @Override
            public void onFailure(Call<DealRequestDetailResponse> call, Throwable t) {
                closeDialog();
                Log.e("getSalemanSoldDeals"," err msg "+t.getMessage());
                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getSalemanSoldDeals(int fromlimit, String action,String salesman_id) {
        if (fromlimit==0) {
            showDialog(mContext.getString(R.string.please_wait));
        }
        Log.e("getSalemanSoldDeals"," fromlimit "+fromlimit+" action "+action+ " salesman_id "+salesman_id);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SalesmanSoldDealsResponse> call = Utils.getAPIInstance().getSalesmanSoldDeals(action,salesman_id,String.valueOf(fromlimit));
        call.enqueue(new Callback<SalesmanSoldDealsResponse>() {
            @Override
            public void onResponse(Call<SalesmanSoldDealsResponse> call, Response<SalesmanSoldDealsResponse> response) {
                Log.e("getSalemanSoldDeals"," code "+response.code()+" isSuccessful "+response.isSuccessful());
                Log.e("getSalemanSoldDeals"," getSoldDeals().size() "+response.body().getSoldDeals().size());
                closeDialog();
                if (response.body().getSoldDeals()!=null && response.body().getSoldDeals().size()>0) {
                    Log.e("getSalemanSoldDeals"," getSoldDeals().size() "+response.body().getSoldDeals().size());
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_SALESMAN_SOLD_DEALS_REQUEST);
                } else {
                    mApiResponseInterface.isError(mContext.getString(R.string.err_salesman_deal_request_list));
                }

            }

            @Override
            public void onFailure(Call<SalesmanSoldDealsResponse> call, Throwable t) {
                closeDialog();
                Log.e("getSalemanSoldDeals"," err msg "+t.getMessage());
                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getSalemanActiveDeals(int fromlimit, String action,String salesman_id) {
        if (fromlimit==0) {
            showDialog(mContext.getString(R.string.please_wait));
        }
        Log.e("makeServiceRequest"," fromlimit "+fromlimit+" action "+action+ " salesman_id "+salesman_id);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SalesmanActiveDealsResponse> call = Utils.getAPIInstance().getSalesmanActiveDeals(action,salesman_id,String.valueOf(fromlimit));
        call.enqueue(new Callback<SalesmanActiveDealsResponse>() {
            @Override
            public void onResponse(Call<SalesmanActiveDealsResponse> call, Response<SalesmanActiveDealsResponse> response) {
                closeDialog();
                if (response.body().getActiveDeals()!=null && response.body().getActiveDeals().size()>0) {
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_SALESMAN_ACTIVE_DEALS_REQUEST);
                } else {

                    mApiResponseInterface.isError(mContext.getString(R.string.err_salesman_deal_request_list));
                }

            }

            @Override
            public void onFailure(Call<SalesmanActiveDealsResponse> call, Throwable t) {
                closeDialog();

                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }


    public void getSalemanList(int fromlimit, String action) {
        if (fromlimit==1) {
            showDialog(mContext.getString(R.string.please_wait));
        }
        Log.e("makeServiceRequest"," fromlimit "+fromlimit+" action "+action);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SalesmanListResponse> call = Utils.getAPIInstance().getSalesmanList(action,String.valueOf(fromlimit));
        call.enqueue(new Callback<SalesmanListResponse>() {
            @Override
            public void onResponse(Call<SalesmanListResponse> call, Response<SalesmanListResponse> response) {
                closeDialog();
                if (response.body().getSalesmanList()!=null && response.body().getSalesmanList().size()>0) {
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_SALESMAN_LIST_REQUEST);
                } else {

                    mApiResponseInterface.isError(mContext.getString(R.string.err_salesman_list));
                }

            }

            @Override
            public void onFailure(Call<SalesmanListResponse> call, Throwable t) {
                closeDialog();

                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getChatMessages(int fromlimit, String action,String user_id,String friend_id) {
        if (fromlimit==0) {
            showDialog(mContext.getString(R.string.please_wait));
        }
        Log.e("makeServiceRequest"," fromlimit "+fromlimit+" user_id "+user_id+" action "+action+" friend_id "+friend_id);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ChatMessageResponse> call = Utils.getAPIInstance().getChatMessages(action, user_id,friend_id,String.valueOf(fromlimit));
        call.enqueue(new Callback<ChatMessageResponse>() {
            @Override
            public void onResponse(Call<ChatMessageResponse> call, Response<ChatMessageResponse> response) {
                closeDialog();
                if (response.body().getData()!=null && response.body().getData().size()>0) {
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.GET_CHAT_MESSAGES_LIST_REQUEST);
                } else {

                    mApiResponseInterface.isError(mContext.getString(R.string.err_chat_list));
                }

            }

            @Override
            public void onFailure(Call<ChatMessageResponse> call, Throwable t) {
                closeDialog();

                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }


    public void sendChatMessages(String action,String message,String user_id,String friend_id,String msg_type) {
        Log.e("makeServiceRequest"," message "+message+" user_id "+user_id+" action "+action+" friend_id "+friend_id+" msg_type "+msg_type);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SendMessageResponse> call = Utils.getAPIInstance().sendChatMessages(action,message, user_id,friend_id,msg_type);
        call.enqueue(new Callback<SendMessageResponse>() {
            @Override
            public void onResponse(Call<SendMessageResponse> call, Response<SendMessageResponse> response) {
                closeDialog();
                if (response.body().getData()!=null && response.body().getData().getMsg()!=null) {
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.SEND_TEXT_MESSAGE_REQUEST);
                } else {

                    mApiResponseInterface.isError(mContext.getString(R.string.err_chat_list));
                }

            }

            @Override
            public void onFailure(Call<SendMessageResponse> call, Throwable t) {
                closeDialog();
                Log.e("ApiManager"," sendChatMessages onFailure "+t.getMessage());
                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void uploadImageChat(String action,String image,String user_id,String friend_id,String msg_type) {
        Log.e("makeServiceRequest"," user_id "+user_id+" action "+action+" friend_id "+friend_id+" msg_type "+msg_type);
        Log.e("makeServiceRequest"," image "+image);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SendMessageResponse> call = Utils.getAPIInstance().uploadImageChat(action,image, user_id,friend_id,msg_type);
        call.enqueue(new Callback<SendMessageResponse>() {
            @Override
            public void onResponse(Call<SendMessageResponse> call, Response<SendMessageResponse> response) {
                closeDialog();
                Log.e("ApiManager"," uploadImageChat onResponse code "+response.code()+" isSuccessful "+response.isSuccessful());
                if (response.body().getData()!=null && response.body().getData().getMsg()!=null) {
                    Log.e("ApiManager"," uploadImageChat onResponse "+response.body().getData().getMsg());
                    mApiResponseInterface.isSuccess(response.body(), AppConstant.SEND_TEXT_MESSAGE_REQUEST);
                } else {
                    Log.e("ApiManager"," uploadImageChat onResponse "+response.body().getData().getMsg());
                    mApiResponseInterface.isError(mContext.getString(R.string.err_chat_list));
                }

            }

            @Override
            public void onFailure(Call<SendMessageResponse> call, Throwable t) {
                closeDialog();
                Log.e("ApiManager"," uploadImageChat onFailure "+t.getMessage());
                Toast.makeText(mContext, mContext.getString(R.string.err_try_again), Toast.LENGTH_LONG).show();
            }
        });
    }

//    sendChatMessages(String action,String message,String user_id,String friend_id,String msg_type)



    /**
     * The purpose of this method is to show the dialog
     *
     * @param message
     */
    private void showDialog(String message) {
//        dialog.setMessage(message);
//        dialog.show();
        hud.show();
    }

    /**
     * The purpose of this method is to close the dialog
     */
    private void closeDialog() {
        /*if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }*/
        if (hud != null && hud.isShowing()) {
            hud.dismiss();
        }
    }


}