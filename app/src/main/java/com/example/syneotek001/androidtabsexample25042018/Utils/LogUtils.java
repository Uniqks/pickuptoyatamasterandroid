package com.example.syneotek001.androidtabsexample25042018.Utils;

import android.util.Log;



/**
 * Created by bluegenie on 5/5/16.
 */
public class LogUtils {

    public static String DEFAULT_TAG = "Drag";

    public static final boolean SHOULD_PRINT_LOG = true;
    /**
     * Print info log
     * @param tag tag
     * @param message message
     */
    public static void i(String tag, String message) {
        if (SHOULD_PRINT_LOG) {
            Log.i(DEFAULT_TAG, message);
        }
    }

    /**
     * Print debug log
     * @param tag tag
     * @param message message
     */
    public static void d(String tag, String message) {
        if (SHOULD_PRINT_LOG) {
            Log.d(tag, message);
        }
    }

    /**
     * Print error log
     * @param tag tag
     * @param message message
     */
    public static void e(String tag, String message) {
        if (SHOULD_PRINT_LOG) {
            Log.e(tag, message);
        }
    }

    /**
     * Print info log
     * @param message message
     */
    public static void i(String message) {
        if (SHOULD_PRINT_LOG) {
            Log.i(DEFAULT_TAG, message);
        }
    }

    /**
     * Print debug log
     * @param message message
     */
    public static void d(String message) {
        if (SHOULD_PRINT_LOG) {
            Log.d(DEFAULT_TAG, message);
        }
    }

    /**
     * Print error log
     * @param message message
     */
    public static void e(String message) {
        if (SHOULD_PRINT_LOG) {
            Log.e(DEFAULT_TAG, message);
        }
    }
}
