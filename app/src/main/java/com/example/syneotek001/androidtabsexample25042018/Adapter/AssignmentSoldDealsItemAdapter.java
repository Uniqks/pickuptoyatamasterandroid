package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.AssignmentSoldDealsFragment.OnLoadMoreListener;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanSoldDealsResponse.SalesmanActiveDealsData;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.util.ArrayList;


public class AssignmentSoldDealsItemAdapter extends RecyclerView.Adapter<ViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<SalesmanActiveDealsData> mData;
    private OnLeadItemViewClickListener onItemClickListener;
    OnLoadMoreListener onLoadMoreListener;
    private boolean loading;
    private int visibleThreshold = AppConstant.LIST_API_VISIBLE_THRESHHOLD;
    private int lastVisibleItem, totalItemCount;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivInfo:
                if (AssignmentSoldDealsItemAdapter.this.onItemClickListener != null) {
                    AssignmentSoldDealsItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            /*case R.id.ivLeadLabel:
                if (SalesmanItemAdapter.this.onItemClickListener != null) {
                    SalesmanItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;*/
            case R.id.ivEmail:
                if (AssignmentSoldDealsItemAdapter.this.onItemClickListener != null) {
                    AssignmentSoldDealsItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivEdit:
                if (AssignmentSoldDealsItemAdapter.this.onItemClickListener != null) {
                    AssignmentSoldDealsItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public static class ProgressViewHolder extends ViewHolder {

        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivInfo;
        LinearLayout llActionItems;
        TextView tvCustomerId, tvDealRequest, tvName, tvDate;
        View viewDummySpace;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivInfo = view.findViewById(R.id.ivInfo);
            llActionItems = view.findViewById(R.id.llActionItems);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvDealRequest = view.findViewById(R.id.tvDealRequest);
            tvDate = view.findViewById(R.id.tvDate);
            tvName = view.findViewById(R.id.tvName);
            viewDummySpace = view.findViewById(R.id.viewDummySpace);


        }

        @Override
        public void onClick(View v) {

        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public AssignmentSoldDealsItemAdapter(Context context, ArrayList<SalesmanActiveDealsData> list, OnLeadItemViewClickListener onItemClickListener, RecyclerView recyclerView) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    Log.e("onLoadMoreListener"," "+onLoadMoreListener+" "+totalItemCount+" "+lastVisibleItem+" "+visibleThreshold+" "+loading);

                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        Log.e("onLoadMoreListener","if");
                        if (onLoadMoreListener != null) {
                            Log.e("onLoadMoreListener","if2");
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });

        }

    }

    public int getItemViewType(int position) {
        return mData.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_assignment_deal_request, parent, false);

        return new MyViewHolder(v);*/

        ViewHolder vh;

        if (viewType == VIEW_ITEM) {

            View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.item_assignment_active_deals, parent, false);

            vh = new MyViewHolder(v);


        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }


        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder instanceof MyViewHolder)
        {
            SalesmanActiveDealsData mLeadItem = (SalesmanActiveDealsData) mData.get(position);
            if (mLeadItem==null)
                return;
            ((MyViewHolder)holder).tvCustomerId.setText(mLeadItem.getCustomer_id());
//            ((MyViewHolder)holder).tvDealRequest.setText(mLeadItem.getDr_count()+" "+mContext.getString(R.string.deal_requests));
//        holder.tvDate.setText(mLeadItem.getDate());
            ((MyViewHolder)holder).tvName.setText(mLeadItem.getFirstname()+" "+mLeadItem.getLastname());

            holder.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (onItemClickListener != null) {
                    /*AssignmentDealRequestItemModel mLeadItem = (AssignmentDealRequestItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);*/
                        // open details page here
//                    DealsDescriptionFragment
                        if (AssignmentSoldDealsItemAdapter.this.onItemClickListener != null) {
                            AssignmentSoldDealsItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(position);
                            return;
                        }
                    }
                }
            });


            ((MyViewHolder)holder).ivInfo.setOnClickListener(this);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }



    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
