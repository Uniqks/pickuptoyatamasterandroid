package com.example.syneotek001.androidtabsexample25042018.model;

public class HelpCenterAdminItemModel {

    private String ticket_id = "";
    private String message = "";
    private String datetime = "";
    private String action = "";

    public HelpCenterAdminItemModel(String ticket_id,String message, String datetime, String action) {
        this.ticket_id = ticket_id;
        this.message = message;
        this.datetime = datetime;
        this.action = action;
    }

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
