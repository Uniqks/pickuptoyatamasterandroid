package com.example.syneotek001.androidtabsexample25042018.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 9/8/18.
 */

public class DealInfoDetailResponse implements Serializable {

    DealInfoDetailData data;

    String msg;

    public DealInfoDetailData getData() {
        return data;
    }

    public void setData(DealInfoDetailData data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class DealInfoDetailData implements Serializable {

        String task;
        String position;
        String label;
        String lastactivitydate;
        String deal_number;
        String customer_number;
        String salesman;
        @SerializedName("package")
        String str_package;
        String ext_accessories;
        String int_accessories;
        String down_payment;
        String trade_in_type;
        String trade_in_value;
        String trade_in_owing;
        String inc_tax;
        String inc_offers;
        String rate;
        String tenure;
        String emi;
        String frequency;
        String annualkm;
        String vehicle_price;
        String cash;
        String is_lease_finance;
        String vin_number;
        String inv_type;
        String make;
        String model;
        String sub_model;
        String body_style;
        String year;
        String exterior_color;
        String interior_color;
        String fuel_type;
        String drive_type;
        String transmission;
        String doors;
        String condition;
        String category;
        String engine;

        public String getTask() {
            return task;
        }

        public void setTask(String task) {
            this.task = task;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getLastactivitydate() {
            return lastactivitydate;
        }

        public void setLastactivitydate(String lastactivitydate) {
            this.lastactivitydate = lastactivitydate;
        }

        public String getDeal_number() {
            return deal_number;
        }

        public void setDeal_number(String deal_number) {
            this.deal_number = deal_number;
        }

        public String getCustomer_number() {
            return customer_number;
        }

        public void setCustomer_number(String customer_number) {
            this.customer_number = customer_number;
        }

        public String getSalesman() {
            return salesman;
        }

        public void setSalesman(String salesman) {
            this.salesman = salesman;
        }

        public String getStr_package() {
            return str_package;
        }

        public void setStr_package(String str_package) {
            this.str_package = str_package;
        }

        public String getExt_accessories() {
            return ext_accessories;
        }

        public void setExt_accessories(String ext_accessories) {
            this.ext_accessories = ext_accessories;
        }

        public String getInt_accessories() {
            return int_accessories;
        }

        public void setInt_accessories(String int_accessories) {
            this.int_accessories = int_accessories;
        }

        public String getDown_payment() {
            return down_payment;
        }

        public void setDown_payment(String down_payment) {
            this.down_payment = down_payment;
        }

        public String getTrade_in_type() {
            return trade_in_type;
        }

        public void setTrade_in_type(String trade_in_type) {
            this.trade_in_type = trade_in_type;
        }

        public String getTrade_in_value() {
            return trade_in_value;
        }

        public void setTrade_in_value(String trade_in_value) {
            this.trade_in_value = trade_in_value;
        }

        public String getTrade_in_owing() {
            return trade_in_owing;
        }

        public void setTrade_in_owing(String trade_in_owing) {
            this.trade_in_owing = trade_in_owing;
        }

        public String getInc_tax() {
            return inc_tax;
        }

        public void setInc_tax(String inc_tax) {
            this.inc_tax = inc_tax;
        }

        public String getInc_offers() {
            return inc_offers;
        }

        public void setInc_offers(String inc_offers) {
            this.inc_offers = inc_offers;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getTenure() {
            return tenure;
        }

        public void setTenure(String tenure) {
            this.tenure = tenure;
        }

        public String getEmi() {
            return emi;
        }

        public void setEmi(String emi) {
            this.emi = emi;
        }

        public String getFrequency() {
            return frequency;
        }

        public void setFrequency(String frequency) {
            this.frequency = frequency;
        }

        public String getAnnualkm() {
            return annualkm;
        }

        public void setAnnualkm(String annualkm) {
            this.annualkm = annualkm;
        }

        public String getVehicle_price() {
            return vehicle_price;
        }

        public void setVehicle_price(String vehicle_price) {
            this.vehicle_price = vehicle_price;
        }

        public String getCash() {
            return cash;
        }

        public void setCash(String cash) {
            this.cash = cash;
        }

        public String getIs_lease_finance() {
            return is_lease_finance;
        }

        public void setIs_lease_finance(String is_lease_finance) {
            this.is_lease_finance = is_lease_finance;
        }

        public String getVin_number() {
            return vin_number;
        }

        public void setVin_number(String vin_number) {
            this.vin_number = vin_number;
        }

        public String getInv_type() {
            return inv_type;
        }

        public void setInv_type(String inv_type) {
            this.inv_type = inv_type;
        }

        public String getMake() {
            return make;
        }

        public void setMake(String make) {
            this.make = make;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getSub_model() {
            return sub_model;
        }

        public void setSub_model(String sub_model) {
            this.sub_model = sub_model;
        }

        public String getBody_style() {
            return body_style;
        }

        public void setBody_style(String body_style) {
            this.body_style = body_style;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getExterior_color() {
            return exterior_color;
        }

        public void setExterior_color(String exterior_color) {
            this.exterior_color = exterior_color;
        }

        public String getInterior_color() {
            return interior_color;
        }

        public void setInterior_color(String interior_color) {
            this.interior_color = interior_color;
        }

        public String getFuel_type() {
            return fuel_type;
        }

        public void setFuel_type(String fuel_type) {
            this.fuel_type = fuel_type;
        }

        public String getDrive_type() {
            return drive_type;
        }

        public void setDrive_type(String drive_type) {
            this.drive_type = drive_type;
        }

        public String getTransmission() {
            return transmission;
        }

        public void setTransmission(String transmission) {
            this.transmission = transmission;
        }

        public String getDoors() {
            return doors;
        }

        public void setDoors(String doors) {
            this.doors = doors;
        }

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getEngine() {
            return engine;
        }

        public void setEngine(String engine) {
            this.engine = engine;
        }
    }


}
