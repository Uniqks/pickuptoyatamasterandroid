package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.syneotek001.androidtabsexample25042018.Adapter.LeadsStatusSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.ScheduleSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;
import java.util.Arrays;


public class LeadsFilterFragment extends Fragment implements OnBackPressed {
    public ImageView ivMenu,ivLabelAll,ivLabelClosed,ivLabelCold,ivLabelDead,ivLabelHot,ivLabelWarm;
    Spinner spinnerSelectOwner;
    ArrayList<String> arr_schedule_categories = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view=inflater.inflate(R.layout.fragment_filter_leads, container, false);
        ivLabelAll=view.findViewById(R.id.ivLabelAll);
        ivLabelClosed=view.findViewById(R.id.ivLabelClosed);
        ivLabelCold=view.findViewById(R.id.ivLabelCold);
        ivLabelDead=view.findViewById(R.id.ivLabelDead);
        ivLabelHot=view.findViewById(R.id.ivLabelHot);
        ivLabelWarm=view.findViewById(R.id.ivLabelWarm);
        ivMenu=view.findViewById(R.id.ivMenu);

        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        spinnerSelectOwner = view.findViewById(R.id.spinnerSelectOwner);
        arr_schedule_categories = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.arr_salesman_status)));
        initCustomSpinner(arr_schedule_categories);
        spinnerSelectOwner.setSelection(1);

        return view;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        LeadsStatusSpinnerAdapter customSpinnerAdapter = new LeadsStatusSpinnerAdapter(getActivity(), countries);
        spinnerSelectOwner.setAdapter(customSpinnerAdapter);
        spinnerSelectOwner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

  /*  @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (this.view == null) {
            this.mBinding = (FragmentFilterLeadBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_filter_lead, container, false);
            this.view = this.mBinding.getRoot();
            setupToolBarWithBackArrow(this.mBinding.toolbar.toolbar, this.mContext.getResources().getString(R.string.filter));
        }
        setHasOptionsMenu(true);
        return this.view;
    }*/

}
