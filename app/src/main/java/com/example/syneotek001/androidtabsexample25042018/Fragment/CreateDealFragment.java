package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;


public class CreateDealFragment extends Fragment implements OnClickListener {
    String[] carType;
    String[] tilBodyStyle;
    String[] tilCategory;
    String[] tilCondition;
    String[] tilDoors;
    String[] tilDriveType;
    String[] tilEngine;
    String[] tilExteriorColor;
    String[] tilFuelType;
    String[] tilInteriorColor;
    String[] tilMake;
    String[] tilModel;
    String[] tilSubModel;
    String[] tilTransmission;
    String[] tilVinNumber;
    String[] tilYear;
    String[] tradeInType;


    public  CardView cvSection1,cvSection2,cvSection3;
    public  EditText etBodyStyle,etCarType,etCategory,etCondition,etDirveType,etDoors,etDownPayment,etEngine,etExteriorColor,etVinNumber;

    public  EditText etFuelType,etInteriorColor,etMake,etModel,etSubModel,etTradeInOwing,etTradeInType,etTradeInValue,etTransmission,etYear;
    public LinearLayout llSection1,llSection2,llSection3;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view =inflater.inflate(R.layout.fragment_create_deal, container, false);


//            Make Deal

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.str_make_deal));

        cvSection1=view.findViewById(R.id.cvSection1);
        cvSection2=view.findViewById(R.id.cvSection2);
        cvSection3=view.findViewById(R.id.cvSection3);
        etBodyStyle=view.findViewById(R.id.etBodyStyle);
        etCarType=view.findViewById(R.id.etCarType);
        etCategory=view.findViewById(R.id.etCategory);
        etCondition=view.findViewById(R.id.etCondition);
        etDirveType=view.findViewById(R.id.etDirveType);
        etDoors=view.findViewById(R.id.etDoors);
        etDownPayment=view.findViewById(R.id.etDownPayment);
        etEngine=view.findViewById(R.id.etEngine);
        etExteriorColor=view.findViewById(R.id.etExteriorColor);
        etVinNumber=view.findViewById(R.id.etVinNumber);


        etFuelType=view.findViewById(R.id.etFuelType);
        etInteriorColor=view.findViewById(R.id.etInteriorColor);
        etMake=view.findViewById(R.id.etMake);
        etModel=view.findViewById(R.id.etModel);
        etSubModel=view.findViewById(R.id.etSubModel);
        etTradeInOwing=view.findViewById(R.id.etTradeInOwing);
        etTradeInType=view.findViewById(R.id.etTradeInType);
        etTradeInValue=view.findViewById(R.id.etTradeInValue);
        etTransmission=view.findViewById(R.id.etTransmission);
        etYear=view.findViewById(R.id.etYear);

        llSection1=view.findViewById(R.id.llSection1);
        llSection2=view.findViewById(R.id.llSection2);
        llSection3=view.findViewById(R.id.llSection3);
        ImageView ivBack=view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

//            this.mBinding = (FragmentCreateDealBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_create_deal, container, false);
//            this.view = this.mBinding.getRoot();
//            setupToolBarWithBackArrow(this.mBinding.toolbar.toolbar, getResources().getString(R.string.create_deal));
       
        setHasOptionsMenu(true);
        setClickEvent();
        return view;
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tradeInType = new String[]{getResources().getString(R.string.owned), getResources().getString(R.string.lease)};
        carType = new String[]{getResources().getString(R.string.type_used), getResources().getString(R.string.type_new)};
        tilMake = new String[]{getResources().getString(R.string.make_toyota), getResources().getString(R.string.make_ford), getResources().getString(R.string.make_hyundai), getResources().getString(R.string.make_jeep), getResources().getString(R.string.make_kia), getResources().getString(R.string.make_lexus), getResources().getString(R.string.make_mercedes), getResources().getString(R.string.make_honda), getResources().getString(R.string.make_scion), getResources().getString(R.string.make_chevrolet), getResources().getString(R.string.make_nishan), getResources().getString(R.string.make_subaru), getResources().getString(R.string.make_volkswagen), getResources().getString(R.string.make_mazda), getResources().getString(R.string.make_dodge), getResources().getString(R.string.make_ram)};
        tilModel = new String[]{getResources().getString(R.string.model_4runner), getResources().getString(R.string.model_avalon), getResources().getString(R.string.model_chr), getResources().getString(R.string.model_chr), getResources().getString(R.string.model_camry), getResources().getString(R.string.model_corolla), getResources().getString(R.string.model_highlander), getResources().getString(R.string.model_matrix), getResources().getString(R.string.model_prius), getResources().getString(R.string.model_rav4), getResources().getString(R.string.model_seiana), getResources().getString(R.string.model_tacoma), getResources().getString(R.string.model_tundra), getResources().getString(R.string.model_venza), getResources().getString(R.string.model_yaris), getResources().getString(R.string.model_sequoia), getResources().getString(R.string.model_im), getResources().getString(R.string.model_frs), getResources().getString(R.string.model_xle_venza), getResources().getString(R.string.model_celica), getResources().getString(R.string.model_priusv)};
        tilSubModel = new String[]{getResources().getString(R.string.sub_model_1), getResources().getString(R.string.sub_model_2), getResources().getString(R.string.sub_model_3), getResources().getString(R.string.sub_model_4), getResources().getString(R.string.sub_model_5), getResources().getString(R.string.sub_model_6), getResources().getString(R.string.sub_model_7), getResources().getString(R.string.sub_model_8), getResources().getString(R.string.sub_model_9), getResources().getString(R.string.sub_model_10), getResources().getString(R.string.sub_model_11)};
        tilBodyStyle = new String[]{getResources().getString(R.string.body_style_minivan_van), getResources().getString(R.string.body_style_suv_crossover), getResources().getString(R.string.body_style_hatchback), getResources().getString(R.string.body_style_sedun), getResources().getString(R.string.body_style_truck), getResources().getString(R.string.two_body_coupe), getResources().getString(R.string.body_suv), getResources().getString(R.string.body_coupe), getResources().getString(R.string.crew_cab), getResources().getString(R.string.truck_crew_cab), getResources().getString(R.string.truck_double_cab)};
        tilExteriorColor = new String[]{getResources().getString(R.string.exterior_color_alpine_white), getResources().getString(R.string.body_style_suv_crossover), getResources().getString(R.string.exterior_color_blue), getResources().getString(R.string.exterior_color_magnetic_grey)};
        tilInteriorColor = new String[]{getResources().getString(R.string.interior_color_black), getResources().getString(R.string.interior_color_dark_grey), getResources().getString(R.string.interior_color_light_grey), getResources().getString(R.string.exterior_color_magnetic_grey)};
        tilFuelType = new String[]{getResources().getString(R.string.fuel_type_gas), getResources().getString(R.string.fuel_type_gasoline), getResources().getString(R.string.fuel_type_gasoline_hybrid)};
        tilDriveType = new String[]{getResources().getString(R.string.drive_type_all_wheel_drive), getResources().getString(R.string.drive_type_front_wheel_drive), getResources().getString(R.string.drive_type_rear_wheel_drive)};
        tilTransmission = new String[]{getResources().getString(R.string.transmission_automatic), getResources().getString(R.string.transmission_manual), getResources().getString(R.string.transmission_variable_cvt)};
        tilDoors = new String[]{getResources().getString(R.string.doors_2_doors), getResources().getString(R.string.doors_4_doors)};
        tilCondition = new String[]{getResources().getString(R.string.condition_good), getResources().getString(R.string.condition_excellent_condition)};
        tilCategory = new String[]{getResources().getString(R.string.category_minivan_van), getResources().getString(R.string.category_sedan)};
        tilEngine = new String[]{getResources().getString(R.string.engine_4_cylinder), getResources().getString(R.string.engine_v_8_cyl)};
        tilVinNumber = new String[]{getResources().getString(R.string.vin_hash_1), getResources().getString(R.string.vin_hash_2), getResources().getString(R.string.vin_hash_3)};
        tilYear = new String[]{getResources().getString(R.string.year_2016), getResources().getString(R.string.year_2017), getResources().getString(R.string.year_2018)};
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_deal, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_create_deal:
//                ((MainActivity) this.mActivity).pushFragment(((MainActivity) this.mContext).mCurrentTab, new DealBuilderCarListHolderFragment(), true, true);
                return true;
            default:
                return false;
        }
    }

    private void setClickEvent() {
       etTradeInType.setOnClickListener(this);
       etCarType.setOnClickListener(this);
       etVinNumber.setOnClickListener(this);
        etEngine.setOnClickListener(this);
      etMake.setOnClickListener(this);
        etModel.setOnClickListener(this);
       etSubModel.setOnClickListener(this);
       etBodyStyle.setOnClickListener(this);
       etYear.setOnClickListener(this);
        etInteriorColor.setOnClickListener(this);
       etExteriorColor.setOnClickListener(this);
       etFuelType.setOnClickListener(this);
       etDirveType.setOnClickListener(this);
       etTransmission.setOnClickListener(this);
     etDoors.setOnClickListener(this);
       etCondition.setOnClickListener(this);
       etCategory.setOnClickListener(this);
    }

    private void displayDialog(String title, final String[] items, final EditText etView) {
        new Builder(getActivity()).setTitle((CharSequence) title).setSingleChoiceItems((CharSequence[]) items, 0, null).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                etView.setText(items[((AlertDialog) dialog).getListView().getCheckedItemPosition()]);
            }
        }).show();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.etBodyStyle:
                displayDialog(getResources().getString(R.string.body_style), this.tilBodyStyle,etBodyStyle);
                return;
            case R.id.etCarType:
                displayDialog(getResources().getString(R.string.select_car_type), this.carType, etCarType);
                return;
            case R.id.etCategory:
                displayDialog(getResources().getString(R.string.category), this.tilCategory, etCategory);
                return;
            case R.id.etCondition:
                displayDialog(getResources().getString(R.string.condition), this.tilCondition, etCondition);
                return;
            case R.id.etDirveType:
                displayDialog(getResources().getString(R.string.drive_type), this.tilDriveType, etDirveType);
                return;
            case R.id.etDoors:
                displayDialog(getResources().getString(R.string.doors), this.tilDoors,etDoors);
                return;
            case R.id.etEngine:
                displayDialog(getResources().getString(R.string.engine), this.tilEngine,etEngine);
                return;
            case R.id.etExteriorColor:
                displayDialog(getResources().getString(R.string.exterior_color), this.tilExteriorColor, etExteriorColor);
                return;
            case R.id.etFuelType:
                displayDialog(getResources().getString(R.string.fuel_type), this.tilFuelType,etFuelType);
                return;
            case R.id.etInteriorColor:
                displayDialog(getResources().getString(R.string.interior_color), this.tilInteriorColor, etInteriorColor);
                return;
            case R.id.etMake:
                displayDialog(getResources().getString(R.string.make), this.tilMake,etMake);
                return;
            case R.id.etModel:
                displayDialog(getResources().getString(R.string.model), this.tilModel, etModel);
                return;
            case R.id.etSubModel:
                displayDialog(getResources().getString(R.string.sub_model), this.tilModel, etSubModel);
                return;
            case R.id.etTradeInType:
                displayDialog(getResources().getString(R.string.select_trade_in_type), this.tradeInType, etTradeInType);
                return;
            case R.id.etTransmission:
                displayDialog(getResources().getString(R.string.transmission), this.tilTransmission, etTransmission);
                return;
            case R.id.etVinNumber:
                displayDialog(getResources().getString(R.string.vin_hash), this.tilVinNumber, etVinNumber);
                return;
            case R.id.etYear:
                displayDialog(getResources().getString(R.string.year), this.tilYear, etYear);
                return;
            default:
                return;
        }
    }
}
