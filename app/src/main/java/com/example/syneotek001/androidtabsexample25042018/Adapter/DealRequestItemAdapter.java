package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.CreateDealFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.DealRequestModel;

import java.util.ArrayList;
import java.util.List;

public class DealRequestItemAdapter extends RecyclerView.Adapter<DealRequestItemAdapter.MyViewHolder> {
    private Context mContext;
    private List<DealRequestModel> mData = new ArrayList();

    class MyViewHolder extends ViewHolder {
        public TextView tvCustomerId, tvInquiry, tvMakeDeal, tvName;

        MyViewHolder(View view) {
            super(view);
            tvCustomerId = view.findViewById(R.id.tvCustomerId);
            tvInquiry = view.findViewById(R.id.tvInquiry);
            tvMakeDeal = view.findViewById(R.id.tvMakeDeal);
            tvName = view.findViewById(R.id.tvName);
        }
    }

    public DealRequestItemAdapter(Context context, ArrayList<DealRequestModel> list) {
        this.mContext = context;
        this.mData = list;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public DealRequestItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_deal_request, parent, false);

        return new DealRequestItemAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DealRequestModel mDealRequestModel = (DealRequestModel) mData.get(position);
        holder.tvName.setText(mDealRequestModel.getSalesManName());
        holder.tvCustomerId.setText(mDealRequestModel.getCustomerId());

        holder.tvMakeDeal.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ((HomeActivity)mContext).replaceFragment(new CreateDealFragment());
//                ((MainActivity) DealRequestItemAdapter.this.mContext).pushFragment(((MainActivity) DealRequestItemAdapter.this.mContext).mCurrentTab, new CreateDealFragment(), true, true);
            }
        });
    }

    public int getItemCount() {
        return mData.size();
    }
}
