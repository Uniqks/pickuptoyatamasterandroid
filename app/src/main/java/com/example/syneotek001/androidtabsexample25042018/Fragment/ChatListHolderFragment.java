package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.content.res.AppCompatResources;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.CustomListAdapter;
import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.ConatctModel;


public class ChatListHolderFragment extends Fragment {
    FragmentTabsPagerAdapter mAdapter;
    public EditText etSearchContacts;
    public TabLayout tabLayout;
    public ViewPager viewPager;
    LinearLayout ll_SearchContacts;
    ImageView ivMenu,ivsearch;
    Context mcontext;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mview=  inflater.inflate(R.layout.fragment_chat_tab_list_holder, container, false);
        mcontext=getActivity();
        etSearchContacts=mview.findViewById(R.id.etSearchContacts);
        ll_SearchContacts=mview.findViewById(R.id.ll_SearchContacts);
        ivsearch=mview.findViewById(R.id.ivsearch);
        ivMenu=mview.findViewById(R.id.ivMenu);

        tabLayout=mview.findViewById(R.id.tabLayout);
        viewPager=mview.findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        setHasOptionsMenu(true);

        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null) {
                    ((HomeActivity)getActivity()).opendrawer();
                }
            }
        });
        TextView tvTitle = mview.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.live_chat));

        ivsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_SearchContacts.getVisibility() == View.VISIBLE) {
                    ll_SearchContacts.setVisibility(View.GONE);
                    Drawable search_white = AppCompatResources.getDrawable(mcontext, R.drawable.ic_vector_search_white);
                    ivsearch.setImageDrawable(search_white);
                } else {
                    ll_SearchContacts.setVisibility(View.VISIBLE);
                    Drawable filter_closed_white = AppCompatResources.getDrawable(mcontext, R.drawable.ic_vector_filter_closed_white);
                    ivsearch.setImageDrawable(filter_closed_white);
                }
            }
        });
        return  mview;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewPager);
    }

    /* @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (this.view == null) {
            this.mBinding = (FragmentChatTabListHolderBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_chat_tab_list_holder, container, false);
            this.view = this.mBinding.getRoot();
            setupToolBarWithMenu(this.mBinding.toolbar.toolbar, this.mContext.getResources().getString(R.string.live_chat));
           
        }
      
        return this.view;
    }*/

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chat_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter_contacts:
                if (ll_SearchContacts.getVisibility() == View.VISIBLE) {
                    ll_SearchContacts.setVisibility(View.GONE);
                    item.setIcon(R.drawable.ic_vector_search_white);
                } else {
                    ll_SearchContacts.setVisibility(View.VISIBLE);
                    item.setIcon(R.drawable.ic_vector_filter_closed_white);
                }
                return true;
            default:
                return false;
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
        ChatListFragment chatListFragment = new ChatListFragment();
        mAdapter.addFragment(chatListFragment, getResources().getString(R.string.chats));
        ChatContactListFragment chatContactListFragment = new ChatContactListFragment();
        mAdapter.addFragment(chatContactListFragment, getResources().getString(R.string.contacts_count, new Object[]{String.valueOf(5)}));
        viewPager.setAdapter(mAdapter);
        tabLayout.setupWithViewPager(viewPager);
       tabLayout.setTabMode(1);
    }
}
