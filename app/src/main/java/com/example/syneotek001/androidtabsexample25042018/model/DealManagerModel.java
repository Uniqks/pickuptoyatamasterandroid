package com.example.syneotek001.androidtabsexample25042018.model;

public class DealManagerModel {
    private String customerId = "";
    private long dateTime;
    private int dealRequestCount = 0;
    private String id = "";
    private String salesmanName = "";

    public DealManagerModel(String id, String customerId, String salesmanName, long dateTime, int dealRequestCount) {
        this.id = id;
        this.customerId = customerId;
        this.salesmanName = salesmanName;
        this.dateTime = dateTime;
        this.dealRequestCount = dealRequestCount;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSalesmanName() {
        return this.salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public long getDateTime() {
        return this.dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public int getDealRequestCount() {
        return this.dealRequestCount;
    }

    public void setDealRequestCount(int dealRequestCount) {
        this.dealRequestCount = dealRequestCount;
    }

    public String toString() {
        return "DealManagerModel{id='" + this.id + '\'' + ", customerId='" + this.customerId + '\'' + ", salesmanName='" + this.salesmanName + '\'' + ", dateTime=" + this.dateTime + ", dealRequestCount=" + this.dealRequestCount + '}';
    }
}
