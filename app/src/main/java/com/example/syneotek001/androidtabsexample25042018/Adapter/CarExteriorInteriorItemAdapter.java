package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.model.CarExteriorInteriorModel;

import java.util.ArrayList;
import java.util.List;

public class CarExteriorInteriorItemAdapter extends RecyclerView.Adapter<CarExteriorInteriorItemAdapter.MyViewHolder> {
    private boolean isItemSelectable;
    private Context mContext;
    private List<CarExteriorInteriorModel> mData = new ArrayList();

    class MyViewHolder extends ViewHolder {
        public ImageView ivImage, ivSelectIndicator;
        public TextView tvDescription, tvPrice, tvTitle;

        MyViewHolder(View view) {
            super(view);
            ivImage = view.findViewById(R.id.ivImage);
            ivSelectIndicator = view.findViewById(R.id.ivSelectIndicator);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvPrice = view.findViewById(R.id.tvPrice);
            tvTitle = view.findViewById(R.id.tvTitle);
        }
    }

    public CarExteriorInteriorItemAdapter(Context context, ArrayList<CarExteriorInteriorModel> list, boolean isSelectable) {
        this.mContext = context;
        this.mData = list;
        this.isItemSelectable = isSelectable;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public CarExteriorInteriorItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_car_exterior_interior, parent, false);

        return new CarExteriorInteriorItemAdapter.MyViewHolder(v);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        CarExteriorInteriorModel mCarExteriorInteriorModel = (CarExteriorInteriorModel) mData.get(position);
        holder.tvTitle.setText(mCarExteriorInteriorModel.getTitle());
        holder.tvPrice.setText(mContext.getResources().getString(R.string.dollar_sign_, new Object[]{mCarExteriorInteriorModel.getPrice()}));
        holder.tvDescription.setText(mCarExteriorInteriorModel.getDescription());
        holder.ivImage.setImageResource(mContext.getResources().getIdentifier(mCarExteriorInteriorModel.getImage(), "drawable", mContext.getPackageName()));
        holder.ivSelectIndicator.setImageDrawable(mCarExteriorInteriorModel.isSelected() ? mContext.getResources().getDrawable(R.drawable.ic_right_with_green_bg) : mContext.getResources().getDrawable(R.drawable.ic_right_with_gray_bg));

        if (CarExteriorInteriorItemAdapter.this.isItemSelectable) {
            holder.ivSelectIndicator.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Logger.m5e("Car Clicked");
                    int pos = position;
                    CarExteriorInteriorModel mCarExteriorInteriorModel = (CarExteriorInteriorModel) CarExteriorInteriorItemAdapter.this.mData.get(pos);
                    mCarExteriorInteriorModel.setSelected(!mCarExteriorInteriorModel.isSelected());
                    CarExteriorInteriorItemAdapter.this.notifyItemChanged(pos, mCarExteriorInteriorModel);
                }
            });
        }
    }

    public int getItemCount() {
        return mData.size();
    }
}
