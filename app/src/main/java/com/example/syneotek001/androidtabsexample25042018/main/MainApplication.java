package com.example.syneotek001.androidtabsexample25042018.main;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.Log;

import com.example.syneotek001.androidtabsexample25042018.Support.FontCache;
import com.example.syneotek001.androidtabsexample25042018.Utils.FontUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;



/**
 * @author bluegenie-13
 */


public class MainApplication extends Application {

	private ArrayList<WeakReference<OnLowMemoryListener>> mLowMemoryListeners;
	private ExecutorService mExecutorService;
	private static MainApplication mInstance = null;
	private static final int CORE_POOL_SIZE = 5;
	private static Context context;

	public  static TypedArray ta;



	@Override
	public void onCreate() {

		super.onCreate();

		context = null;
		context = getApplicationContext();


//		FontCache.getInstance().addFont("montmed", FontUtils.MONT_MED_FONT);
	/*	FontCache.getInstance().addFont("blackitalicfont",Utils.BLACK_ITALIC_FONT);
		FontCache.getInstance().addFont("boldfont",Utils.BOLD_FONT);
		FontCache.getInstance().addFont("bolditalicfont",Utils.BOLD_ITALIC_FONT);
		FontCache.getInstance().addFont("bookfont",Utils.BOOK_FONT);
		FontCache.getInstance().addFont("bookitalicfont",Utils.BOOK_ITALIC_FONT);
		FontCache.getInstance().addFont("mediumfont",Utils.MEDIUM_FONT);
		FontCache.getInstance().addFont("mediumitalicfont",Utils.MEDIUM_ITALIC_FONT);
		FontCache.getInstance().addFont("hfont",Utils.HELVETICA_FONT);*/

		mLowMemoryListeners = new ArrayList<WeakReference<OnLowMemoryListener>>();
		mInstance = this;

	}

	public static Context getGlobalContext() {
		return context;
	}

	public static Resources getAppResources() {
		return context.getResources();
	}

	public static String getAppString(int resourceId, Object... formatArgs) {
		return getAppResources().getString(resourceId, formatArgs);
	}

	public static String getAppString(int resourceId) {
		return getAppResources().getString(resourceId);
	}



	/*public static ArrayList<ListCoins> getArr_list_coins() {
		return arr_list_coins;
	}

	public  static void setArr_list_coins(ArrayList<ListCoins> arr_list_coins) {
		this.arr_list_coins = arr_list_coins;
	}*/


	@Override
	public void onTerminate() {

		Log.e("onTerminate","onTerminate");
		super.onTerminate();
	}

	public static interface OnLowMemoryListener {

		/**
		 * Callback to be invoked when the system needs memory.
		 */
		public void onLowMemoryReceived();
	}


	public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
		if (listener != null) {
			mLowMemoryListeners.add(new WeakReference<OnLowMemoryListener>(
					listener));
		}
	}

	public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
		if (listener != null) {
			int i = 0;
			while (i < mLowMemoryListeners.size()) {
				final OnLowMemoryListener l = mLowMemoryListeners.get(i).get();
				if (l == null || l == listener) {
					mLowMemoryListeners.remove(i);
				} else {
					i++;
				}
			}
		}
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		int i = 0;
		while (i < mLowMemoryListeners.size()) {
			final OnLowMemoryListener listener = mLowMemoryListeners.get(i)
					.get();
			if (listener == null) {
				mLowMemoryListeners.remove(i);
			} else {
				listener.onLowMemoryReceived();
				i++;
			}
		}
	}

	private static final ThreadFactory sThreadFactory = new ThreadFactory() {
		private final AtomicInteger mCount = new AtomicInteger(1);

		public Thread newThread(Runnable r) {
			return new Thread(r, "GreenDroid thread #"
					+ mCount.getAndIncrement());
		}
	};

	public ExecutorService getExecutor() {
		if (mExecutorService == null) {
			mExecutorService = Executors.newFixedThreadPool(CORE_POOL_SIZE,
					sThreadFactory);
		}
		return mExecutorService;
	}



	public static MainApplication getInstance() {
		return mInstance;
	}

	/*public static void changeLang(){

		LogUtils.i("MainApplication"+" changeLang TAG_LANGUAGE "+Utils.getInstance(getGlobalContext()).getString(TAG_LANGUAGE));
		Locale myLocale = new Locale(Utils.getInstance(getGlobalContext()).getString(TAG_LANGUAGE));
		Locale.setDefault(myLocale);
		android.content.res.Configuration config = new android.content.res.Configuration();
		config.locale = myLocale;
		getGlobalContext().getResources().updateConfiguration(config, getGlobalContext().getResources().getDisplayMetrics());
	}*/

}
