package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.AssignmentDealRequestItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse;
import com.example.syneotek001.androidtabsexample25042018.model.SalesmanDealRequestListResponse.SalesmanDealRequestListData;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AssignmentDealRequestFragment extends Fragment implements OnLeadItemViewClickListener, ApiResponseInterface {
    public static final String BUNDLE_UPDATE_LABEL_POSITION = "position";
    public static final String BUNDLE_UPDATE_LABEL_VALUE = "labelValue";
    public static final int LEAD_LIST_UPDATE_LABEL = 1;
    ArrayList<SalesmanDealRequestListData> leadItemArray = new ArrayList<>();
    AssignmentDealRequestItemAdapter mAdapter;
    View view;
    public RecyclerView recyclerView;
    public TextView tvNoDataFound;
    String sales_man_name,sales_man_id;
    ApiManager apiManager;
    int fromlimit = 1;
    Handler handler;
    boolean loadMore = false;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_assignment_deal_request, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);

        if (getArguments()!=null) {
            sales_man_name = getArguments().getString("sales_man_name");
            sales_man_id = getArguments().getString("sales_man_id");
        }



        if (sales_man_id!=null && !sales_man_id.equals("")) {
//            setUpRecyclerView();

            apiManager = new ApiManager(getActivity(), this);
            recyclerView = view.findViewById(R.id.recyclerView);
            tvNoDataFound = view.findViewById(R.id.tvNoDataFound);
            handler = new Handler();

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
            mAdapter = new AssignmentDealRequestItemAdapter(getActivity(), this.leadItemArray,this, recyclerView);
            mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {

                    Log.e("ChatListFragment", " onLoadMore ");

                    //add null , so the adapter will check view_type and show progress bar at bottom
                    leadItemArray.add(null);
                    recyclerView.post(new Runnable() {
                        public void run() {
                            mAdapter.notifyItemInserted(leadItemArray.size() - 1);
                        }
                    });

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            //   remove progress item
                            if (leadItemArray.size() > 0)
                                leadItemArray.remove(leadItemArray.size() - 1);

                            mAdapter.notifyItemRemoved(leadItemArray.size());

                            if (Utils.isOnline()) {
                                fromlimit = fromlimit + 1;
                                loadMore = true;
                                apiManager.getSalemanDealRequestList(fromlimit, AppConstant.SALESMAN_DEAL_REQUESTS_LIST_ACTION,sales_man_id);

                            } else {
                                Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
                            }

                        }
                    }, 1000);
                }
            });

        }
        else {
            recyclerView.setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void setUpRecyclerView() {

        if (Utils.isOnline()) {

            loadMore = false;
            leadItemArray.clear();
            LogUtils.i("BookingHistory" + " loadData arr_myrequestlist size " + leadItemArray.size());
            //getTripHistory(loadMore, 0, pos/*,TripHistoryActivity.this*/);
            if (sales_man_id!=null && !sales_man_id.equals("")) {
                apiManager.getSalemanDealRequestList(fromlimit, AppConstant.SALESMAN_DEAL_REQUESTS_LIST_ACTION, sales_man_id);
            }
        } else {

            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
        }

        /*leadItemArray.clear();

        leadItemArray.add(new AssignmentDealRequestItemModel("1525361955", "1", sales_man_name, "2018-03-13 15:27:59"));
        leadItemArray.add(new AssignmentDealRequestItemModel("1525334955", "2", sales_man_name, "2018-03-13 15:27:59"));
        leadItemArray.add(new AssignmentDealRequestItemModel("1525367855", "1", sales_man_name, "2018-03-13 15:27:59"));
        leadItemArray.add(new AssignmentDealRequestItemModel("1525361952", "2", sales_man_name, "2018-03-13 15:27:59"));
        leadItemArray.add(new AssignmentDealRequestItemModel("1525361925", "1", sales_man_name, "2018-03-13 15:27:59"));
        leadItemArray.add(new AssignmentDealRequestItemModel("1525361956", "1", sales_man_name, "2018-03-13 15:27:59"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new AssignmentDealRequestItemAdapter(getActivity(), leadItemArray, this);
        recyclerView.setAdapter(mAdapter);*/
    }

    public void onItemClicked(int position) {
    }


    public void onLeadDetailIconClicked(int position) {
        // open description page here
        LogUtils.i("AssignmentDealRequestFragment"+" onLeadDetailIconClicked position "+position+" DealRequests "+leadItemArray.get(position).getRequest_list().size());
        if (sales_man_name!=null && !sales_man_name.equals("")) {

            Bundle bundle = new Bundle();
            bundle.putSerializable(Utils.EXTRA_DEAL_REQUEST_LIST, (Serializable) leadItemArray.get(position).getRequest_list());
            bundle.putString(Utils.EXTRA_DEAL_DETAIL_FRIEND_ID,leadItemArray.get(position).getSalesman_id());
            bundle.putString(Utils.EXTRA_DEAL_DETAIL_FRIEND_NAME,leadItemArray.get(position).getFirstname()+" "+leadItemArray.get(position).getLastname());
            DealsDescriptionFragment dealsDescriptionFragment = new DealsDescriptionFragment();
            dealsDescriptionFragment.setArguments(bundle);
            ((HomeActivity) getActivity()).replaceFragment(dealsDescriptionFragment);



            /*ArrayList<ChatModel> mCarList = new ArrayList<>();
            mCarList.add(new ChatModel("1", sales_man_name, "https://pickeringtoyota.com/toyotasalesadmin/salesmanimages/1513327020_Koala.jpg", 0, true, 1526119235));
            DealsDescriptionFragment chatMessages = new DealsDescriptionFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(ChatMessagesFragment.BUNDLE_CHAT_FRIEND_MODEL, (Serializable) mCarList.get(0));
            bundle.putString("no_of_requests",leadItemArray.get(position).getDealRequests());
            chatMessages.setArguments(bundle);
            ((HomeActivity) getActivity()).replaceFragment(chatMessages);*/
        }
    }

    public void onLeadEmailIconClicked(int position) {
        ((HomeActivity) getActivity()).replaceFragment(new ComposeFragment());
    }

    public void onLeadScheduleIconClicked(int position) {
        // open edit page here
    }

    public void onLeadInactiveIconClicked(int position) {
    }

    @Override
    public void onLeadLabelClicked(int i) {

    }

    public void onLeadDealRequestClicked(int position) {
    }

    public void onLeadBusinessRequestClicked(int position) {
    }


    @Override
    public void onResume() {
        super.onResume();
        leadItemArray.clear();
        loadMore = false;
        fromlimit = 1;
        Log.e("DealRequest"," onResume leadItemArray size "+leadItemArray.size()+" loadMore "+loadMore+" fromlimit "+fromlimit);
        setUpRecyclerView();
    }

    @Override
    public void isError(String errorCode) {
        Log.e("DealRequest", " isError loadMore " + loadMore);
        if (!loadMore){
            recyclerView.setVisibility(View.GONE);
            tvNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void isSuccess(Object response, int ServiceCode) {
        SalesmanDealRequestListResponse chatListResponse = (SalesmanDealRequestListResponse) response;
        Log.e("loadmore_Count", "" + loadMore);

        final List<SalesmanDealRequestListData> taxiList = chatListResponse.getDealRequests();


        if (taxiList.size() > 0) {

            leadItemArray.addAll(taxiList);

            if (loadMore) {
                Log.e("loadmore_Count", "if");
                mAdapter.notifyItemInserted(leadItemArray.size());
                mAdapter.notifyDataSetChanged();
                mAdapter.setLoaded();
                LogUtils.i("BookingHistory" + " getTripHistory onResponse loadMore arr_myrequestlist.size() " + leadItemArray.size());

            } else {

                Log.e("loadmore_Count", "else");
                recyclerView.setVisibility(View.VISIBLE);

                Log.i("json", leadItemArray.size() + " arr_myrequestlist ");
                recyclerView.setAdapter(mAdapter);
                mAdapter.setLoaded();

                LogUtils.i("BookingHistory" + " getTripHistory onResponse !loadMore arr_myrequestlist.size() " + leadItemArray.size());

            }

        } else {

            if (!loadMore) {

                if (leadItemArray.isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
//                                    empty_view.setVisibility(View.VISIBLE);

                    tvNoDataFound.setVisibility(View.VISIBLE);

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    tvNoDataFound.setVisibility(View.GONE);
                }
            }
        }
    }

    public interface OnLoadMoreListener {

        void onLoadMore();

    }

}
