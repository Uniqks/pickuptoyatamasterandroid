package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.databinding.FragmentDealInfoDetailsBinding;
import com.example.syneotek001.androidtabsexample25042018.model.DealInfoDetailResponse;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiManager;
import com.example.syneotek001.androidtabsexample25042018.rest.ApiResponseInterface;
import com.example.syneotek001.androidtabsexample25042018.rest.AppConstant;

public class DealInfoDetailsFragment extends Fragment implements ApiResponseInterface {
    ImageView iv_Close;

    FragmentDealInfoDetailsBinding binding;
    ApiManager apiManager;

    String str_deal_id;

/*
    TextView tvTaskVal,tvPositionVal,tvLabelVal,tvLastActivityVal,tvDealNumberVal,tvCustomerIdVal,tvSalesManVal,tvPackageVal,tvExtAccessoriesVal,
            tvIntAccessoriesVal,tvDownPayment,tvTradeInTypeVal,tvTradeInValueVal,tvTradeInOwingVal,tvTaxIncludeVal,tvOffersIncludeVal,tvRateVal,
            tvTenureVal,tvEMIVal,tvFrequencyVal,tvAnnualKMVal,tvVehiclePriceVal,tvCaseVal,tvDealTypeVal;*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_deal_info_details, container, false);
        View view = binding.getRoot();
//        View view = inflater.inflate(R.layout.deal_info_details, container, false);
        iv_Close = view.findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });


        ImageView ivBack=view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        apiManager = new ApiManager(getActivity(), this);

        if (getArguments()!=null) {
            if (getArguments().getString(Utils.EXTRA_DEAL_DETAIL_INFO_DEAL_ID) !=null &&
                    !getArguments().getString(Utils.EXTRA_DEAL_DETAIL_INFO_DEAL_ID).equals("")) {
                str_deal_id = getArguments().getString(Utils.EXTRA_DEAL_DETAIL_INFO_DEAL_ID);
                Log.e("DealInfoDetailsFragment"," str_deal_id "+str_deal_id);
            }
        }

        if (Utils.isOnline()) {


            //getTripHistory(loadMore, 0, pos/*,TripHistoryActivity.this*/);
            if (str_deal_id!=null && !str_deal_id.equals("")) {
                apiManager.getDealInfoDetail(AppConstant.DEAL_INFO_DETAIL_ACTION, str_deal_id);
            }
        } else {

            Utils.getInstance(getActivity()).Toast(getActivity(), getString(R.string.err_no_internet));
        }

        return view;

    }


    @Override
    public void isError(String errorCode) {

    }

    @Override
    public void isSuccess(Object response, int ServiceCode) {
        Log.e("DealInfoDetailsFragment"," isSuccess ServiceCode "+ServiceCode);
        DealInfoDetailResponse dealInfoDetailResponse = (DealInfoDetailResponse) response;
        if (dealInfoDetailResponse.getData()!=null){

            if (isNotEmpty(dealInfoDetailResponse.getData().getTask()))
                binding.tvTaskVal.setText(dealInfoDetailResponse.getData().getTask()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getPosition()))
                binding.tvPositionVal.setText(dealInfoDetailResponse.getData().getPosition()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getPosition()))
                binding.tvPositionVal.setText(dealInfoDetailResponse.getData().getPosition()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getLabel()))
                binding.tvLabelVal.setText(dealInfoDetailResponse.getData().getLabel()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getLastactivitydate()))
                binding.tvLastActivityVal.setText(dealInfoDetailResponse.getData().getLastactivitydate()+"");

            if (isNotEmpty(dealInfoDetailResponse.getData().getDeal_number()))
                binding.tvDealNumberVal.setText(dealInfoDetailResponse.getData().getDeal_number()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getCustomer_number()))
                binding.tvCustomerIdVal.setText(dealInfoDetailResponse.getData().getCustomer_number()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getSalesman()))
                binding.tvSalesManVal.setText(dealInfoDetailResponse.getData().getSalesman()+"");

            if (isNotEmpty(dealInfoDetailResponse.getData().getStr_package()))
                binding.tvPackageVal.setText(dealInfoDetailResponse.getData().getStr_package()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getExt_accessories()))
                binding.tvExtAccessoriesVal.setText(dealInfoDetailResponse.getData().getExt_accessories()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getInt_accessories()))
                binding.tvIntAccessoriesVal.setText(dealInfoDetailResponse.getData().getInt_accessories()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getDown_payment()))
                binding.tvDownPayment.setText(dealInfoDetailResponse.getData().getDown_payment()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getTrade_in_type()))
                binding.tvTradeInTypeVal.setText(dealInfoDetailResponse.getData().getTrade_in_type()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getTrade_in_value()))
                binding.tvTradeInValueVal.setText(dealInfoDetailResponse.getData().getTrade_in_value()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getTrade_in_owing()))
                binding.tvTradeInOwingVal.setText(dealInfoDetailResponse.getData().getTrade_in_owing()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getInc_tax()))
                binding.tvTaxIncludeVal.setText(dealInfoDetailResponse.getData().getInc_tax()+"");

            if (isNotEmpty(dealInfoDetailResponse.getData().getInc_offers()))
                binding.tvOffersIncludeVal.setText(dealInfoDetailResponse.getData().getInc_offers()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getRate()))
                binding.tvRateVal.setText(dealInfoDetailResponse.getData().getRate()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getTenure()))
                binding.tvTenureVal.setText(dealInfoDetailResponse.getData().getTenure()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getEmi()))
                binding.tvEMIVal.setText(dealInfoDetailResponse.getData().getEmi()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getFrequency()))
                binding.tvFrequencyVal.setText(dealInfoDetailResponse.getData().getFrequency()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getAnnualkm()))
                binding.tvAnnualKMVal.setText(dealInfoDetailResponse.getData().getAnnualkm()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getVehicle_price()))
                binding.tvVehiclePriceVal.setText(dealInfoDetailResponse.getData().getVehicle_price()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getCash()))
                binding.tvCaseVal.setText(dealInfoDetailResponse.getData().getCash()+"");

            if (isNotEmpty(dealInfoDetailResponse.getData().getIs_lease_finance()))
                binding.tvDealTypeVal.setText(dealInfoDetailResponse.getData().getIs_lease_finance()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getVin_number()))
                binding.tvVinVal.setText(dealInfoDetailResponse.getData().getVin_number()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getInv_type()))
                binding.tvINVTypeVal.setText(dealInfoDetailResponse.getData().getInv_type()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getMake()))
                binding.tvMakeVal.setText(dealInfoDetailResponse.getData().getMake()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getModel()))
                binding.tvModelVal.setText(dealInfoDetailResponse.getData().getModel()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getSub_model()))
                binding.tvSubModelVal.setText(dealInfoDetailResponse.getData().getSub_model()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getBody_style()))
                binding.tvBodyStyleVal.setText(dealInfoDetailResponse.getData().getBody_style()+"");

            if (isNotEmpty(dealInfoDetailResponse.getData().getYear()))
                binding.tvYearVal.setText(dealInfoDetailResponse.getData().getYear()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getExterior_color()))
                binding.tvExtColorVal.setText(dealInfoDetailResponse.getData().getExterior_color()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getInterior_color()))
                binding.tvIntColorVal.setText(dealInfoDetailResponse.getData().getInterior_color()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getFuel_type()))
                binding.tvFuelTypeVal.setText(dealInfoDetailResponse.getData().getFuel_type()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getDrive_type()))
                binding.tvDriveTypeVal.setText(dealInfoDetailResponse.getData().getDrive_type()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getTransmission()))
                binding.tvTransmissionVal.setText(dealInfoDetailResponse.getData().getTransmission()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getDoors()))
                binding.tvDoorVal.setText(dealInfoDetailResponse.getData().getDoors()+"");

            if (isNotEmpty(dealInfoDetailResponse.getData().getCondition()))
                binding.tvConditionVal.setText(dealInfoDetailResponse.getData().getCondition()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getCategory()))
                binding.tvCategoryVal.setText(dealInfoDetailResponse.getData().getCategory()+"");
            if (isNotEmpty(dealInfoDetailResponse.getData().getEngine()))
                binding.tvEngineVal.setText(dealInfoDetailResponse.getData().getEngine()+"");

        }

    }

    public boolean isNotEmpty(String str_value) {
        boolean result = false;
        if (str_value != null && !str_value.equals("")) {
            result = true;
        }
        Log.e("DealInfoDetailsFragment"," isNotEmpty str_value "+str_value + " result "+result);
        return result;
    }

}
