package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.EmailItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.EmailAdapterListener;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.EmailListModel;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.Iterator;

public class EmailJunkListFragment extends Fragment implements EmailAdapterListener, View.OnClickListener, OnBackPressed {
    TypedArray bgCircleList;
    private Handler mHandler;
    private ActionMode actionMode;
    public FloatingActionButton fabCompose, fabDraft, fabFolders, fabInbox, fabJunk, fabSent, fabTrash;
    ArrayList<EmailListModel> junkEmailList = new ArrayList<>();
    public FloatingActionMenu fabEmailMenu;
    EmailItemAdapter mAdapter;
    private int selectedItem = -1;
    View view;
    Context mcontext;
    RecyclerView recyclerView;
    public TextView tvNoEmailFound;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mcontext = getActivity();
        View view = inflater.inflate(R.layout.fragment_emailjunk_list, container, false);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        bgCircleList = getResources().obtainTypedArray(R.array.bg_circles);
        tvNoEmailFound = view.findViewById(R.id.tvNoEmailFound);
        fabCompose = view.findViewById(R.id.fabCompose);
        fabDraft = view.findViewById(R.id.fabDraft);
        fabEmailMenu = view.findViewById(R.id.fabEmailMenu);
        fabFolders = view.findViewById(R.id.fabFolders);
        fabInbox = view.findViewById(R.id.fabInbox);
        fabJunk = view.findViewById(R.id.fabJunk);
        fabSent = view.findViewById(R.id.fabSent);
        fabTrash = view.findViewById(R.id.fabTrash);
        prepareFabMenu();
        setUpRecyclerView();
        ImageView ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        return view;
    }

    private void prepareFabMenu() {
        ArrayList<FloatingActionButton> fabButtons = new ArrayList<>();
        fabButtons.add(fabCompose);
        fabButtons.add(fabDraft);
        fabButtons.add(fabSent);
        fabButtons.add(fabJunk);
        fabButtons.add(fabInbox);
        fabButtons.add(fabTrash);
        fabButtons.add(fabFolders);
        prepareFabMenu(fabEmailMenu, fabButtons, fabJunk);
    }

    protected void prepareFabMenu(FloatingActionMenu fabEmailMenu, ArrayList<FloatingActionButton> fabButtons, FloatingActionButton fabCurrentButton) {
        mHandler = new Handler();
//       fabEmailMenu = fabEmailMenu;
        fabEmailMenu.setClosedOnTouchOutside(true);
        Iterator it = fabButtons.iterator();
        while (it.hasNext()) {
            FloatingActionButton fabButton = (FloatingActionButton) it.next();
            fabButton.setOnClickListener(this);
            if (fabButton.equals(fabCurrentButton)) {
                fabCurrentButton.setVisibility(View.GONE);
            }
        }
    }

    private void setUpRecyclerView() {
        junkEmailList.add(new EmailListModel("4", "New Offer! Hurry Up!", "xyz@gmail.com", "tester@test.com", "Main Subject", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        junkEmailList.add(new EmailListModel("4", "Congratulation! You won $1200.", "xyz@gmail.com", "annonymous@test.com", "Main Subject", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), true));
        junkEmailList.add(new EmailListModel("4", "You are getting 20% off", "xyz@gmail.com", "stranger@test.com", "Main Subject", "Body", "", "", 1526462694, this.bgCircleList.getResourceId((int) (Math.random() * ((double) this.bgCircleList.length())), 0), false));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mcontext, R.anim.layout_animation_fall_down));
        mAdapter = new EmailItemAdapter(getActivity(), junkEmailList, this);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new EmailSentListFragment.onSrollListener());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabCompose:
                ((HomeActivity) mcontext).replaceFragment(new ComposeFragment());
                return;
            case R.id.fabDraft:
                ((HomeActivity) mcontext).replaceFragment(new EmailDraftListFragment());
                return;
            case R.id.fabInbox:
                ((HomeActivity) mcontext).replaceFragment(new EmailInboxListFragment());
                return;
            case R.id.fabJunk:
                ((HomeActivity) mcontext).replaceFragment(new EmailJunkListFragment());

                return;
            case R.id.fabSent:
                ((HomeActivity) mcontext).replaceFragment(new EmailSentListFragment());
                return;
            case R.id.fabTrash:
                ((HomeActivity) mcontext).replaceFragment(new EmailTrashListFragment());
                return;
            default:
                return;
        }
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onIconClicked(int i) {

    }

    @Override
    public void onMessageRowClicked(int i) {

        if (actionMode != null) {
            actionMode.finish();
            unsetActionMode();
        }
        EmailDetailFragment emailDetailFragment = new EmailDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EmailDetailFragment.BUNDLE_EMAIL_DATA, junkEmailList.get(i));
        emailDetailFragment.setArguments(bundle);
        ((HomeActivity)getActivity()).replaceFragment(emailDetailFragment);
    }

    @Override
    public void onRowLongClicked(int i) {

    }

    class onSrollListener extends RecyclerView.OnScrollListener {
        onSrollListener() {
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0 && !fabEmailMenu.isMenuButtonHidden()) {
                fabEmailMenu.hideMenuButton(true);
            } else if (dy < 0 && fabEmailMenu.isMenuButtonHidden()) {
                fabEmailMenu.showMenuButton(true);
            }
        }
    }

    protected void unsetActionMode() {
        actionMode = null;
    }
}

