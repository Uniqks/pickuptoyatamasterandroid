package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.ArrayList;


/**
 * Created by bluegenie-24 on 29/6/18.
 */

public class LeaseSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<String> countries;
    LayoutInflater layoutInflater;
    Typeface tf;

    public LeaseSpinnerAdapter(Context context, ArrayList<String> countries) {
        this.countries=countries;
        activity = context;
        layoutInflater = LayoutInflater.from(activity);
        tf = Typeface.createFromAsset(context.getAssets(),"fonts/montserrat_regular.otf");
    }



    public int getCount()
    {
        return countries.size();
    }

    public Object getItem(int i)
    {
        return countries.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }



    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(18);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(countries.get(position));
        txt.setTextColor(ContextCompat.getColor(activity,R.color.custom_spiiner_txt));
        txt.setTypeface(tf);
        return  txt;
    }

    public View getView(int i, View convertView, ViewGroup viewgroup) {

        /*TextView txt = new TextView(activity);
        txt.setGravity(Gravity.CENTER);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
        txt.setText(asr.get(i));
        txt.setTextColor(ContextCompat.getColor(activity,R.color.white));
        return  txt;*/

        convertView = layoutInflater.inflate(R.layout.adapter_spinner_lease, null);

        TextView tvContry = (TextView) convertView.findViewById(R.id.tvContry);

        tvContry.setText(countries.get(i));

        return convertView;


    }

}
