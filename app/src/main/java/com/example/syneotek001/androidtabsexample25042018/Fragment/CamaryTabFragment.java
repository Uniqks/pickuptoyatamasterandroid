package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.CamaryPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

public class CamaryTabFragment extends Fragment implements OnBackPressed {
    TabLayout tabLayout;
    ViewPager viewPager;
    View view;
    CamaryPagerAdapter camaryPagerAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_camary_tab, container, false);
        tabLayout = (TabLayout)view. findViewById(R.id.tab_layout_camry);
        tabLayout.addTab(tabLayout.newTab().setText("EXTERIOR ACCESSORIES"));
        tabLayout.addTab(tabLayout.newTab().setText("INTERIOR ACCESSORIES "));
        viewPager = (ViewPager)view.findViewById(R.id.tab_pager_camry);
        camaryPagerAdapter=new CamaryPagerAdapter(getActivity().getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(camaryPagerAdapter);
        return view;
    }

    @Override
    public void onBackPressed() {

       /* getActivity().getSupportFragmentManager().popBackStack();*/

    }
}
