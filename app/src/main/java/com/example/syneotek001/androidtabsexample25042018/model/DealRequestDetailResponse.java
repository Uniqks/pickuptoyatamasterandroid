package com.example.syneotek001.androidtabsexample25042018.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bluegenie-24 on 9/8/18.
 */

public class DealRequestDetailResponse implements Serializable {

    DealRequestDetailData deal_request_detail;

    public class DealRequestDetailData implements Serializable {
        String deal_id;
        String salesman_id;
        String lead_id;
        String deal_number;
        String request_notes;
        String request_attachment;
        String dr_datetime;
        String is_read;

        public String getDeal_id() {
            return deal_id;
        }

        public void setDeal_id(String deal_id) {
            this.deal_id = deal_id;
        }

        public String getSalesman_id() {
            return salesman_id;
        }

        public void setSalesman_id(String salesman_id) {
            this.salesman_id = salesman_id;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getDeal_number() {
            return deal_number;
        }

        public void setDeal_number(String deal_number) {
            this.deal_number = deal_number;
        }

        public String getRequest_notes() {
            return request_notes;
        }

        public void setRequest_notes(String request_notes) {
            this.request_notes = request_notes;
        }

        public String getRequest_attachment() {
            return request_attachment;
        }

        public void setRequest_attachment(String request_attachment) {
            this.request_attachment = request_attachment;
        }

        public String getDr_datetime() {
            return dr_datetime;
        }

        public void setDr_datetime(String dr_datetime) {
            this.dr_datetime = dr_datetime;
        }

        public String getIs_read() {
            return is_read;
        }

        public void setIs_read(String is_read) {
            this.is_read = is_read;
        }
    }



    public ArrayList<DealRequestMessagesData> deal_request_msgs;

    public DealRequestDetailData getDeal_request_detail() {
        return deal_request_detail;
    }

    public void setDeal_request_detail(DealRequestDetailData deal_request_detail) {
        this.deal_request_detail = deal_request_detail;
    }

    public ArrayList<DealRequestMessagesData> getDeal_request_msgs() {
        return deal_request_msgs;
    }

    public void setDeal_request_msgs(ArrayList<DealRequestMessagesData> deal_request_msgs) {
        this.deal_request_msgs = deal_request_msgs;
    }

    public class DealRequestMessagesData implements Serializable {

        String id;
        String deal_request_id;
        String msg_type;
        String msg_to;
        String msg_from;
        String msg_text;
        String msg_status;
        String msg_datetime;
        String is_received;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDeal_request_id() {
            return deal_request_id;
        }

        public void setDeal_request_id(String deal_request_id) {
            this.deal_request_id = deal_request_id;
        }

        public String getMsg_type() {
            return msg_type;
        }

        public void setMsg_type(String msg_type) {
            this.msg_type = msg_type;
        }

        public String getMsg_to() {
            return msg_to;
        }

        public void setMsg_to(String msg_to) {
            this.msg_to = msg_to;
        }

        public String getMsg_from() {
            return msg_from;
        }

        public void setMsg_from(String msg_from) {
            this.msg_from = msg_from;
        }

        public String getMsg_text() {
            return msg_text;
        }

        public void setMsg_text(String msg_text) {
            this.msg_text = msg_text;
        }

        public String getMsg_status() {
            return msg_status;
        }

        public void setMsg_status(String msg_status) {
            this.msg_status = msg_status;
        }

        public String getMsg_datetime() {
            return msg_datetime;
        }

        public void setMsg_datetime(String msg_datetime) {
            this.msg_datetime = msg_datetime;
        }

        public String getIs_received() {
            return is_received;
        }

        public void setIs_received(String is_received) {
            this.is_received = is_received;
        }
    }
}
