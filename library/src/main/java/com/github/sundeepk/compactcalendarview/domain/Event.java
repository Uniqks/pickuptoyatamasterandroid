package com.github.sundeepk.compactcalendarview.domain;

import android.support.annotation.Nullable;

public class Event {

    private int color;
    private long timeInMillis;
    private Object data;

    public EventData getEventData() {
        return eventData;
    }

    public void setEventData(EventData eventData) {
        this.eventData = eventData;
    }

    EventData eventData;

    public class EventData {
        String eventTime;
        String uname;
        String ugender;
        String ustatus;
        String taskName;
        String taskStartTime;
        String taskEndTime;
        boolean is_selected;

        public String getTaskName() {
            return taskName;
        }

        public void setTaskName(String taskName) {
            this.taskName = taskName;
        }

        public String getTaskStartTime() {
            return taskStartTime;
        }

        public void setTaskStartTime(String taskStartTime) {
            this.taskStartTime = taskStartTime;
        }

        public String getTaskEndTime() {
            return taskEndTime;
        }

        public void setTaskEndTime(String taskEndTime) {
            this.taskEndTime = taskEndTime;
        }

        public EventData(String taskName, String taskStartTime, String taskEndTime){
            this.taskName = taskName;
            this.taskStartTime = taskStartTime;
            this.taskEndTime = taskEndTime;
        }

        public EventData(String eventTime,String uname,String ugender,String ustatus,boolean is_selected){
            this.eventTime = eventTime;
            this.uname = uname;
            this.ugender = ugender;
            this.ustatus = ustatus;
            this.is_selected = is_selected;
        }

        public String getEventTime() {
            return eventTime;
        }

        public void setEventTime(String eventTime) {
            this.eventTime = eventTime;
        }

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getUgender() {
            return ugender;
        }

        public void setUgender(String ugender) {
            this.ugender = ugender;
        }

        public String getUstatus() {
            return ustatus;
        }

        public void setUstatus(String ustatus) {
            this.ustatus = ustatus;
        }

        public boolean isIs_selected() {
            return is_selected;
        }

        public void setIs_selected(boolean is_selected) {
            this.is_selected = is_selected;
        }
    }

    public Event(int color, long timeInMillis, EventData eventData) {
        this.color = color;
        this.timeInMillis = timeInMillis;
        this.eventData = eventData;
    }

    public Event() {
    }

    public Event(int color, long timeInMillis, Object data, EventData eventData) {
        this.color = color;
        this.timeInMillis = timeInMillis;
        this.data = data;
        this.eventData = eventData;
    }

    public int getColor() {
        return color;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    @Nullable
    public Object getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (color != event.color) return false;
        if (timeInMillis != event.timeInMillis) return false;
        if (data != null ? !data.equals(event.data) : event.data != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = color;
        result = 31 * result + (int) (timeInMillis ^ (timeInMillis >>> 32));
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }



    @Override
    public String toString() {
        return "Event{" +
                "color=" + color +
                ", timeInMillis=" + timeInMillis +
                ", data=" + data +
                '}';
    }
}
